output "arn" {
  description = "Terraform実行用ロールのARN"
  value       = aws_iam_role.execution.arn
}
