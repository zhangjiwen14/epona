output "terraformer_users" {
  description = "各環境のTerraform実行用ユーザのARNと名前"
  value = { for env, v in aws_iam_user.terraformer : env => {
    "arn"  = v.arn
    "name" = v.name
  } }
}

output "terraform_execution_role_arn" {
  description = "Terraform実行ロールのARN"
  value       = module.terraform_execution.arn
}
