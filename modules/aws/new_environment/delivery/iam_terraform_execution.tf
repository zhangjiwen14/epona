# Delivery環境のTerraform実行ロール設定
module "terraform_execution" {
  source = "../terraform_execution/"

  prefix = var.prefix
  # Delivery環境用のTerraform実行ユーザに実行用ロールを割り当て可能にする
  principals = [aws_iam_user.terraformer[var.delivery_account_name].arn]
}
