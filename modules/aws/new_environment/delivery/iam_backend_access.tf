# それぞれのTerraform実行ユーザにのみ移譲するassume-role policy
data "aws_iam_policy_document" "assume_role" {
  for_each = { for env, v in aws_iam_user.terraformer : env => v.arn }

  statement {
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "AWS"
      identifiers = [each.value]
    }
  }
}

resource "aws_iam_role" "backend_access" {
  for_each = local.environments

  name               = format("%s%sTerraformBackendAccessRole", title(var.prefix), title(each.key))
  assume_role_policy = data.aws_iam_policy_document.assume_role[each.key].json
}

resource "aws_iam_role_policy_attachment" "backend_access" {
  for_each = local.environments

  role       = aws_iam_role.backend_access[each.key].name
  policy_arn = aws_iam_policy.backend_access[each.key].arn
}

resource "aws_iam_policy" "backend_access" {
  for_each = local.environments

  description = "Allow access to terraform backend for ${each.key}"
  name        = format("%s%sTerraformBackendAccessPolicy", title(var.prefix), title(each.key))
  policy      = data.aws_iam_policy_document.backend_access[each.key].json
}

# Terraform Backend にアクセスするためのポリシー
# see: https://www.terraform.io/docs/backends/types/s3.html
data "aws_iam_policy_document" "backend_access" {
  for_each = local.environments

  statement {
    actions = [
      "s3:ListBucket",
    ]
    resources = [
      aws_s3_bucket.tfstate[each.key].arn
    ]
  }

  statement {
    actions = [
      "s3:PutObject",
      "s3:GetObject"
    ]
    resources = [
      "${aws_s3_bucket.tfstate[each.key].arn}/*"
    ]
  }

  statement {
    actions = [
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:DeleteItem"
    ]
    resources = [
      aws_dynamodb_table.tfstate_lock.arn
    ]
  }
}
