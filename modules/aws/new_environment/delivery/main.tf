locals {
  environments = toset(concat(
    [title(var.delivery_account_name)],
    keys(var.runtime_accounts),
  ))
}

resource "aws_iam_group" "terraformer" {
  name = format("%sTerraformGroup", title(var.prefix))
}

# 環境の数だけTerraform実行ユーザを作成
resource "aws_iam_user" "terraformer" {
  for_each = local.environments
  # 複数名を指定可能にする
  name = format("%s%sTerraformer", title(var.prefix), title(each.value))

  tags = {
    Environment = each.value
  }
}

# Terraform実行ユーザを全て同一グループに所属させる
resource "aws_iam_user_group_membership" "terraformer" {
  for_each = toset(values(aws_iam_user.terraformer)[*].name)

  user = each.value

  groups = concat([aws_iam_group.terraformer.name], var.user_groups)
}

resource "aws_s3_bucket" "backend_itself" {
  bucket        = join("-", [var.name, lower(var.delivery_account_name), "backend"])
  acl           = "private"
  force_destroy = var.force_destroy

  # tfstate 用の bucket バージョニング推奨
  # https://www.terraform.io/docs/backends/types/s3.html
  versioning {
    enabled = true
  }

  # SSE による暗号化
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    Name = join("-", [var.name, lower(var.delivery_account_name), "backend"])
  }
}

resource "aws_s3_bucket_public_access_block" "backend_itself" {
  bucket = aws_s3_bucket.backend_itself.bucket

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
