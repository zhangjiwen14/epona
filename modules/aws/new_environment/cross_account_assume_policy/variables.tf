variable "prefix" {
  description = "テスト時等でユニークな名前を付与したいとき、ユニークになるように付与する接頭語"
  type        = string
  default     = ""
}

variable "execution_role_map" {
  description = "環境名とTerraformExecutionRoleのマップ"
  type        = map(string)
}

variable "terraformer_users" {
  description = "環境毎のTerraform実行用ユーザのARN・名前のマップ。キーが環境名、値が実行ユーザ情報"
  type = map(object({
    arn  = string
    name = string
  }))
}
