variable "name" {
  description = "サービスを一意に特定する名前"
  type        = string
}

variable "force_destroy" {
  description = "bucketが空でなくても強制的に削除するかどうか"
  type        = bool
  default     = false
}

variable "environment" {
  description = "環境名"
  type        = string
}
