output "bucket_arn" {
  description = "tfstate用バケットのARN"
  value       = aws_s3_bucket.tfstate.arn
}

output "lock_table_arn" {
  description = "tfstateロック管理用テーブルのARN"
  value       = aws_dynamodb_table.tfstate_lock.arn
}
