provider "aws" {
  alias = "ssm_provider"
}

provider "aws" {
  alias = "datadog_deployment"
}

locals {
  kms_key_alias = "alias/${var.datadog_forwarder_cloudformation_stack_name}-key"

  required_parameters = {
    DdApiKey          = "****" # 他の値にすると、apply後のplanで差分になる
    DdApiKeySecretArn = aws_secretsmanager_secret.dd_api_key.arn
  }

  optional_parameters = {
    FunctionName = var.datadog_forwarder_lambda_function_name
    DdTags       = var.datadog_forwarder_tags
  }
}


data "aws_ssm_parameter" "dd_api_key" {
  provider = aws.ssm_provider

  name = var.dd_api_key_ssm_parameter_name
}

module "kms_key" {
  source = "../../components/kms"

  providers = {
    aws = aws.datadog_deployment
  }

  kms_keys = [
    {
      alias_name              = local.kms_key_alias
      deletion_window_in_days = var.secretsmanager_kms_key_deletion_window_in_days
      is_enabled              = true
      enable_key_rotation     = false
    }
  ]

  tags = var.tags
}

resource "aws_secretsmanager_secret" "dd_api_key" {
  provider = aws.datadog_deployment

  name        = var.dd_api_key_secretsmanager_secret_name
  description = "Encrypted Datadog API Key"

  kms_key_id              = module.kms_key.keys[local.kms_key_alias].key_arn
  recovery_window_in_days = var.secretsmanager_recovery_window_in_days

  tags = var.tags
}

resource "aws_secretsmanager_secret_version" "dd_api_key" {
  provider = aws.datadog_deployment

  secret_id     = aws_secretsmanager_secret.dd_api_key.id
  secret_string = data.aws_ssm_parameter.dd_api_key.value
}

resource "aws_cloudformation_stack" "datadog_forwarder" {
  provider = aws.datadog_deployment

  name = var.datadog_forwarder_cloudformation_stack_name

  capabilities = ["CAPABILITY_IAM", "CAPABILITY_NAMED_IAM", "CAPABILITY_AUTO_EXPAND"]
  parameters   = merge(local.required_parameters, local.optional_parameters, var.datadog_forwarder_parameters)
  template_url = var.datadog_forwarder_cloudformation_template_url

  tags = var.tags

  depends_on = [aws_secretsmanager_secret.dd_api_key]
}

data "aws_lambda_function" "datadog_forwarder" {
  provider = aws.datadog_deployment

  function_name = aws_cloudformation_stack.datadog_forwarder.parameters["FunctionName"]

  # OutputにCloudFormationにより作成した、LambdaのARNを取得するため
  depends_on = [aws_cloudformation_stack.datadog_forwarder]
}
