variable "tags" {
  description = "このモジュールで構築されるリソースに、共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "dd_api_key_ssm_parameter_name" {
  description = <<DESCRIPTION
  AWS Systems Manager パラメーターストアに格納した、DatadogのAPIキーのパラメーター名。Datadog Forwarderが提供するLambda関数に必要なため、このモジュールでリソース設定を行う。

  最終的には、AWS Secrets Managerに格納され、DdApiKeySecretArnとして使用される。

  Datadogにより提供されるLambda関数が参照するAPIキーは、本来はAWS Secrets Managerで登録する必要があるが、
  Eponaでは秘匿情報はAWS Systems Manager パラメーターストアを使用するため、本モジュールでAWS Systems Manager パラメーターストアに
  AWS Secrets Managerに登録し直している

  詳細は、[こちら](https://docs.datadoghq.com/ja/serverless/forwarder#cloudformation-%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E3%83%BC)を参照。
  DESCRIPTION
  type        = string
}

variable "dd_api_key_secretsmanager_secret_name" {
  description = "DatadogのAPIキーをAWS Secrets Managerに格納する際の名前"
  type        = string
  default     = "datadog_api_key"
}

variable "secretsmanager_kms_key_deletion_window_in_days" {
  description = "このモジュールが、SecretsManager用に内部で作成するKMS CMKの削除猶予期間を指定する"
  type        = number
  default     = 30
}

variable "secretsmanager_recovery_window_in_days" {
  description = "SecretsManager上に作成する、Datadog転送用パラメータの削除猶予期間。削除猶予期間を7〜30（単位：日）の間で設定する"
  type        = number
  default     = null
}

variable "datadog_forwarder_cloudformation_stack_name" {
  description = "このモジュールでデプロイするCloudFormationのスタック名を指定する"
  type        = string
  default     = "datadog-forwarder"
}

variable "datadog_forwarder_lambda_function_name" {
  description = <<DESCRIPTION
  このモジュールでデプロイされる、Lambdaの関数名を指定する（DatadogのCloudFormationパラメーター`FunctionName`に相当する）

  詳細は、[こちら](https://docs.datadoghq.com/ja/serverless/forwarder#cloudformation-%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E3%83%BC)を参照。
  DESCRIPTION
  type        = string
  default     = "datadog-forwarder"
}

variable "datadog_forwarder_tags" {
  description = <<DESCRIPTION
  このモジュールでデプロイされるLambda関数が付与する、タグを指定する（DatadogのCloudFormationパラメーター`DdTags`に相当する）

  詳細は、[こちら](https://docs.datadoghq.com/ja/serverless/forwarder#cloudformation-%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E3%83%BC)を参照。
  DESCRIPTION
  type        = string
  default     = ""
}

variable "datadog_forwarder_parameters" {
  description = <<DESCRIPTION
  このモジュールでデプロイする、CloudFormationスタックに引き渡すパラメーターを`map`形式で指定する。

  ただし、DdApiKeySecretArn、FunctionName、DdTagsについては他の変数を使用すればよく、DdApiKeyについては指定する必要がない。

  詳細は、[こちら](https://docs.datadoghq.com/ja/serverless/forwarder#cloudformation-%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E3%83%BC)を参照。
  DESCRIPTION
  type        = map(any)
  default     = {}
}

variable "datadog_forwarder_cloudformation_template_url" {
  description = "このモジュールがデプロイする、CloudFormationのテンプレートURLを指定する"
  type        = string
  default     = "https://datadog-cloudformation-template.s3.amazonaws.com/aws/forwarder/latest.yaml"
}
