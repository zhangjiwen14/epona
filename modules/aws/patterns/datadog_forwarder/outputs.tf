output "dd_api_key_secret_arn" {
  description = "Datadog APIキーを格納した、Secrets ManagerのARN"
  value       = aws_secretsmanager_secret.dd_api_key.arn
}

output "datadog_forwarder_lambda_function_arn" {
  description = "CloudFormationによりデプロイされた、Datadogのログ転送用のLambda関数のARN"
  value       = data.aws_lambda_function.datadog_forwarder.arn
}

output "kms_keys" {
  description = "このモジュールが作成したKMS CMK"
  value       = module.kms_key.keys
}
