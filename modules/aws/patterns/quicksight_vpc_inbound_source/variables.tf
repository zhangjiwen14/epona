variable "security_group_name" {
  description = "このモジュールで作成されるSecurityGroupの名前"
  type        = string
}

variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "integration_security_groups" {
  description = <<DESCRIPTION
QuickSightが参照するAWSリソースに設定されているセキュリティグループ設定のリスト

VPC内にあるリソースをQuickSightから参照する際に必要になる設定
```
integration_security_groups = [
  {
    description       = "RDS integration for QuickSight"   # 結合するセキュリティグループの説明
    security_group_id = "sg-000000"   # VPC内のリソースに設定されているセキュリティグループ
  }
]
```
DESCRIPTION
  type        = list(map(string))
}

variable "vpc_id" {
  description = "QuickSightが参照するリソースが配置されているVPCのID"
  type        = string
}
