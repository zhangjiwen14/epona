output "frontend_bucket_name" {
  description = "ビルド済みファイルを配置するバケットの名前"
  value       = module.s3_frontend_bucket.bucket_name
}

output "frontend_bucket_origin_path" {
  description = "ビルド済みファイルを配置するバケットのパス"
  # var.cloudfront_origin に origin_path が含まれている場合は origin_path を出力
  value = contains(keys(var.cloudfront_origin), "origin_path") ? var.cloudfront_origin.origin_path : ""
}

output "cloudfront_id" {
  description = "CloudFrontのID"
  value       = module.cloudfront_distribution.id
}

output "cloudfront_domain_name" {
  description = "CloudFrontのドメイン名"
  value       = module.cloudfront_distribution.domain_name
}

output "cloudfront_aliases" {
  description = "CloudFrontのドメインに設定した別名（CNAME）"
  value       = module.cloudfront_distribution.aliases
}

output "cloudfront_logging_bucket_id" {
  description = "CloudFrontのログ出力用のS3バケットのID"
  value       = module.cloudfront_logging_bucket.bucket_id
}

output "cloudfront_logging_bucket" {
  description = "CloudFrontのログ出力用のログ出力用のS3バケット名"
  value       = module.cloudfront_logging_bucket.bucket
}

output "s3_access_log_bucket_id" {
  description = "S3へのアクセスログ出力用のS3バケットのID"
  value       = module.s3_access_log_bucket.bucket_id
}

output "s3_access_log_bucket" {
  description = "S3へのアクセスログ出力用のS3バケット名"
  value       = module.s3_access_log_bucket.bucket
}
