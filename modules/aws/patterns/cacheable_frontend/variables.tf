variable "s3_frontend_bucket_name" {
  description = "フロントの静的ファイルを配置する場所として作成するS3バケット名"
  type        = string
}

variable "s3_frontend_bucket_force_destroy" {
  description = "フロントの静的ファイル配置用S3バケットにファイルがあっても、destroyでバケットごと強制削除できるようにする"
  type        = bool
  default     = false
}

variable "zone_name" {
  description = "事前作成済みのホストゾーン名"
  type        = string
}

variable "record_name" {
  description = "アプリケーションのエンドポイントとなるレコード名"
  type        = string
}

variable "cloudfront_default_root_object" {
  description = "ルートへのアクセス時に表示させるファイル名（例: `index.html`）"
  type        = string
}

variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "cloudfront_http_version" {
  description = "CloudFrontでサポートする最大のHTTPのバージョン（`http1.1` or `http2`）"
  type        = string
  default     = null
}

variable "cloudfront_is_ipv6_enabled" {
  description = "CloudFrontでのIPv6有効化"
  type        = bool
  default     = null
}

variable "cloudfront_price_class" {
  description = "CloudFrontで利用する価格クラス（`PriceClass_All` or `PriceClass_200` or `PriceClass_100`）"
  type        = string
  default     = null
}

variable "cloudfront_web_acl_id" {
  description = "CloudFrontで利用するWebACLのARN"
  type        = string
  default     = null
}


variable "cloudfront_origin" {
  description = <<DESCRIPTION
  CloudFrontの配信ソースの設定
```
{
  origin_path = 配信ソースとするバケットのスラッシュから始まるディレクトリパス（デフォルトでは「/」）
  custom_headers = [
    {
      name : レスポンスヘッダーに付与するヘッダーの名前
      value : レスポンスヘッダーに付与するヘッダーの値
    }
  ]
}
```
DESCRIPTION
  type        = any
  default     = {}
}

variable "cloudfront_default_cache_behavior" {
  description = "CloudFrontのデフォルトのキャッシュ動作設定"
  type        = any
  default = {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = null
    default_ttl            = null
    max_ttl                = null
    compress               = false
  }
}

variable "cloudfront_viewer_certificate" {
  description = "CloudFrontのSSL化の設定"
  type        = map(string)
  default = {
    minimum_protocol_version = "TLSv1.2_2019"
  }
}

variable "cloudfront_custom_error_responses" {
  description = <<DESCRIPTION
CloudFrontのエラーページアクセス時の動作設定
詳細は[カスタムエラー応答の生成](https://docs.aws.amazon.com/ja_jp/AmazonCloudFront/latest/DeveloperGuide/GeneratingCustomErrorResponses.html)をご参照ください。
```
[
  {
    error_code            = 403  # Required
    error_caching_min_ttl = 300  # Optional
    response_code         = 200  # Optional
    response_page_path    = "/"  # Optional
  }
]
```
DESCRIPTION
  type        = list(map(string))
  default     = []
}

variable "cloudfront_logging_config" {
  description = <<DESCRIPTION
CloudFrontのアクセスログの設定
アクセスログを収集したい場合は`bucket_name`の指定が必須
CloudFormationでの指定方法と同様なため、詳細は[CloudFormationのドキュメント](https://docs.aws.amazon.com/ja_jp/AWSCloudFormation/latest/UserGuide/aws-properties-cloudfront-distribution-logging.html)をご参照ください。
DESCRIPTION
  type        = map(string)
  default = {
    bucket_name     = null
    include_cookies = null
    prefix          = null
  }
}

variable "create_cloudfront_logging_bucket" {
  description = "新規にCloufFrontログ用のS3バケットを作成するか既存のバケットを使用するかを指定するフラグ（trueでバケットを新規に作成）"
  type        = bool
  default     = true
}

variable "cloudfront_logging_bucket_force_destroy" {
  description = "destroy時にbucketとともに保存されているデータを強制的に削除可能にする。`create_cloudfront_logging_bucket`をtrueにしたときにのみ有効。"
  type        = bool
  default     = false
}

variable "enable_s3_access_log" {
  description = "S3へのアクセスをログとして記録する場合、trueを指定する"
  type        = bool
  default     = true
}

variable "s3_access_log_bucket" {
  description = "S3へのアクセスログ保存用S3バケット名"
  type        = string
  default     = null
}

variable "create_s3_access_log_bucket" {
  description = "新規にS3アクセスログ用のS3バケットを作成するか既存のバケットを使用するかを指定するフラグ（trueでバケットを新規に作成）"
  type        = bool
  default     = true
}

variable "s3_access_log_bucket_force_destroy" {
  description = "destroy時にbucketとともに保存されているデータを強制的に削除可能にする。`create_s3_access_log_bucket`をtrueにしたときにのみ有効。"
  type        = bool
  default     = false
}

variable "s3_access_log_object_prefix" {
  description = "S3へのアクセスログ保存時に、オブジェクトに付与するprefix"
  type        = string
  default     = null
}

variable "viewer_response_lambda_function_name" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数の名前"
  type        = string
  default     = "AddSecurityHeaderFunction"
}

variable "viewer_response_lambda_timeout" {
  description = "実行されたLambdaが停止するまでのタイムアウト設定。単位は秒で指定してください。"
  type        = number
  default     = 3
}

variable "viewer_response_lambda_source_dir" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数のソースコードが配置されたディレクトリのパス（デフォルト以外のコードを使用したい場合に指定）"
  type        = string
  default     = null
}

variable "viewer_response_lambda_handler" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数のエントリーポイント"
  type        = string
  default     = "index.handler"
}

variable "viewer_response_lambda_runtime" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数のランタイム"
  type        = string
  default     = "nodejs12.x"
}

variable "viewer_response_lambda_log_retention_in_days" {
  description = <<-DESCRIPTION
  CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数がCloudWatch Logsへ出力するログの保存期間を設定する。

  値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.
  [Resource: aws_cloudwatch_log_group / retention_in_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days)
  DESCRIPTION
  type        = number
  default     = 3653
}

variable "viewer_response_lambda_log_kms_key_id" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数のログデータを暗号化するための、KMS CMKのARNを指定する"
  type        = string
  default     = null
}
