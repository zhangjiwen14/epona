provider "aws" {
  alias = "cache_provider"
}

provider "aws" {
  alias = "origin_provider"
}

# Origin Access Identityを作成
module "cloudfront_origin_access_identity" {
  source = "../../components/cloudfront/origin_access_identity"

  providers = {
    aws = aws.origin_provider
  }

  s3_frontend_bucket_name = var.s3_frontend_bucket_name
}

# S3へのアクセスログを保管するバケット
module "s3_access_log_bucket" {
  source = "../../components/s3/logging_bucket"

  providers = {
    aws = aws.origin_provider
  }

  create_bucket = var.enable_s3_access_log && var.create_s3_access_log_bucket
  bucket        = var.s3_access_log_bucket == null ? "${var.s3_frontend_bucket_name}-access-log" : var.s3_access_log_bucket
  acl           = "log-delivery-write"
  force_destroy = var.s3_access_log_bucket_force_destroy
  tags          = var.tags
}

# 静的ファイルを配置するバケットを作成
module "s3_frontend_bucket" {
  source = "../../components/s3/cloudfront_origin_bucket"

  providers = {
    aws = aws.origin_provider
  }

  cloudfront_origin_access_identity_iam_arn = module.cloudfront_origin_access_identity.iam_arn
  s3_frontend_bucket_name                   = var.s3_frontend_bucket_name
  force_destroy                             = var.s3_frontend_bucket_force_destroy
  enable_logging                            = var.enable_s3_access_log
  logging_bucket                            = module.s3_access_log_bucket.bucket
  logging_prefix                            = var.s3_access_log_object_prefix
  tags                                      = var.tags
}

# CloudFrontへのアクセスログを保管するバケット
module "cloudfront_logging_bucket" {
  source = "../../components/s3/logging_bucket"

  providers = {
    aws = aws.cache_provider
  }

  create_bucket = var.cloudfront_logging_config.bucket_name != null && var.create_cloudfront_logging_bucket
  bucket        = var.cloudfront_logging_config.bucket_name
  force_destroy = var.cloudfront_logging_bucket_force_destroy
  grants = [
    {
      type        = "CanonicalUser"
      permissions = ["FULL_CONTROL"]
      # awslogsdeliveryアカウントのID
      # ref:https://docs.aws.amazon.com/ja_jp/AmazonCloudFront/latest/DeveloperGuide/AccessLogs.html#AccessLogsBucketAndFileOwnership
      id = "c4c1ede66af53448b93c283ce9448c4ba468c9432aa01d700d3878632f77d2d0"
    }
  ]
  tags = var.tags
}

# 証明書作成
module "acm_with_validate_dns" {
  source = "../../components/acm/certificate_with_validate_dns"

  create = true

  providers = {
    aws = aws.cache_provider
  }

  validation_method = "DNS"
  zone_name         = var.zone_name
  domain_name       = var.record_name
  ttl               = 60
}

# CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数が使用するIAM Role"
module "iam_role" {
  source = "../../components/iam/security_header_role"

  providers = {
    aws = aws.cache_provider
  }

  lambda_function_name = var.viewer_response_lambda_function_name
  tags                 = var.tags
}

# CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数が使用するロググループ"
module "log" {
  source = "../../components/cloudwatch/log"

  providers = {
    aws = aws.cache_provider
  }

  lambda_function_name        = var.viewer_response_lambda_function_name
  log_group_retention_in_days = var.viewer_response_lambda_log_retention_in_days
  log_group_kms_key_id        = var.viewer_response_lambda_log_kms_key_id
  tags                        = var.tags
}

# CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数"
module "lambda" {
  source = "../../components/lambda/security_header"

  providers = {
    aws = aws.cache_provider
  }

  lambda_function_name = var.viewer_response_lambda_function_name
  lambda_timeout       = var.viewer_response_lambda_timeout
  lambda_source_dir    = var.viewer_response_lambda_source_dir
  lambda_handler       = var.viewer_response_lambda_handler
  lambda_runtime       = var.viewer_response_lambda_runtime
  iam_role_arn         = module.iam_role.arn

  tags = var.tags
}

# CloudFront作成に必要な変数の準備
locals {
  origin_path = lookup(var.cloudfront_origin, "origin_path", null)
  origin_id   = local.origin_path != null ? "S3-${var.s3_frontend_bucket_name}${var.cloudfront_origin.origin_path}" : "S3-${var.s3_frontend_bucket_name}"
}

# CloudFront Distribution作成
module "cloudfront_distribution" {
  source = "../../components/cloudfront/s3_cache"

  providers = {
    aws = aws.cache_provider
  }

  enabled             = true
  default_root_object = var.cloudfront_default_root_object
  aliases = [
    var.record_name
  ]
  tags            = var.tags
  http_version    = var.cloudfront_http_version
  is_ipv6_enabled = var.cloudfront_is_ipv6_enabled
  price_class     = var.cloudfront_price_class
  web_acl_id      = var.cloudfront_web_acl_id

  origin = {
    domain_name    = module.s3_frontend_bucket.bucket_regional_domain_name
    origin_id      = local.origin_id
    origin_path    = lookup(var.cloudfront_origin, "origin_path", null)
    custom_headers = lookup(var.cloudfront_origin, "custom_headers", [])
    s3_origin_config = {
      origin_access_identity = "origin-access-identity/cloudfront/${module.cloudfront_origin_access_identity.id}"
    }
  }

  default_cache_behavior = {
    target_origin_id           = local.origin_id
    allowed_methods            = lookup(var.cloudfront_default_cache_behavior, "allowed_methods", ["GET", "HEAD"])
    cached_methods             = lookup(var.cloudfront_default_cache_behavior, "cached_methods", ["GET", "HEAD"])
    viewer_protocol_policy     = lookup(var.cloudfront_default_cache_behavior, "viewer_protocol_policy", "redirect-to-https")
    compress                   = lookup(var.cloudfront_default_cache_behavior, "compress", null)
    min_ttl                    = lookup(var.cloudfront_default_cache_behavior, "min_ttl", null)
    default_ttl                = lookup(var.cloudfront_default_cache_behavior, "default_ttl", null)
    max_ttl                    = lookup(var.cloudfront_default_cache_behavior, "max_ttl", null)
    viewer_response_lambda_arn = module.lambda.qualified_arn
  }

  viewer_certificate = {
    acm_certificate_arn      = module.acm_with_validate_dns.certificate_arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = lookup(var.cloudfront_viewer_certificate, "minimum_protocol_version", "TLSv1.2_2019")
  }

  custom_error_responses = var.cloudfront_custom_error_responses

  logging_config = {
    bucket          = var.cloudfront_logging_config.bucket_name != null ? module.cloudfront_logging_bucket.bucket_domain_name : null
    include_cookies = lookup(var.cloudfront_logging_config, "include_cookies", null)
    prefix          = lookup(var.cloudfront_logging_config, "prefix", null)
  }
}

# Route53で取得済みのドメインにCloudFrontへのエイリアスレコードを作成
module "route53_alias" {
  source = "../../components/route53/alias"

  providers = {
    aws = aws.cache_provider
  }

  zone_name   = var.zone_name
  record_name = var.record_name
  alias_target = {
    "dns_name" = module.cloudfront_distribution.domain_name
    "zone_id"  = module.cloudfront_distribution.hosted_zone_id
  }
}
