module "kms" {
  source = "../../components/kms"

  kms_keys = var.kms_keys

  tags = var.tags
}
