module "parameter_store" {
  source = "../../components/ssm/parameter_store"

  parameters = var.parameters

  tags = var.tags
}
