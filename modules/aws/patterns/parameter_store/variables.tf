variable "tags" {
  description = "このモジュールで作られるリソースに共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "parameters" {
  description = "パラメーターを構成する属性（name, type, value, description, tier, key_id, overwrite, allowed_pattern, tags）のリスト"
  type        = list(map(any))
}
