output "s3_ci_bucket_arns" {
  description = "S3バケットの名前とARNのマップ"
  value       = module.build_artifact_buckets.bucket_arns
}

output "container_image_repository_arns" {
  description = "ECRリポジトリの名前とARNのマップ"
  value       = module.container_image_repositories.arns
}

output "container_image_repository_urls" {
  description = "ECRリポジトリの名前とURLのマップ"
  value       = module.container_image_repositories.urls
}
