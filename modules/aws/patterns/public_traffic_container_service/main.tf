module "container_logs" {
  source = "../../components/cloudwatch/log"

  for_each = toset(var.container_log_group_names)

  log_group_name              = each.value
  log_group_retention_in_days = var.container_log_group_retention_in_days
  log_group_kms_key_id        = var.container_log_group_kms_key_id

  tags = merge(
    {
      "Name" = each.value
    },
    var.tags
  )
}

module "default_ecs_task_execution_role" {
  source = "../../components/iam/ecs_task_execution_role"

  create = var.create_default_ecs_task_execution_role

  ecs_task_execution_iam_role_name   = var.default_ecs_task_execution_iam_role_name
  ecs_task_execution_iam_policy_name = var.default_ecs_task_execution_iam_policy_name
}

module "default_ecs_task_role" {
  source = "../../components/iam/ecs_task_role"

  create = var.create_default_ecs_task_role

  ecs_task_iam_role_name   = var.default_ecs_task_iam_role_name
  ecs_task_iam_policy_name = var.default_ecs_task_iam_policy_name
}

module "fargate_container_service" {
  source = "../../components/container/ecs_fargate"

  name = var.name
  tags = var.tags

  cluster_name = var.container_cluster_name

  desired_count    = var.container_service_desired_count
  platform_version = var.container_service_platform_version

  load_balancer_target_group_arn = module.public_traffic_load_balancer.target_group_arn
  container_port                 = var.container_port

  security_groups = [module.container_service_security_group.security_group_id]

  subnets = var.container_subnets

  cpu                = var.container_task_cpu
  memory             = var.container_task_memory
  task_role_arn      = var.container_task_role_arn != null ? var.container_task_role_arn : module.default_ecs_task_role.ecs_task_iam_role_arn
  execution_role_arn = var.container_task_execution_role_arn != null ? var.container_task_execution_role_arn : module.default_ecs_task_execution_role.ecs_task_execution_iam_role_arn

  container_definitions = var.container_definitions
}
