module "users" {
  source  = "../../components/iam/user"
  users   = var.users
  tags    = var.tags
  pgp_key = var.pgp_key
}

module "roles_delivery" {
  source       = "../../components/iam/role"
  admins       = var.admins
  approvers    = var.approvers
  costmanagers = var.costmanagers
  developers   = var.developers
  operators    = var.operators
  viewers      = var.viewers

  additional_admin_role_policies       = var.additional_admin_role_policies
  additional_approver_role_policies    = var.additional_approver_role_policies
  additional_costmanager_role_policies = var.additional_costmanager_role_policies
  additional_developer_role_policies   = var.additional_developer_role_policies
  additional_operator_role_policies    = var.additional_operator_role_policies
  additional_viewer_role_policies      = var.additional_viewer_role_policies

  account_id  = var.account_id
  system_name = var.system_name
  tags        = var.tags

}

module "bind_role_delivery" {
  source           = "../../components/iam/bind_role"
  admins           = var.admins
  approvers        = var.approvers
  costmanagers     = var.costmanagers
  developers       = var.developers
  operators        = var.operators
  viewers          = var.viewers
  account_id       = var.account_id
  environment_name = var.environment_name
  system_name      = var.system_name
}

output "password" {
  value       = module.users.password
  description = "IAMユーザーの暗号化されたパスワード"
}
