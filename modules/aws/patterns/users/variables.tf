variable "users" {
  description = "作成するユーザー一覧"
  type        = list(string)
}

variable "admins" {
  description = "adminへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "approvers" {
  description = "approverへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "costmanagers" {
  description = "costmanagerへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "developers" {
  description = "developerへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "operators" {
  description = "operatorへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "viewers" {
  description = "viewerへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "additional_admin_role_policies" {
  description = "adminロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_approver_role_policies" {
  description = "approverロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_costmanager_role_policies" {
  description = "costmanagerロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_developer_role_policies" {
  description = "developerロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_operator_role_policies" {
  description = "operatorロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "additional_viewer_role_policies" {
  description = "viewerロールに追加するIAMポリシーのARNリスト"
  type        = list(string)
  default     = null
}

variable "account_id" {
  description = "Deliveryu環境のAWSアカウントID"
  type        = string
}

variable "environment_name" {
  description = "スイッチ先の環境名"
  type        = string
}

variable "system_name" {
  description = "システム名"
  type        = string
}

variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "pgp_key" {
  description = "IAMユーザーのパスワード生成で利用するpgpの公開鍵(base64形式)"
  type        = string
}
