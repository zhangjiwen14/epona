module "cloudwatch_logs_subscription_filter" {
  source = "../../components/cloudwatch/log_subscription_filter"

  count = length(var.cloudwatch_log_subscription_filters)

  name            = lookup(var.cloudwatch_log_subscription_filters[count.index], "name", "datadog-forwarder-subscription-filter")
  log_group_name  = lookup(var.cloudwatch_log_subscription_filters[count.index], "log_group_name")
  filter_pattern  = lookup(var.cloudwatch_log_subscription_filters[count.index], "filter_pattern", "")
  destination_arn = lookup(var.cloudwatch_log_subscription_filters[count.index], "destination_arn", var.datadog_forwarder_lambda_arn)
  role_arn        = lookup(var.cloudwatch_log_subscription_filters[count.index], "role_arn", null)
}

module "logging_s3_bucket_notification" {
  source = "../../components/s3/notification"

  count = length(var.logging_s3_bucket_notifications)

  bucket = lookup(var.logging_s3_bucket_notifications[count.index], "bucket")

  lambda_function_notifications = [
    {
      id                  = lookup(var.logging_s3_bucket_notifications[count.index], "id", null)
      lambda_function_arn = lookup(var.logging_s3_bucket_notifications[count.index], "lambda_function_arn", var.datadog_forwarder_lambda_arn)
      events              = lookup(var.logging_s3_bucket_notifications[count.index], "events", "s3:ObjectCreated:*")
      filter_prefix       = lookup(var.logging_s3_bucket_notifications[count.index], "filter_prefix", null)
      filter_suffix       = lookup(var.logging_s3_bucket_notifications[count.index], "filter_suffix", null)
    }
  ]
}
