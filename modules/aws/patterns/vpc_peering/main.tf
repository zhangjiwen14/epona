module "vpc_peering" {
  source = "../../components/vpc/peering"

  name             = var.name
  requester_vpc_id = var.requester_vpc_id
  accepter_vpc_id  = var.accepter_vpc_id

  tags = var.tags
}
