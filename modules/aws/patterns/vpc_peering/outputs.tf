output "peering_id" {
  description = "VPCピアリングのID"
  value       = module.vpc_peering.peering_id
}
