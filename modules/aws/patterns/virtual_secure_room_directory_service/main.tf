module "directory_service" {
  source = "../../components/directory_service"

  tags = var.tags

  name       = var.directory_service_domain_name
  password   = var.directory_service_domain_password
  short_name = var.directory_service_short_name
  vpc_id     = var.directory_service_vpc_id
  subnet_ids = var.directory_service_subnet_ids

}

module "ad_manager" {
  source = "../../components/ec2/ad_manager"

  name = var.ad_manager_name
  tags = var.tags

  administrator_password = var.ad_manager_administrator_password
  ami                    = var.ad_manager_ami
  instance_type          = var.ad_manager_instance_type
  iam_instance_profile   = module.ad_manager_iam.iam_instance_profile_name
  subnet_id              = var.ad_manager_subnet_id
  vpc_security_group_ids = [module.ad_manager_sg.security_group_id]
  root_block_volume_size = var.ad_manager_root_block_volume_size

  ssm_directory_service_id = module.directory_service.id
  ssm_domain_name          = var.directory_service_domain_name
  ssm_ou                   = var.directory_service_ou
  ssm_dns_ip_addresses     = module.directory_service.dns_ip_addresses
}

module "ad_manager_iam" {
  source = "../../components/iam/ad_manager_instance_profile"

  tags = var.tags

  role_name             = var.ad_manager_role_name
  instance_profile_name = var.ad_manager_instance_profile_name
}

module "ad_manager_sg" {
  source = "../../components/security_group/generic"

  name = var.ad_manager_sg_name
  tags = var.tags

  vpc_id = var.directory_service_vpc_id

  ingresses = var.ad_manager_sg_ingresses

  egresses = [{
    port        = -1
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "AD Manager outbound SG Rule"
  }]
}