output "directory_id" {
  description = "Diretory ServiceのID"
  value       = module.directory_service.id
}

output "admanager_public_dns" {
  description = "AD Managerのパブリックホスト名"
  value       = module.ad_manager.public_dns
}

output "admanager_public_ip" {
  description = "AD ManagerのパブリックIPアドレス"
  value       = module.ad_manager.public_ip
}

output "admanager_login_user" {
  description = "ドメインのAdminでAD Managerへログインする際のユーザー名"
  value       = "Admin@${var.directory_service_domain_name}"
}

output "admanager_login_password" {
  description = "ドメインのAdminに設定している初期パスワード"
  value       = var.directory_service_domain_password
}
