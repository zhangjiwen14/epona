output "instance_identifier" {
  description = "RDSインスタンスの識別子"
  value       = module.rds.instance_identifier
}

output "instance_name" {
  description = "データベース名"
  value       = module.rds.instance_name
}

output "instance_username" {
  description = "ユーザー名"
  value       = module.rds.instance_username
}

output "instance_address" {
  description = "RDSインスタンスへアクセスするためのアドレス"
  value       = module.rds.instance_address
}

output "instance_port" {
  description = "RDSインスタンスへアクセスするためのポート"
  value       = module.rds.instance_port
}

output "instance_endpoint" {
  description = "RDSインスタンスのエンドポイント"
  value       = module.rds.instance_endpoint
}

output "instance_engine" {
  description = "RDSインスタンスのデータベースエンジン"
  value       = module.rds.instance_engine
}

output "instance_engine_version" {
  description = "RDSインスタンスのデータベースエンジンのバージョン"
  value       = module.rds.instance_engine_version
}

output "instance_log_group_names" {
  description = "RDSインスタンスのロググループ名のリスト"
  value       = [for log in module.rds_cloudwatch_log : log.log_group_name]
}

output "instance_security_group_id" {
  description = "RDSインスタンスのセキュリティグループのID"
  value       = module.rds_simple_security_group.security_group_id
}

output "monitoring_log_group_name" {
  description = "拡張モニタリングが有効な場合、拡張モニタリングメトリクスを含むCloudWatch Logsロググループ名を返却する。ただし、このロググループは、各RDSインスタンスで共有となる"
  value       = var.monitoring_interval > 0 ? "RDSOSMetrics" : ""
}
