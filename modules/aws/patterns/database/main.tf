data "aws_vpc" "this" {
  id = var.vpc_id
}

locals {
  create_new_security_group = length(var.security_groups) == 0
}

module "rds_simple_security_group" {
  source = "../../components/security_group/generic"

  name = "${var.name}-security-group"
  tags = var.tags

  create = local.create_new_security_group

  vpc_id = var.vpc_id

  ingresses = [{
    port        = var.port
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.this.cidr_block]
    description = "RDS inbound SG Rule"
  }]

  egresses = []
}

module "source_security_group_attach_rule" {
  source = "../../components/security_group/attach_rule"

  target_security_group_id = module.rds_simple_security_group.security_group_id

  sg_ingresses = [for source_security_group_id in var.source_security_group_ids : {
    port              = var.port
    protocol          = "tcp"
    security_group_id = source_security_group_id
    description       = "Enable access RDS from SG Rule"
  }]
}

module "rds_enhanced_monitoring_role" {
  source = "../../components/iam/rds_enhanced_monitoring_role"

  create = var.monitoring_interval > 0 ? var.monitoring_role_arn == null : false

  rds_enhanced_monitoring_role_name = "${title(var.name)}RdsEnhancedMonitoringRole"
  tags                              = var.tags
}

locals {
  default_exportable_cloudwatch_logs_by_rdbms = {
    "mysql"     = ["audit", "error", "general", "slowquery"]
    "mariadb"   = ["audit", "error", "general", "slowquery"]
    "postgres"  = ["postgresql", "upgrade"]
    "oracle"    = ["alert", "audit", "listener", "trace"]
    "sqlserver" = ["agent", "error"]
  }

  database_type = split("-", var.engine)[0] # normalize, "oracle-xx", "sqlserver-xx" => "oracle", "sqlserver"

  exportable_cloudwatch_logs = var.enabled_cloudwatch_logs_exports == null ? local.default_exportable_cloudwatch_logs_by_rdbms[local.database_type] : var.enabled_cloudwatch_logs_exports
}

module "rds_cloudwatch_log" {
  source = "../../components/cloudwatch/log"

  for_each = toset(local.exportable_cloudwatch_logs)

  log_group_name              = "/aws/rds/instance/${var.identifier}/${each.value}"
  log_group_retention_in_days = var.cloudwatch_logs_retention_in_days
  log_group_kms_key_id        = var.cloudwatch_logs_kms_key_id

  tags = merge(
    {
      "Name" = "${var.identifier}-logs-${each.value}"
    },
    var.tags
  )
}

module "rds" {
  source = "../../components/datastore/rds"

  name = var.name
  tags = var.tags

  identifier = var.identifier

  engine         = var.engine
  engine_version = var.engine_version

  port = var.port

  instance_class        = var.instance_class
  storage_type          = var.storage_type
  allocated_storage     = var.allocated_storage
  max_allocated_storage = var.max_allocated_storage
  iops                  = var.iops

  username = var.username
  password = var.password

  availability_zone = var.availability_zone
  multi_az          = var.multi_az

  security_groups = length(var.security_groups) > 0 ? var.security_groups : [module.rds_simple_security_group.security_group_id]

  deletion_protection = var.deletion_protection

  allow_major_version_upgrade = var.allow_major_version_upgrade
  auto_minor_version_upgrade  = var.auto_minor_version_upgrade

  maintenance_window = var.maintenance_window

  apply_immediately = var.apply_immediately

  skip_final_snapshot       = var.skip_final_snapshot
  final_snapshot_identifier = var.final_snapshot_identifier != null ? var.final_snapshot_identifier : "final-${var.identifier}-snapshot"
  copy_tags_to_snapshot     = var.copy_tags_to_snapshot

  db_subnet_group_name = var.db_subnet_group_name
  db_subnets           = var.db_subnets

  parameter_group_name   = var.parameter_group_name
  parameter_group_family = var.parameter_group_family
  parameters             = var.parameters

  option_group_name                 = var.option_group_name
  option_group_engine_name          = var.option_group_engine_name
  option_group_major_engine_version = var.option_group_major_engine_version
  option_group_options              = var.option_group_options

  monitoring_interval = var.monitoring_interval
  monitoring_role_arn = var.monitoring_interval > 0 ? (var.monitoring_role_arn != null ? var.monitoring_role_arn : module.rds_enhanced_monitoring_role.rds_enhanced_monitoring_role_arn) : null

  performance_insights_enabled          = var.performance_insights_enabled
  performance_insights_kms_key_id       = var.performance_insights_kms_key_id
  performance_insights_retention_period = var.performance_insights_retention_period

  exportable_cloudwatch_logs = local.exportable_cloudwatch_logs

  storage_encrypted = var.storage_encrypted
  kms_key_id        = var.kms_key_id

  ca_cert_identifier = var.ca_cert_identifier

  character_set_name = var.character_set_name

  license_model = var.license_model

  mssql_timezone = var.mssql_timezone

  backup_retention_period = var.backup_retention_period
  backup_window           = var.backup_window

  snapshot_identifier = var.snapshot_identifier
}


