variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "lambda_function_name" {
  description = "検出結果を送信するLambda関数の名前"
  type        = string
}

variable "lambda_timeout" {
  description = "実行されたLambdaが停止するまでのタイムアウト設定。単位は秒で指定してください。"
  type        = number
  default     = 3
}

variable "vpc_id" {
  description = "このモジュールで作成するLambdaが配置される、VPCのID"
  type        = string
}

variable "lambda_subnet_ids" {
  description = "Lambdaを配置するサブネットのIDリスト。参考：[高可用性関数のために](https://aws.amazon.com/jp/premiumsupport/knowledge-center/internet-access-lambda-function/)"
  type        = list(string)
}

variable "notification_lambda_config" {
  description = <<-EOT
検出結果を送信するLambda関数の設定値。
`type`: チャットサービス["slack" | "teams"]
endpoints: エンドポイントのurl情報
`low`/`medium`/`high`: それぞれの脅威度毎のIncoming Webhookのパス（通知しない場合は未設定もしくは`null`を指定）

以下、設定例です。

```
{
  type   = "slack"
  endpoints = {
    medium = "https://hooks.slack.com/xxxx/xxxx"
    high   = "https://hooks.slack.com/yyyy/yyyy"
  }
}
```
EOT
  type = object({
    type      = string,
    endpoints = map(string)
  })
  default = {
    type = "slack"
    endpoints = {
      low    = null,
      medium = null,
      high   = null
    }
  }
}

variable "notification_lambda_source_dir" {
  description = "検出結果を送信するLambda関数のソースコードが配置されたディレクトリのパス"
  type        = string
  default     = null
}

variable "notification_lambda_handler" {
  description = "検出結果を送信するLambda関数のエントリーポイント"
  type        = string
  default     = "index.handler"
}

variable "notification_lambda_runtime" {
  description = "検出結果を送信するLambda関数のランタイム"
  type        = string
  default     = "nodejs12.x"
}

variable "log_retention_in_days" {
  description = <<-DESCRIPTION
  検出結果を送信するLambda関数のログの記録先である、CloudWatch Logsの保存期間を設定する。

  値は、次の範囲の値から選ぶこと： 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653.
  [Resource: aws_cloudwatch_log_group / retention_in_days](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group#retention_in_days)
  DESCRIPTION
  type        = number
  default     = null
}

variable "log_kms_key_id" {
  description = "通知先へ検知を送信するLambda関数のログ出力先である、CloudWatch Logsを暗号化するためのKMS CMKのARN"
  type        = string
  default     = null
}

variable "create_guardduty" {
  description = "Amazon GuardDutyを新規に作成する場合はtrue(すでに有効になっている場合は、falseを指定してください)"
  type        = bool
  default     = true
}

variable "finding_publishing_frequency" {
  description = "既存の結果の再検出時に通知する頻度。選択可能な値は、[GuardDuty APIリファレンス](https://docs.aws.amazon.com/guardduty/latest/APIReference/API_CreateDetector.html#API_CreateDetector_RequestSyntax)を参照してください。"
  type        = string
  default     = "SIX_HOURS"
}
