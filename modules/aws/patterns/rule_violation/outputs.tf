output "lambda_function_rule_violation_log_group_name" {
  description = "Lambda関数のログ出力先となる、CloudWatch Logsロググループ名"
  value       = module.log.log_group_name
}
