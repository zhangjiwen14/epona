module "awsconfigdeiverychannel" {
  source                                        = "../../components/awsconfig/rule_violation"
  s3_bucket_name                                = var.s3_bucket_name
  aws_config_role_arn                           = module.awsconfigiam.aws_config_role_arn
  aws_config_lambda_arn                         = module.awsconfiglambda.lambda_function_rule_violation_arn
  notification_topic_name                       = var.notification_topic_name
  aws_config_channel_name                       = var.aws_config_channel_name
  aws_config_recorder_name                      = var.aws_config_recorder_name
  s3_bucket_force_destroy                       = var.s3_bucket_force_destroy
  bucket_transitions                            = var.bucket_transitions
  recorder_status                               = var.recorder_status
  delivery_frequency                            = var.delivery_frequency
  configrule_prefix                             = var.configrule_prefix
  configrule_access_key_max_age                 = var.configrule_access_key_max_age
  configrule_access_key_frequency               = var.configrule_access_key_frequency
  configrule_acm_days_to_expiration             = var.configrule_acm_days_to_expiration
  configrule_acm_certificate_frequency          = var.configrule_acm_certificate_frequency
  configrule_cloud_trail_encryption_frequency   = var.configrule_cloud_trail_encryption_frequency
  configrule_cloud_trail_enabled_frequency      = var.configrule_cloud_trail_enabled_frequency
  configrule_cloud_trail_validation_frequency   = var.configrule_cloud_trail_validation_frequency
  configrule_password_max_age                   = var.configrule_password_max_age
  configrule_password_min_length                = var.configrule_password_min_length
  configrule_password_reuse_prevention          = var.configrule_password_reuse_prevention
  configrule_password_require_lower_case        = var.configrule_password_require_lower_case
  configrule_password_require_numbers           = var.configrule_password_require_numbers
  configrule_password_require_symbols           = var.configrule_password_require_symbols
  configrule_password_require_upper_case        = var.configrule_password_require_upper_case
  configrule_password_policy_frequency          = var.configrule_password_policy_frequency
  configrule_iam_root_access_key_frequency      = var.configrule_iam_root_access_key_frequency
  configrule_iam_mfa_enabled_frequency          = var.configrule_iam_mfa_enabled_frequency
  configrule_cloud_trail_multi_region_frequency = var.configrule_cloud_trail_multi_region_frequency
  configrule_root_mfa_frequency                 = var.configrule_root_mfa_frequency
  configrule_s3_public_read_frequency           = var.configrule_s3_public_read_frequency
  configrule_s3_public_write_frequency          = var.configrule_s3_public_write_frequency
  configrule_waf_logging_frequency              = var.configrule_waf_logging_frequency
  enable_default_rule                           = var.enable_default_rule
  create_config_recorder_delivery_channel       = var.create_config_recorder_delivery_channel
  all_supported                                 = var.all_supported
  include_grobal_resources                      = var.include_grobal_resources
  recording_resource_types                      = var.recording_resource_types
}

module "awsconfigiam" {
  source                 = "../../components/iam/rule_violation"
  function_name          = var.function_name
  aws_config_role_name   = var.aws_config_role_name
  tags                   = var.tags
  aws_config_bucket_arn  = module.awsconfigdeiverychannel.aws_config_bucket_arn
  notification_topic_arn = module.awsconfigdeiverychannel.notification_topic_arn
}

module "awsconfiglambda" {
  source                         = "../../components/lambda/rule_violation"
  tags                           = var.tags
  function_name                  = var.function_name
  notification_type              = var.notification_type
  notification_channel           = var.notification_channel
  notification_lambda_source_dir = var.notification_lambda_source_dir
  notification_lambda_handler    = var.notification_lambda_handler
  notification_lambda_runtime    = var.notification_lambda_runtime
  notification_lambda_timeout    = var.notification_lambda_timeout
  iam_role_rule_violation_arn    = module.awsconfigiam.iam_role_rule_violation_arn
  notification_topic_arn         = module.awsconfigdeiverychannel.notification_topic_arn
}

module "log" {
  source = "../../components/cloudwatch/log"

  lambda_function_name        = var.function_name
  log_group_retention_in_days = var.log_group_retention_in_days
  log_group_kms_key_id        = var.log_group_kms_key_id

  tags = var.tags
}
