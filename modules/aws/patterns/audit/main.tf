data "aws_iam_policy_document" "kms_key_policy" {
  source_json   = data.aws_iam_policy_document.trail_kms_access_policy.json
  override_json = var.kms_custom_key_policy != null ? var.kms_custom_key_policy : ""
}

module "kms" {
  source = "../../components/kms"

  kms_keys = [{
    alias_name               = var.kms_key_alias_name
    description              = "KMS to encrypt CloudTrail logs"
    key_usage                = "ENCRYPT_DECRYPT"
    customer_master_key_spec = "SYMMETRIC_DEFAULT"
    policy                   = data.aws_iam_policy_document.kms_key_policy.json
    deletion_window_in_days  = var.kms_key_deletion_in_days
    is_enabled               = true
    enable_key_rotation      = false
  }]

  tags = var.kms_key_tags
}

module "cloudtrail" {
  source = "../../components/cloudtrail"

  # basis
  name                  = var.trail_name
  is_multi_region_trail = var.is_multi_region_trail
  tags                  = var.trail_tags

  # listen event
  read_write_type = var.event_read_write_type
  data_resources  = var.event_data_resources

  # bucket settings
  bucket_name          = var.bucket_name
  bucket_force_destroy = var.bucket_force_destroy
  bucket_mfa_delete    = var.bucket_mfa_delete
  bucket_transitions   = var.bucket_transitions

  kms_key_arn = module.kms.keys[var.kms_key_alias_name].key_arn
}
