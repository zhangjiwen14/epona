module "workspaces" {
  source = "../../components/workspaces"

  tags = var.tags

  directory_id                              = var.workspaces_directory_id
  subnet_ids                                = var.workspaces_subnet_ids
  volume_encryption_key                     = var.workspaces_kms_key
  user_names                                = var.workspaces_user_names
  bundle_id                                 = var.workspaces_bundle_id
  compute_type_name                         = var.workspaces_compute_type_name
  user_volume_size_gib                      = var.workspaces_user_volume_size_gib
  root_volume_size_gib                      = var.workspaces_root_volume_size_gib
  running_mode                              = var.workspaces_running_mode
  running_mode_auto_stop_timeout_in_minutes = var.workspaces_running_mode_auto_stop_timeout_in_minutes
  rules                                     = var.workspaces_access_source_addresses
}
