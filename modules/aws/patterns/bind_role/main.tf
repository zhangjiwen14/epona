module "bind_role" {
  source           = "../../components/iam/bind_role"
  admins           = var.admins
  approvers        = var.approvers
  costmanagers     = var.costmanagers
  developers       = var.developers
  operators        = var.operators
  viewers          = var.viewers
  account_id       = var.account_id
  environment_name = var.environment_name
  system_name      = var.system_name
}
