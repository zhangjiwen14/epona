
variable "admins" {
  description = "adminへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "approvers" {
  description = "approverへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "costmanagers" {
  description = "costmanagerへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "developers" {
  description = "developerへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "operators" {
  description = "operatorへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "viewers" {
  description = "viewerへスイッチロール可能なユーザー一覧"
  type        = list(string)
}

variable "account_id" {
  description = "ユーザーがAssume role可能なAWSアカウントID"
  type        = string
}

variable "environment_name" {
  description = "スイッチ先の環境名"
  type        = string
}

variable "system_name" {
  description = "システム名"
  type        = string
}
