data "aws_vpc" "this" {
  id = var.vpc_id
}

module "elasticache_redis_simple_security_group" {
  source = "../../components/security_group/generic"

  name = "${var.replication_group_id}-security-group"
  tags = var.tags

  create = length(var.security_groups) == 0

  vpc_id = var.vpc_id

  ingresses = [{
    port        = var.port
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.this.cidr_block]
    description = "ElastiCache for Redis inbound SG Rule"
  }]

  egresses = []
}

module "source_security_group_attach_rule" {
  source = "../../components/security_group/attach_rule"

  target_security_group_id = module.elasticache_redis_simple_security_group.security_group_id

  sg_ingresses = [for source_security_group_id in var.source_security_group_ids : {
    port              = var.port
    protocol          = "tcp"
    security_group_id = source_security_group_id
    description       = "Enable access Redis from SG Rule"
  }]
}

module "elasticache_redis" {
  source = "../../components/datastore/elasticache_redis"

  replication_group_id          = var.replication_group_id
  replication_group_description = var.replication_group_description

  tags = var.tags

  engine_version = var.engine_version
  node_type      = var.node_type

  port = var.port

  cluster_mode_enabled  = var.cluster_mode_enabled
  number_cache_clusters = var.number_cache_clusters
  availability_zones    = var.availability_zones

  automatic_failover_enabled = var.automatic_failover_enabled

  at_rest_encryption_enabled = var.at_rest_encryption_enabled
  transit_encryption_enabled = var.transit_encryption_enabled

  auth_token = var.auth_token
  kms_key_id = var.kms_key_id

  snapshot_name            = var.snapshot_name
  snapshot_retention_limit = var.snapshot_retention_limit
  snapshot_window          = var.snapshot_window

  maintenance_window     = var.maintenance_window
  notification_topic_arn = var.notification_topic_arn
  apply_immediately      = var.apply_immediately

  cluster_mode_replicas_per_node_group = var.cluster_mode_replicas_per_node_group
  cluster_mode_num_node_groups         = var.cluster_mode_num_node_groups

  security_groups = length(var.security_groups) == 0 ? [module.elasticache_redis_simple_security_group.security_group_id] : var.security_groups

  family = var.family

  parameters = var.parameters
  subnets    = var.subnets
}
