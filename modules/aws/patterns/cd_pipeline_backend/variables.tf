variable "pipeline_name" {
  description = "デプロイメントパイプラインに付与する名前"
  type        = string
}

variable "tags" {
  description = "作成するリソースに共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "delivery_account_id" {
  description = "Delivery環境のAWSアカウントID"
  type        = string
}

variable "source_bucket_name" {
  description = "デプロイ用の設定ファイルを配置するバケット名"
  type        = string
}

variable "deployment_require_approval" {
  description = "デプロイに管理者の承認を必要とするか"
  type        = bool
  default     = true
}

variable "deployment_config_name" {
  description = "CodeDeployが使用するデプロイ設定名。指定できる値については、[CodeDeploy でデプロイ設定を使用する](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/deployment-configurations.html)の`Amazon ECS コンピューティングプラットフォームでの事前定義されたデプロイ設定`を参照。"
  default     = "CodeDeployDefault.ECSAllAtOnce"
  type        = string
}

variable "deployment_auto_rollback_events" {
  description = "デプロイの自動ロールバックを行うイベント名のリスト。指定できるイベント名については[AutoRollbackConfiguration](https://docs.aws.amazon.com/codedeploy/latest/APIReference/API_AutoRollbackConfiguration.html)の`events`欄を参照。"
  type        = list(string)
  default     = ["DEPLOYMENT_FAILURE"]
}

variable "deployment_action_on_timeout" {
  description = "新環境へトラフィックをルーティングするタイミング指定。指定できる値については[DeploymentReadyOption](https://docs.aws.amazon.com/codedeploy/latest/APIReference/API_DeploymentReadyOption.html)を参照。"
  type        = string
  default     = "CONTINUE_DEPLOYMENT"
}

variable "deployment_wait_time_in_minutes" {
  description = "`deployment_action_on_timeout`に`STOP_DEPLOYMENT`が指定された場合、手動でのルーティング切り替えが指示されなかったときにデプロイを失敗させるタイムアウト指定。単位は分。"
  type        = number
  default     = 10
}

variable "deployment_setting_key" {
  description = "CodeDeployが使用する設定ファイル群(zip)のS3 Object Key"
  type        = string
  default     = "settings.zip"
}

variable "termination_wait_time_in_minutes" {
  description = "Blue/Green Deploymentが成功したとき、何分待って旧環境のサービスを終了させるか"
  type        = number
  default     = 1
}

variable "artifact_store_bucket_name" {
  type        = string
  description = <<DESCRIPTION
CodePipelineの[アーティファクト](https://docs.aws.amazon.com/ja_jp/codepipeline/latest/userguide/concepts.html#concepts-artifacts)を管理するS3 bucket名
未設定の場合、 "[pipeline_name]-artifact-store" でS3 bucketを作成する。
DESCRIPTION
  default     = ""
}

variable "artifact_store_bucket_force_destroy" {
  type        = bool
  description = "アーティファクトストアとして使っているS3 bucketを強制的に削除可能にするか否か"
  default     = false
}

variable "artifact_store_bucket_transitions" {
  description = <<-DESCRIPTION
  アーティファクトストア用S3バケットの移行に対するポリシー設定。最新でなくなったファイルに対して適用される。
  `days`キーに対しては、最新でなくなってから何日経過したコンテンツに対して適用するかを値として指定する。
  `storage_class`キーに対しては`ONEZONE_IA`、`STANDARD_IA`、`INTELLIGENT_TIERING`、`GLACIER`、`DEEP_ARCHIVE`のいずれかを指定する。
  
  未設定の場合は30日後にAmazon S3 Glacierへ移行するルールが適用される。
  DESCRIPTION
  type        = list(map(string))
  default = [{
    days          = 30
    storage_class = "GLACIER"
  }]
}

variable "task_definition_template_file" {
  description = "ECSの[タスク定義](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/task_definitions.html)ファイル名"
  type        = string
  default     = "taskdef.json"
}

variable "appspec_template_file" {
  description = "CodeDeployが使用する[アプリケーション仕様ファイル](https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/application-specification-files.html)名"
  type        = string
  default     = "appspec.yaml"
}

variable "target_group_names" {
  description = "Blue-Green Deploymentに使用するターゲットグループ名のリスト"
  type        = list(string)
}

variable "cluster_name" {
  description = "デプロイ対象のAmazon ECSクラスター名"
  type        = string
}

variable "service_name" {
  description = "デプロイ対象のAmazon ECSサービス名"
  type        = string
}

variable "prod_listener_arns" {
  description = "稼働環境用トラフィックを受け持つ、ロードバランサーのリスナーARNのリスト"
  type        = list(string)
}

variable "codedeploy_app_name" {
  type = string
}

variable "deployment_group_name_ecs" {
  type        = string
  description = "ECSへのデプロイに使用するデプロイメントグループ名"
}

variable "cross_account_codepipeline_access_role_arn" {
  description = "Runtime環境のCodePipelineから利用するクロスアカウントアクセス用RoleのARN。"
  type        = string
}

variable "ecr_repositories" {
  description = <<-DESCRIPTION
  デプロイ対象のECRリポジトリ名とobjectのマップ。objectについては、
  `tag`キーの値がデプロイするコンテナイメージのタグ名、
  `artifact_name`キーの値は当該リポジトリのイメージが対応するアーティファクト名、
  `container_name`キーの値は、[タスク定義](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/task_definitions.html)ファイル上のコンテナ名を示す。
  
  リポジトリとしては4つまで指定可能。ただし、なお、複数のリポジトリを指定する場合、`artifact_name`については重複しない名称を指定すること。
  DESCRIPTION
  type = map(object({
    tag            = string
    artifact_name  = string
    container_name = string
  }))
}
