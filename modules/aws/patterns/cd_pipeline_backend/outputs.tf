output "artifact_store_bucket_arn" {
  description = "Artifact store用bucketのARN"
  value       = module.codepipeline.artifact_store_bucket_arn
}

output "deployment_setting_key" {
  description = "デプロイ設定ファイルのS3 Bucket Key"
  value       = module.codepipeline.deployment_setting_key
}

output "kms_keys" {
  description = "作成されたKMSの鍵"
  value       = module.kms_key
}
