data "aws_arn" "source_bucket" {
  arn = var.source_bucket_arn
}

# Deliviery環境上におけるCodePipeline用ロール
module "cross_account_codepipeline_role_frontend" {
  source = "../../components/iam/cross_account_codepipeline_frontend/"

  pipeline_name      = var.pipeline_name
  runtime_account_id = var.runtime_account_id
  source_bucket_arn  = var.source_bucket_arn

  artifact_store_bucket_arn                = var.artifact_store_bucket_arn
  artifact_store_bucket_encryption_key_arn = var.artifact_store_bucket_encryption_key_arn
}

module "event_bus_role" {
  source = "../../components/iam/event_bus/sender/"

  source_name          = var.pipeline_name
  target_event_bus_arn = var.target_event_bus_arn
}

# S3 に PutObject されたら Event Bus にイベントを送信する
module "s3_event_bus_sender" {
  source = "../../components/cloudwatch/event_bus/sender/"

  target_event_bus_arn        = var.target_event_bus_arn
  cloudwatch_event_rule_names = [aws_cloudwatch_event_rule.s3.name]
  role_arn                    = module.event_bus_role.arn
}

resource "aws_cloudwatch_event_rule" "s3" {
  name          = format("FireCodePipeline-%s", var.pipeline_name)
  description   = "Amazon CloudWatch Events rule to automatically start your pipeline when a PUSH occurs in the Amazon S3."
  event_pattern = <<-JSON
  {
    "source": [
      "aws.s3"
    ],
    "detail-type": [
      "AWS API Call via CloudTrail"
    ],
    "detail": {
        "eventSource": [
          "s3.amazonaws.com"
        ],
        "eventName": [
          "PutObject"
        ],
        "requestParameters": {
          "bucketName": [
            ${jsonencode(data.aws_arn.source_bucket.resource)}
          ],
          "key": [
            ${jsonencode(var.deployment_source_object_key)}
          ]
        }
    }
  }
  JSON
}
