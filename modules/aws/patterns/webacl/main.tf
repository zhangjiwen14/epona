module "webacl" {
  source = "../../components/webacl"

  default_action             = var.default_action
  name                       = var.name
  scope                      = var.scope
  rule_name_prefix           = "${var.name}-rule"
  rule_groups                = var.rule_groups
  cloudwatch_metrics_enabled = var.web_acl_cloudwatch_metrics_enabled
  metric_name                = "${var.name}-metric-name"
  sampled_requests_enabled   = var.web_acl_sampled_requests_enabled
  resource_arn               = var.resource_arn
  tags                       = var.tags
}

module "s3" {
  source = "../../components/s3/logging_bucket"

  bucket        = var.s3_logging_bucket_name
  create_bucket = var.create_logging_bucket
  force_destroy = var.s3_logging_bucket_force_destroy
  tags          = var.tags
}

module "iam_role" {
  source = "../../components/iam/waf_logging_firehose_role"

  name_suffix       = var.name
  allow_bucket_name = var.s3_logging_bucket_name
  tags              = var.tags
}

module "kinesis_data_firehose" {
  source = "../../components/kinesis_data_firehose/waf_logging"

  name_suffix        = var.name
  prefix             = var.logging_prefix
  compression_format = var.logging_compression_format
  waf_webacl_arn     = module.webacl.webacl_arn
  iam_role_arn       = module.iam_role.arn
  bucket_arn         = module.s3.arn
  tags               = var.tags
}
