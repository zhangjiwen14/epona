variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "default_action" {
  description = "WebACLに設定するデフォルトアクション（`allow` or `block`）"
  type        = string
}

variable "name" {
  description = "作成するリソースに付ける名前"
  type        = string
}

variable "scope" {
  description = "CloudFront用かリージョンアプリケーション用かの指定（`CLOUDFRONT` or `REGIONAL`）"
  type        = string
}

variable "rule_groups" {
  description = <<DESCRIPTION
WebACLに関連付けるルールグループ（List形式で複数指定可、記載順に適用される）
```
rule_groups = [
  {
    arn                        = "dummy"  # WebACLに設定するルールグループのARN（作成するWebACLと同一のリージョンに作成済みのルールグループのみ指定可）
    override_action            = "none"   # none: 元々のルールグループのアクションをそのまま適用する。count: 一致したWebリクエストのカウントのみを行うようにする。
    cloudwatch_metrics_enabled = false    # このルールグループのCloudWatchによる件数モニタリングを有効化する
    sampled_requests_enabled   = false    # このルールグループのルールに一致した過去3時間分のWebリクエストの保存を有効化する
  }
]
```
DESCRIPTION
  type        = list(map(any))
  default     = []
}

variable "web_acl_cloudwatch_metrics_enabled" {
  description = "WebACLのCloudWatchによる件数モニタリングを有効化する"
  type        = bool
  default     = true
}

variable "web_acl_sampled_requests_enabled" {
  description = "ルールに一致した過去3時間分のWebリクエストの保存を有効化する"
  type        = bool
  default     = true
}

variable "resource_arn" {
  description = "WAFを関連付けるリソースのARN（ここでのCloudFrontの指定は不可）"
  type        = string
  default     = null
}

variable "create_logging_bucket" {
  description = "新規にS3バケットを作成するか既存のバケットを使用するかを指定するフラグ（trueでバケットを新規に作成）"
  type        = bool
  default     = true
}

variable "s3_logging_bucket_name" {
  description = "ロギングに使用するバケットの名前"
  type        = string
}

variable "s3_logging_bucket_force_destroy" {
  description = "destroy時、データがあったとしても強制的にS3バケットを削除する"
  type        = bool
  default     = false
}

variable "logging_prefix" {
  description = "S3に配置する際にパスに付与するPrefix"
  type        = string
  default     = null
}

variable "logging_compression_format" {
  description = "S3に配置するログの圧縮フォーマット（`無圧縮(null)` or `GZIP` or `ZIP` or `Snappy`）"
  type        = string
  default     = null
}
