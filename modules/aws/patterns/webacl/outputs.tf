output "webacl_arn" {
  description = "作成されたWebACLのARN"
  value       = module.webacl.webacl_arn
}

output "webacl_traffic_log_bucket_id" {
  description = "ログ出力用のS3バケットのID"
  value       = module.s3.bucket_id
}

output "webacl_traffic_log_bucket" {
  description = "ログ出力用のS3バケット名"
  value       = module.s3.bucket
}
