variable "name" {
  description = "構成するデプロイメントパイプライン名"
  type        = string
}

variable "bucket_name" {
  description = "デプロイ用の設定ファイルを配置するS3バケット名"
  type        = string
}

variable "bucket_force_destroy" {
  description = "デプロイ用の設定ファイルを配置するS3バケットを強制的に削除可能にするか否か"
  type        = bool
  default     = false
}

variable "tags" {
  description = "デプロイメントパイプラインに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "bucket_noncurrent_version_transitions" {
  description = <<-DESCRIPTION
  バケットの移行に対するポリシー設定。最新でなくなったファイルに対して適用される。
  `days`キーに対しては、最新でなくなってから何日経過したコンテンツに対して適用するかを値として指定する。
  `storage_class`キーに対しては`ONEZONE_IA`、`STANDARD_IA`、`INTELLIGENT_TIERING`、`GLACIER`、`DEEP_ARCHIVE`のいずれかを指定する。

  未設定の場合は30日後にAmazon S3 Glacierへ移行するルールが適用される。
  DESCRIPTION
  type        = list(map(string))
  default = [{
    days          = 30
    storage_class = "GLACIER"
  }]
}

variable "runtime_account_id" {
  description = "Runtime環境のAWSアカウントID"
  type        = string
}

variable "ecr_repositories" {
  description = <<-DESCRIPTION
  リポジトリ名がキー、値がobjectとなるマップ。
  objectとしては、`tag`キーに対してデプロイ対象となるイメージタグを1つだけ指定し、`arn`キーに対してはリポジトリのARNを指定する。
  DESCRIPTION
  type = map(object({
    tags = list(string)
    arn  = string
  }))
}

variable "artifact_store_bucket_arn" {
  description = "Runtime環境に作成するArtifact storeのARN。Artifact storeの実体は`cd_pipeline_backend pattern`のインスタンス作成時に作成される。形式は`arn:aws:s3:::[アーティファクトストアのバケット名]`となるため、バケット名さえ決定すれば`cd_pipeline_backend pattern`の適用を待たずに設定可能"
  type        = string
}

variable "artifact_store_bucket_encryption_key_arn" {
  description = "Runtime環境のArtifact store暗号化用CMKのARN。CMKは`cd_pipeline_backend` patternのインスタンス作成時に作成される。"
  type        = string
  default     = null
}

variable "deployment_setting_key" {
  description = "CodePipelineおよびCodeDeployが使用する設定ファイル群(zip)のS3 Object Key"
  type        = string
  default     = "settings.zip"
}

variable "target_event_bus_arn" {
  description = "送信先のEventBusのARN。デフォルトのイベントバスを利用する場合は`arn:aws:events:[リージョン名]:[Runtime環境のアカウントID]:event-bus/default`を指定する"
  type        = string
}
