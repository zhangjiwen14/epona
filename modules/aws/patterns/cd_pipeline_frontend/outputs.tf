output "artifact_store_arn" {
  description = "Artifact store用bucketのARN"
  value       = module.codepipeline.artifact_store_bucket_arn
}

output "kms_keys" {
  description = "作成されたKMSの鍵"
  value       = module.kms_key
}

output "codebuild" {
  description = "デプロイプロパイダとして用いるCodeBuildの情報"
  value = {
    project_name    = module.codebuild.codebuild_project_name
    arn             = module.codebuild.codebuild_arn
    cloudwatch_logs = module.codebuild_cloudwatch_log
  }
}
