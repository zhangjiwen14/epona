resource "datadog_integration_aws" "aws_integration" {
  count = var.create_iam_role_only ? 0 : 1

  account_id = var.integrate_aws_account_id
  role_name  = var.integration_role_name

  filter_tags                      = var.filter_tags
  host_tags                        = var.host_tags
  account_specific_namespace_rules = var.account_specific_namespace_rules
  excluded_regions                 = var.excluded_regions
}

module "assume_datadog_service" {
  source = "../../components/iam/assume_datadog_service"

  tags = var.tags

  aws_integration_external_id = var.create_iam_role_only ? var.aws_integration_external_id : datadog_integration_aws.aws_integration[0].external_id
  allow_actions               = var.allow_actions
  integration_policy_name     = var.integration_policy_name
  integration_role_name       = var.integration_role_name
}
