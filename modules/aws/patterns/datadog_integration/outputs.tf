output "datadog_aws_integration_external_id" {
  description = "AWS Integrationを行ったExternal ID"
  value       = concat(datadog_integration_aws.aws_integration.*.external_id, [var.aws_integration_external_id])[0]
}