terraform {
  required_providers {
    datadog = {
      source  = "DataDog/datadog"
      version = ">= 2.17.0, < 3.0.0"
    }
  }
  required_version = "~> 0.13.5"
}
