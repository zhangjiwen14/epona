variable "tags" {
  description = "Datadogと連携するIAMロール関連のリソースに、共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "create_iam_role_only" {
  description = "IAMロールの作成までにとどめ、DatadogとAWSのインテグレーションまでは行わない場合、`true`を指定する"
  type        = bool
  default     = false
}

variable "integrate_aws_account_id" {
  description = "Datadogと紐付けるAWSアカウントID"
  type        = string
}

variable "filter_tags" {
  description = "メトリクスの収集対象とのあるEC2を制御するためのタグを指定する。指定しない場合は、`integrate_aws_account_id`で指定するAWSアカウント配下の全EC2が収集対象となる。指定方法については[タグの使用方法](https://docs.datadoghq.com/ja/getting_started/tagging/using_tags/?tab=assignment#%E3%82%A4%E3%83%B3%E3%83%86%E3%82%B0%E3%83%AC%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3)を参照"
  type        = list(string)
  default     = null
}

variable "host_tags" {
  description = "このインテグレーションによりレポートされる、すべてのホストとメトリクスに追加するタグを指定する"
  type        = list(string)
  default     = null
}

variable "account_specific_namespace_rules" {
  description = "このアカウント内の特定の名前空間に対して、メトリクス収集の有効、無効を設定する"
  type        = map(bool)
  default     = null
}

variable "excluded_regions" {
  description = "メトリクス収集から除外するリージョンを指定する"
  type        = list(string)
  default     = null
}

variable "aws_integration_external_id" {
  description = "`create_iam_role_only`を`false`とした場合、必須。DatadogのAWS Integrationページより、External IDを取得して設定する"
  type        = string
  default     = null
}

variable "allow_actions" {
  description = <<DESCRIPTION
  Datadogに実行許可するアクションを、リストで指定する。
  どのようなアクションが指定可能かは、[DatadogのAWSインテグレーション](https://docs.datadoghq.com/ja/integrations/amazon_web_services/?tab=cloudformation)を参照すること。

  デフォルトでは、[DatadogのAWSインテグレーション]に記載されているすべてのアクセスを権限を許可している。
  DESCRIPTION
  type        = list(string)
  default = [
    "apigateway:GET",
    "autoscaling:Describe*",
    "budgets:ViewBudget",
    "cloudfront:GetDistributionConfig",
    "cloudfront:ListDistributions",
    "cloudtrail:DescribeTrails",
    "cloudtrail:GetTrailStatus",
    "cloudtrail:LookupEvents",
    "cloudwatch:Describe*",
    "cloudwatch:Get*",
    "cloudwatch:List*",
    "codedeploy:List*",
    "codedeploy:BatchGet*",
    "directconnect:Describe*",
    "dynamodb:List*",
    "dynamodb:Describe*",
    "ec2:Describe*",
    "ecs:Describe*",
    "ecs:List*",
    "elasticache:Describe*",
    "elasticache:List*",
    "elasticfilesystem:DescribeFileSystems",
    "elasticfilesystem:DescribeTags",
    "elasticfilesystem:DescribeAccessPoints",
    "elasticloadbalancing:Describe*",
    "elasticmapreduce:List*",
    "elasticmapreduce:Describe*",
    "es:ListTags",
    "es:ListDomainNames",
    "es:DescribeElasticsearchDomains",
    "health:DescribeEvents",
    "health:DescribeEventDetails",
    "health:DescribeAffectedEntities",
    "kinesis:List*",
    "kinesis:Describe*",
    "lambda:AddPermission",
    "lambda:GetPolicy",
    "lambda:List*",
    "lambda:RemovePermission",
    "logs:DeleteSubscriptionFilter",
    "logs:DescribeLogGroups",
    "logs:DescribeLogStreams",
    "logs:DescribeSubscriptionFilters",
    "logs:FilterLogEvents",
    "logs:PutSubscriptionFilter",
    "logs:TestMetricFilter",
    "rds:Describe*",
    "rds:List*",
    "redshift:DescribeClusters",
    "redshift:DescribeLoggingStatus",
    "route53:List*",
    "s3:GetBucketLogging",
    "s3:GetBucketLocation",
    "s3:GetBucketNotification",
    "s3:GetBucketTagging",
    "s3:ListAllMyBuckets",
    "s3:PutBucketNotification",
    "ses:Get*",
    "sns:List*",
    "sns:Publish",
    "sqs:ListQueues",
    "states:ListStateMachines",
    "states:DescribeStateMachine",
    "support:*",
    "tag:GetResources",
    "tag:GetTagKeys",
    "tag:GetTagValues",
    "xray:BatchGetTraces",
    "xray:GetTraceSummaries"
  ]
}

variable "integration_policy_name" {
  description = "Datadog連携のために作成するポリシーの名前"
  type        = string
  default     = "DatadogAWSIntegrationPolicy"
}

variable "integration_role_name" {
  description = "Datadog連携のために作成するIAMロール名"
  type        = string
  default     = "DatadogAWSIntegrationRole"
}
