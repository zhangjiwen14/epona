output "internal_container_endpoint" {
  description = "Proxyコンテナのエンドポイント"
  value       = "${var.service_discovery_service_name}.${var.service_discovery_namespace_name}:3128"
}
