variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "name_prefix" {
  description = "作成するリソース名に付与する接頭辞"
  type        = string
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
}

variable "container_subnets" {
  description = "コンテナサービスを配置する、サブネットIDのリスト"
  type        = list(string)
}

variable "service_discovery_namespace_name" {
  description = "Service DiscoveryのNamespaceの名前（ドメイン名になる）"
  type        = string
}

variable "service_discovery_service_name" {
  description = "Service DiscoveryのNamespaceに作成するサービス名（サブドメイン名になる）"
  type        = string
  default     = "proxy"
}

variable "repository_url" {
  description = "ECSでデプロイするProxyコンテナイメージのリポジトリURL"
  type        = string
}

variable "proxy_image_tag" {
  description = "ECSでデプロイするProxyコンテナイメージに付けたタグ"
  type        = string
  default     = "latest"
}

variable "container_service_desired_count" {
  description = "コンテナサービス内の、タスクのインスタンス数"
  type        = number
  default     = 1
}

variable "container_service_platform_version" {
  description = "コンテナサービスを実行するプラットフォームのバージョン"
  # https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/platform_versions.html
  type    = string
  default = "LATEST"
}

variable "container_task_cpu" {
  description = "コンテナサービス内で実行されるタスクに割り当てるCPU"
  type        = string
  default     = "256"
}

variable "container_task_memory" {
  description = "コンテナサービス内で実行されるタスクに割り当てるメモリ"
  type        = string
  default     = "512"
}

variable "container_task_execution_role_arn" {
  description = "コンテナサービスのタスクに割り当てるタスク実行IAMロールのARN。create_default_ecs_task_execution_roleをfalseにする場合に指定すること"
  type        = string
  default     = null
}

variable "create_default_ecs_task_execution_role" {
  description = "デフォルトのタスク実行ロールを作成する場合、trueを指定する"
  type        = bool
  default     = true
}

variable "default_ecs_task_execution_iam_role_name" {
  description = "デフォルトのタスク実行ロールを作成する場合のIAMロール名"
  type        = string
  default     = "DefaultContainerServiceTaskExecutionRole"
}

variable "default_ecs_task_execution_iam_policy_name" {
  description = "デフォルトのタスク実行ロールを作成する場合のIAMポリシー名"
  type        = string
  default     = "DefaultContainerServiceTaskExecutionRolePolicy"
}
