variable "create" {
  description = "ACMを使用して、SSL/TLS証明書を作成する場合、true"
  type        = bool
}

variable "tags" {
  description = "ACMに関するリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "domain_name" {
  description = "作成するDNSレコードで使用するドメイン名"
  type        = string
}

variable "validation_method" {
  description = "ドメインの検証方法"
  type        = string
}

variable "zone_name" {
  description = "DNSレコードを登録するホストゾーン名"
  type        = string
}

variable "ttl" {
  description = "DNSレコードのTTL"
  type        = number
}