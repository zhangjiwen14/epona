output "arn" {
  description = "イベントをEvent Busに送信するロールのARN"
  value       = aws_iam_role.put_events.arn
}
