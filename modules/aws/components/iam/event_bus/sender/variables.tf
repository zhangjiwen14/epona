variable "target_event_bus_arn" {
  description = "送信先のEventBusのARN"
  type        = string
}

variable "source_name" {
  description = "イベント送信元の名前"
  type        = string
}
