# Cloudwatch Events が Event Bus へイベント送信可能にする
data "aws_iam_policy_document" "cloudwatch_events_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

# Event Busに送信可能なロール
resource "aws_iam_role" "put_events" {
  name               = format("%sPutEventsToEventBusRole", title(var.source_name))
  assume_role_policy = data.aws_iam_policy_document.cloudwatch_events_assume_role.json
}

resource "aws_iam_policy" "event_bus_policy" {
  name   = format("%sPutEventsToEventBusPolicy", title(var.source_name))
  policy = data.aws_iam_policy_document.event_bus_policy.json
}

resource "aws_iam_role_policy_attachment" "event_bus_policy" {
  role       = aws_iam_role.put_events.name
  policy_arn = aws_iam_policy.event_bus_policy.arn
}

data "aws_iam_policy_document" "event_bus_policy" {
  statement {
    actions = [
      "events:PutEvents"
    ]
    resources = [var.target_event_bus_arn]
  }
}
