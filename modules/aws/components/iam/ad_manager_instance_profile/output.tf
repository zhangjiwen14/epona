output "iam_instance_profile_name" {
  description = "AD管理用EC2に付与するインスタンスプロファイルの名前"
  value       = aws_iam_instance_profile.ad_manager.name
}
