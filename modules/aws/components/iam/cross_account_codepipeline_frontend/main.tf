# Runtime環境から利用するCodePipeline用クロスアカウント
# - Runtime環境上のArtifact Storeに読み書きするポリシー
# - Runtime環境上のKMS CMKを利用するポリシー

locals {
  principals = [format("arn:aws:iam::%s:root", var.runtime_account_id)]
}

resource "aws_iam_role" "this" {
  name               = format("%sAccessRole", title(var.pipeline_name))
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    sid     = "AssumeRoleForAWSAccounts"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = local.principals
    }
  }
}

# CodePipelineのアーティファクトストアへのReadWriteポリシー
data "aws_iam_policy_document" "artifact_store_bucket_access" {
  statement {
    sid    = "EnableReadAndWriteArtifacts"
    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:PutObjectAcl"
    ]
    resources = [
      "${var.artifact_store_bucket_arn}/*"
    ]
  }

  statement {
    sid    = "ListBucketForArtifactStore"
    effect = "Allow"
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      var.artifact_store_bucket_arn
    ]
  }
}

resource "aws_iam_policy" "artifact_store_bucket_access" {
  name        = format("%sArtifactStoreAccessPolicy", title(var.pipeline_name))
  description = "Allow cross accounts to access artifact store bucket"
  policy      = data.aws_iam_policy_document.artifact_store_bucket_access.json
}

resource "aws_iam_role_policy_attachment" "artifact_store_bucket_access" {
  role       = aws_iam_role.this.name
  policy_arn = aws_iam_policy.artifact_store_bucket_access.arn
}

# ソースファイルが格納されたzipファイルが格納されているバケットへのアクセスポリシー
data "aws_iam_policy_document" "source_bucket_access" {
  statement {
    sid    = "ReadOnlyAccessForSourceBucket"
    effect = "Allow"
    actions = [
      "s3:Get*"
    ]
    resources = [
      "${var.source_bucket_arn}/*",
      var.source_bucket_arn
    ]
  }

  statement {
    sid    = "ListBucketForSourceBucket"
    effect = "Allow"
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      var.source_bucket_arn
    ]
  }
}

resource "aws_iam_policy" "source_bucket_access" {
  name        = format("%sSourceBucketAccessPolicy", title(var.pipeline_name))
  description = "Allow cross accounts access to source bucket"
  policy      = data.aws_iam_policy_document.source_bucket_access.json
}

resource "aws_iam_role_policy_attachment" "source_bucket_access" {
  role       = aws_iam_role.this.name
  policy_arn = aws_iam_policy.source_bucket_access.arn
}

# アーティファクトストアへのクロスアカウントでの暗号鍵へのアクセスポリシー
data "aws_iam_policy_document" "cmk" {
  count = var.artifact_store_bucket_encryption_key_arn != null ? 1 : 0

  statement {
    sid = "EnableAccessToAtifactStoreEncryptionKey"
    actions = [
      "kms:DescribeKey",
      "kms:GenerateDataKey*",
      "kms:Encrypt",
      "kms:ReEncrypt*",
      "kms:Decrypt"
    ]
    resources = [
      var.artifact_store_bucket_encryption_key_arn
    ]
  }
}

resource "aws_iam_policy" "cmk" {
  count = var.artifact_store_bucket_encryption_key_arn != null ? 1 : 0

  name        = format("%sArtifactStoreEncryptionPolicy", title(var.pipeline_name))
  description = "Allow cross accounts to encrypt and decrypt artifact store object"
  policy      = data.aws_iam_policy_document.cmk[0].json
}

resource "aws_iam_role_policy_attachment" "cmk" {
  count = var.artifact_store_bucket_encryption_key_arn != null ? 1 : 0

  role       = aws_iam_role.this.name
  policy_arn = aws_iam_policy.cmk[0].arn
}
