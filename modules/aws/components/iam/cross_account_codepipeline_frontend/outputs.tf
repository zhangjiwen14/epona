output "arn" {
  description = "Runtimeから利用するクロスアカウント用ロールのARN"
  value       = aws_iam_role.this.arn
}
