variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
}

variable "users" {
  description = "作成するユーザー一覧"
  type        = list(string)
}

variable "pgp_key" {
  description = "IAMユーザーのパスワード生成で利用するpgpの公開鍵(base64形式)"
  type        = string
}
