resource "aws_iam_user" "users" {
  for_each      = toset(var.users)
  name          = each.value
  tags          = var.tags
  force_destroy = true
}

data "aws_caller_identity" "self" {}

resource "aws_iam_user_policy" "users_policy" {
  for_each   = toset(var.users)
  name       = each.value
  user       = each.value
  depends_on = [aws_iam_user.users]
  policy     = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "iam:ChangePassword",
        "iam:CreateVirtualMFADevice",
        "iam:DeleteVirtualMFADevice",
        "iam:EnableMFADevice",
        "iam:GetAccountPasswordPolicy",
        "iam:GetLoginProfile",
        "iam:GetUser",
        "iam:ResyncMFADevice",
        "iam:UpdateLoginProfile",
        "iam:CreateAccessKey",
        "iam:DeleteAccessKey",
        "iam:UpdateAccessKey",
        "iam:UpdateSigningCertificate",
        "iam:UploadSigningCertificate"
      ],
      "Resource": [
        "arn:aws:iam::${data.aws_caller_identity.self.account_id}:user/${each.value}",
        "arn:aws:iam::${data.aws_caller_identity.self.account_id}:mfa/${each.value}"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
         "iam:DeactivateMFADevice"
      ],
      "Resource": "*",
      "Condition": {
        "Bool": {
          "aws:MultiFactorAuthPresent": true
        }
      }
    },
    {
      "Effect": "Allow",
      "Action": [
        "iam:ListMFADevices",
        "iam:ListVirtualMFADevices",
        "iam:ListUsers"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_user_login_profile" "users_profile" {
  for_each   = toset(values(aws_iam_user.users)[*].name)
  user       = each.value
  pgp_key    = var.pgp_key
  depends_on = [aws_iam_user.users]
}

output "password" {
  value = {
    for key, value in aws_iam_user_login_profile.users_profile :
    key => value.encrypted_password
  }
}
