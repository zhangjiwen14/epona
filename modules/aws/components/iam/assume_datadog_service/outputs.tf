output "datadog_aws_integration_iam_role_name" {
  value = aws_iam_role.datadog_aws_integration.name
}