output "aws_iam_access_key" {
  value     = aws_iam_access_key.ses_smtp_key.id
  sensitive = true
}

output "aws_iam_secret" {
  value     = aws_iam_access_key.ses_smtp_key.secret
  sensitive = true
}

output "aws_iam_smtp_password_v4" {
  value     = aws_iam_access_key.ses_smtp_key.ses_smtp_password_v4
  sensitive = true
}
