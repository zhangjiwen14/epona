variable "username" {
  description = "SES STMPで使用するIAMユーザー名"
  type        = string
  default     = "SesSmtpUser"
}

variable "tags" {
  description = "SES SMTPで使用するIAMユーザーに関するリソースに共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "policy_name" {
  description = "ポリシー名"
  type        = string
  default     = "SesSmtpPolicy"
}

variable "policy" {
  description = "IAMユーザーに適用するポリシーの内容"
  type        = string
  default     = <<JSON
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect":"Allow",
      "Action":[
        "ses:SendEmail",
        "ses:SendRawEmail"
      ],
      "Resource":"*"
    }
  ]
}
JSON
}
