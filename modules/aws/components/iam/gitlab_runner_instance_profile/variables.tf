variable "tags" {
  description = "GitLab Runnerが稼働するEC2インスタンスに付与するIAMロール関連のリソースに、共通的に付与するタグ"
  type        = map(string)
  default     = {}
}


variable "assume_role_service_identifiers" {
  description = "AssumeRoleで指定するidenfitierのリスト"
  type        = list(string)
  default     = null
}

variable "iam_role_name" {
  description = "IAMロール名"
  type        = string
}

variable "enable_systems_manager" {
  description = "SystemManagerに関するIAMロールポリシー（AmazonSSMManagedInstanceCore）をアタッチするかどうか"
  type        = bool
  default     = true
}

variable "enable_access_ecr" {
  description = "ECRへアクセスするIAMロールポリシー（AmazonEC2ContainerRegistryPowerUser）をアタッチするかどうか"
  type        = bool
  default     = true
}

variable "enable_access_s3" {
  description = "S3へアクセスするIAMロールポリシー（AmazonS3FullAccess）をアタッチするかどうか"
  type        = bool
  default     = true
}

variable "additional_iam_policy_name" {
  description = "任意のIAMポリシーを付与する場合、その名前となる値"
  type        = string
  default     = null
}

variable "additional_iam_policy" {
  description = "任意のIAMポリシーを付与する場合、その内容をJSONで指定する"
  type        = string
  default     = null
}

variable "iam_instance_profile_name" {
  description = "GitLab Runnerが稼働するEC2インスタンスに付与する、IAMインスタンスプロファイル名"
  type        = string
}
