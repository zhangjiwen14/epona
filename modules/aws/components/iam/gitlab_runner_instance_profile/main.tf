locals {
  assume_role_default_service_identifiers = ["ec2.amazonaws.com"]
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = var.assume_role_service_identifiers != null ? var.assume_role_service_identifiers : local.assume_role_default_service_identifiers
    }
  }
}

resource "aws_iam_role" "role" {
  name               = var.iam_role_name
  assume_role_policy = data.aws_iam_policy_document.assume_role.json

  tags = merge(
    {
      "Name" = var.iam_role_name
    },
    var.tags
  )
}

data "aws_iam_policy" "systems_manager" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

data "aws_iam_policy" "access_ecr" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
}

data "aws_iam_policy" "access_s3" {
  arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "systems_manager" {
  count = var.enable_systems_manager ? 1 : 0

  role       = aws_iam_role.role.name
  policy_arn = data.aws_iam_policy.systems_manager.arn
}

resource "aws_iam_role_policy_attachment" "access_ecr" {
  count = var.enable_access_ecr ? 1 : 0

  role       = aws_iam_role.role.name
  policy_arn = data.aws_iam_policy.access_ecr.arn
}

resource "aws_iam_role_policy_attachment" "access_s3" {
  count = var.enable_access_s3 ? 1 : 0

  role       = aws_iam_role.role.name
  policy_arn = data.aws_iam_policy.access_s3.arn
}

resource "aws_iam_policy" "additional" {
  count = var.additional_iam_policy_name != null && var.additional_iam_policy != null ? 1 : 0

  name   = var.additional_iam_policy_name
  policy = var.additional_iam_policy
}

resource "aws_iam_role_policy_attachment" "additional" {
  count      = var.additional_iam_policy_name != null && var.additional_iam_policy != null ? 1 : 0
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.additional[0].arn
}

resource "aws_iam_instance_profile" "this" {
  name = var.iam_instance_profile_name
  role = aws_iam_role.role.name
}
