locals {
  costmanager_entities = [for user in var.costmanagers : "arn:aws:iam::${var.account_id}:user/${user}"]
}

resource "aws_iam_role" "costmanager_role" {
  tags               = var.tags
  name               = format("%sCostManagerRole", title(var.system_name))
  assume_role_policy = data.aws_iam_policy_document.costmanager_policy.json
}

data "aws_iam_policy_document" "costmanager_policy" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type        = "AWS"
      identifiers = local.costmanager_entities
    }

    # MFAが有効になっているリクエストのみAssumeRoleを許可する
    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"

      values = [true]
    }
  }
}

resource "aws_iam_role_policy_attachment" "costmanager_role_attach" {
  role       = aws_iam_role.costmanager_role.name
  policy_arn = "arn:aws:iam::aws:policy/job-function/Billing"
}

resource "aws_iam_role_policy_attachment" "additional_costmanager_role_attach" {
  count = var.additional_costmanager_role_policies != null ? length(var.additional_costmanager_role_policies) : 0

  role       = aws_iam_role.costmanager_role.name
  policy_arn = var.additional_costmanager_role_policies[count.index]
}
