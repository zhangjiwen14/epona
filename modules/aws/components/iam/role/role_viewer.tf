locals {
  viewer_entities = [for user in var.viewers : "arn:aws:iam::${var.account_id}:user/${user}"]
}

resource "aws_iam_role" "viewer_role" {
  tags               = var.tags
  name               = format("%sViewerRole", title(var.system_name))
  assume_role_policy = data.aws_iam_policy_document.viewer_policy.json
}

data "aws_iam_policy_document" "viewer_policy" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type        = "AWS"
      identifiers = local.viewer_entities
    }

    # MFAが有効になっているリクエストのみAssumeRoleを許可する
    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"

      values = [true]
    }
  }
}

resource "aws_iam_role_policy_attachment" "viewer_role_attach" {
  role       = aws_iam_role.viewer_role.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "additional_viewer_role_attach" {
  count = var.additional_viewer_role_policies != null ? length(var.additional_viewer_role_policies) : 0

  role       = aws_iam_role.viewer_role.name
  policy_arn = var.additional_viewer_role_policies[count.index]
}
