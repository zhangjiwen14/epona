locals {
  admin_entities = [for user in var.admins : "arn:aws:iam::${var.account_id}:user/${user}"]
}

resource "aws_iam_role" "admin_role" {
  tags               = var.tags
  name               = format("%sAdminRole", title(var.system_name))
  assume_role_policy = data.aws_iam_policy_document.admin_policy.json
}

data "aws_iam_policy_document" "admin_policy" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type        = "AWS"
      identifiers = local.admin_entities
    }

    # MFAが有効になっているリクエストのみAssumeRoleを許可する
    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"

      values = [true]
    }
  }
}

resource "aws_iam_role_policy_attachment" "admin_role_attach" {
  role       = aws_iam_role.admin_role.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_role_policy_attachment" "additional_admin_role_attach" {
  count = var.additional_admin_role_policies != null ? length(var.additional_admin_role_policies) : 0

  role       = aws_iam_role.admin_role.name
  policy_arn = var.additional_admin_role_policies[count.index]
}
