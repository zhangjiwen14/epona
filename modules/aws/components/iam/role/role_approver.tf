locals {
  approver_entities = [for user in var.approvers : "arn:aws:iam::${var.account_id}:user/${user}"]
}

resource "aws_iam_role" "approver_role" {
  tags               = var.tags
  name               = format("%sApproverRole", title(var.system_name))
  assume_role_policy = data.aws_iam_policy_document.approver_policy.json
}

data "aws_iam_policy_document" "approver_policy" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type        = "AWS"
      identifiers = local.approver_entities
    }

    # MFAが有効になっているリクエストのみAssumeRoleを許可する
    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"

      values = [true]
    }
  }
}


resource "aws_iam_role_policy_attachment" "approver_role_attach" {
  role       = aws_iam_role.approver_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSCodePipelineApproverAccess"
}

resource "aws_iam_role_policy_attachment" "additional_approver_role_attach" {
  count = var.additional_approver_role_policies != null ? length(var.additional_approver_role_policies) : 0

  role       = aws_iam_role.approver_role.name
  policy_arn = var.additional_approver_role_policies[count.index]
}
