locals {
  viewer_role_name = format("%sViewerRole", title(var.system_name))
}

resource "aws_iam_policy" "viewer_switch_policy" {
  name        = format("%s%sViewerSwitchPolicy", title(var.system_name), title(var.environment_name))
  description = "IAM Policy for viewer"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::${var.account_id}:role/${local.viewer_role_name}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "viewer_attach" {
  name       = format("%s%sViewerAttach", title(var.system_name), title(var.environment_name))
  users      = var.viewers
  policy_arn = aws_iam_policy.viewer_switch_policy.arn
}
