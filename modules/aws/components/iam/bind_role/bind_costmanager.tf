locals {
  costmanager_role_name = format("%sCostManagerRole", title(var.system_name))
}

resource "aws_iam_policy" "costmanager_switch_policy" {
  name        = format("%s%sCostManagerSwitchPolicy", title(var.system_name), title(var.environment_name))
  description = "IAM Policy for costmanager"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::${var.account_id}:role/${local.costmanager_role_name}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "costmanager_attach" {
  name       = format("%s%sCostManagerAttach", title(var.system_name), title(var.environment_name))
  users      = var.costmanagers
  policy_arn = aws_iam_policy.costmanager_switch_policy.arn
}
