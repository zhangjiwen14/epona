locals {
  operator_role_name = format("%sOperatorRole", title(var.system_name))
}

resource "aws_iam_policy" "operator_switch_policy" {
  name        = format("%s%sOperatorSwitchPolicy", title(var.system_name), title(var.environment_name))
  description = "IAM Policy for operator"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::${var.account_id}:role/${local.operator_role_name}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "operator_attach" {
  name       = format("%s%sOperatorAttach", title(var.system_name), title(var.environment_name))
  users      = var.operators
  policy_arn = aws_iam_policy.operator_switch_policy.arn
}
