locals {
  developer_role_name = format("%sDeveloperRole", title(var.system_name))
}

resource "aws_iam_policy" "developer_switch_policy" {
  name        = format("%s%sDeveloperSwitchPolicy", title(var.system_name), title(var.environment_name))
  description = "IAM Policy for developer"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::${var.account_id}:role/${local.developer_role_name}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "developer_attach" {
  name       = format("%s%sDeveloperAttach", title(var.system_name), title(var.environment_name))
  users      = var.developers
  policy_arn = aws_iam_policy.developer_switch_policy.arn
}
