locals {
  approver_role_name = format("%sApproverRole", title(var.system_name))
}

resource "aws_iam_policy" "approver_switch_policy" {
  name        = format("%s%sApproverSwitchPolicy", title(var.system_name), title(var.environment_name))
  description = "IAM Policy for approver"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::${var.account_id}:role/${local.approver_role_name}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "approver_attach" {
  name       = format("%s%sApproverAttach", title(var.system_name), title(var.environment_name))
  users      = var.approvers
  policy_arn = aws_iam_policy.approver_switch_policy.arn
}
