data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

# 通知先へ検知を送信するLambda関数に設定するIAM Roleを作成する
resource "aws_iam_role" "threat_detection" {
  name               = "lambda-${var.lambda_function_name}-role"
  tags               = var.tags
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "threat_detection" {
  description = "通知先へ検知を送信するLambda関数に設定するIAM Roleのポリシー"
  name        = "lambda-${var.lambda_function_name}-policy"
  path        = "/"

  # LambdaがVPC内部に配置される場合、ネットワークインターフェースの操作が必要
  # see: https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/configuration-vpc.html#vpc-permissions
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${var.lambda_function_name}:*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:CreateNetworkInterface",
                "ec2:DescribeNetworkInterfaces",
                "ec2:DeleteNetworkInterface"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

# 通知先へ検知を送信するLambda関数に設定するIAM Roleにポリシーを設定する
resource "aws_iam_role_policy_attachment" "threat_detection" {
  role       = aws_iam_role.threat_detection.name
  policy_arn = aws_iam_policy.threat_detection.arn
}
