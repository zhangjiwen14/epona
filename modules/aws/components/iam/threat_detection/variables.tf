variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "lambda_function_name" {
  description = "通知先へ検知を送信するLambda関数の名前"
  type        = string
}
