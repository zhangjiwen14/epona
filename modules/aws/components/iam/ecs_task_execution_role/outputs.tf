output "ecs_task_execution_iam_role_arn" {
  value = concat(aws_iam_role.ecs_task_execution.*.arn, [""])[0]
}
