variable "create" {
  description = "このモジュールでIAMロール、IAMポリシーを作成する場合、true"
  type        = bool
}

variable "ecs_task_execution_iam_role_name" {
  description = "タスク実行IAMロール名"
  type        = string
}

variable "ecs_task_execution_iam_policy_name" {
  description = "タスク実行IAMポリシー名"
  type        = string
}