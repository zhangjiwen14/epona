data "aws_iam_policy" "rds_enhanced_monitoring_role_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      identifiers = ["monitoring.rds.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "rds_enhanced_monitoring_role" {
  count = var.create ? 1 : 0

  name               = var.rds_enhanced_monitoring_role_name
  assume_role_policy = data.aws_iam_policy_document.assume_role.json

  tags = merge(
    {
      "Name" = var.rds_enhanced_monitoring_role_name
    },
    var.tags
  )
}

resource "aws_iam_role_policy_attachment" "rds_enhanced_monitoring_policy" {
  count = var.create ? 1 : 0

  role       = aws_iam_role.rds_enhanced_monitoring_role[0].name
  policy_arn = data.aws_iam_policy.rds_enhanced_monitoring_role_policy.arn
}
