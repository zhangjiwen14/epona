output "rds_enhanced_monitoring_role_arn" {
  description = "RDS拡張モニタリング用のIAMロールのARN"
  value       = concat(aws_iam_role.rds_enhanced_monitoring_role[*].arn, [""])[0]
}
