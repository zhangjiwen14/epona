variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "name_suffix" {
  description = "IAM RoleとIAM Policyの名前に付ける接尾語"
  type        = string
}

variable "allow_bucket_name" {
  description = "Kinesis Data Firehoseの出力先にする3バケットの名前"
  type        = string
}
