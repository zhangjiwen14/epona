data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_iam_policy_document" "assume_role_policy_lambda_role" {
  version = "2012-10-17"
  statement {
    sid    = ""
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
    actions = [
      "sts:AssumeRole"
    ]
  }
}

resource "aws_iam_role" "lambda_role" {
  name               = "lambda${title(var.function_name)}Role"
  tags               = var.tags
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy_lambda_role.json
}

data "aws_iam_policy_document" "iam_policy_lambda_role" {
  version = "2012-10-17"
  statement {
    sid    = ""
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup"
    ]
    resources = [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*"
    ]
  }
  statement {
    sid    = ""
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${var.function_name}:*"
    ]
  }
}

resource "aws_iam_policy" "iam_policy_lambda_role" {
  name   = "lambda${title(var.function_name)}Policy"
  policy = data.aws_iam_policy_document.iam_policy_lambda_role.json
}

resource "aws_iam_role_policy_attachment" "lambda_role" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.iam_policy_lambda_role.arn
}

data "aws_iam_policy_document" "assume_role_policy_awsconfig" {
  version = "2012-10-17"
  statement {
    sid    = ""
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["config.amazonaws.com"]
    }
    actions = [
      "sts:AssumeRole"
    ]
  }
}

resource "aws_iam_role" "awsconfig_role" {
  name               = "config${title(var.aws_config_role_name)}Role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy_awsconfig.json
}

resource "aws_iam_role_policy_attachment" "awsconfig_awsconfigrole" {
  role       = aws_iam_role.awsconfig_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSConfigRole"
}

data "aws_iam_policy_document" "iam_policy_awsconfig" {
  version = "2012-10-17"
  statement {
    effect = "Allow"
    actions = [
      "s3:PutObject*"
    ]
    resources = [
      var.aws_config_bucket_arn,
      "${var.aws_config_bucket_arn}/*"
    ]
    condition {
      test     = "StringLike"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:GetBucketAcl"
    ]
    resources = [
      var.aws_config_bucket_arn
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "sns:Publish"
    ]
    resources = [
      var.notification_topic_arn
    ]
  }
}
resource "aws_iam_policy" "iam_policy_awsconfig" {
  name   = "${aws_iam_role.awsconfig_role.name}Policy"
  policy = data.aws_iam_policy_document.iam_policy_awsconfig.json
}
resource "aws_iam_role_policy_attachment" "awsconfig" {
  role       = aws_iam_role.awsconfig_role.name
  policy_arn = aws_iam_policy.iam_policy_awsconfig.arn
}

