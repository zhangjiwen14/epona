output "iam_role_rule_violation_arn" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数が使用するIAM RoleのARNを設定する。"
  value       = aws_iam_role.lambda_role.arn
  sensitive   = true
}
output "aws_config_role_arn" {
  description = "AWS Configが情報を収集するために使用するIAM RoleのARNを設定する。"
  value       = aws_iam_role.awsconfig_role.arn
}

