variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグを設定する。"
  type        = map(string)
  default     = {}
}

variable "function_name" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数の名前を設定する。名称は「lambda-（本項目に設定した文字列）-role」となる。"
  type        = string
}

variable "aws_config_role_name" {
  description = "AWS Configがリソースの情報を収集するために使用するIAMロールの名称を設定する。名称は「config-（本項目に設定した文字列）-role」となる。"
  type        = string
}
variable "aws_config_bucket_arn" {
  description = "AWS Configがリソースの設定変更の詳細を送信するS3バケットのARNを設定する。"
  type        = string
}
variable "notification_topic_arn" {
  description = "AWS Configがリソースの設定変更またはルール違反を検知した場合に通知を送るSNSトピックのARNを設定する。"
  type        = string
}
