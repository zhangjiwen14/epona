output "parameters" {
  value = {
    for parameter in aws_ssm_parameter.this : parameter.name => {
      name   = parameter.name
      type   = parameter.type
      key_id = parameter.key_id
    }
  }
}
