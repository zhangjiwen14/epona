resource "aws_elasticache_replication_group" "this" {
  replication_group_id          = var.replication_group_id
  replication_group_description = var.replication_group_description

  engine         = "redis"
  engine_version = var.engine_version

  port = var.port

  node_type             = var.node_type
  number_cache_clusters = var.cluster_mode_enabled ? null : var.number_cache_clusters

  availability_zones = var.cluster_mode_enabled ? null : var.availability_zones

  automatic_failover_enabled = var.automatic_failover_enabled

  at_rest_encryption_enabled = var.at_rest_encryption_enabled
  transit_encryption_enabled = var.transit_encryption_enabled

  auth_token = var.auth_token
  kms_key_id = var.kms_key_id

  snapshot_name            = var.snapshot_name
  snapshot_retention_limit = var.snapshot_retention_limit
  snapshot_window          = var.snapshot_window

  maintenance_window     = var.maintenance_window
  notification_topic_arn = var.notification_topic_arn

  apply_immediately = var.apply_immediately

  security_group_ids   = var.security_groups
  parameter_group_name = aws_elasticache_parameter_group.this.name
  subnet_group_name    = aws_elasticache_subnet_group.this.name

  tags = merge(
    {
      "Name" = var.replication_group_id
    },
    var.tags
  )

  dynamic "cluster_mode" {
    for_each = var.cluster_mode_enabled ? ["true"] : []

    content {
      replicas_per_node_group = var.cluster_mode_replicas_per_node_group
      num_node_groups         = var.cluster_mode_num_node_groups
    }
  }

  lifecycle {
    ignore_changes = [auth_token] # 認証トークン変更の差分は無視する
  }
}

resource "aws_elasticache_parameter_group" "this" {
  name   = "${var.replication_group_id}-parameter-group"
  family = var.family

  dynamic "parameter" {
    for_each = var.cluster_mode_enabled ? concat([{ name = "cluster-enabled", value = "yes" }], var.parameters) : var.parameters

    content {
      name  = parameter.value.name
      value = parameter.value.value
    }
  }
}

resource "aws_elasticache_subnet_group" "this" {
  name       = "${var.replication_group_id}-subnet"
  subnet_ids = var.subnets
}
