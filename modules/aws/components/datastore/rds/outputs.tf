output "instance_identifier" {
  description = "RDSインスタンスの識別子"
  value       = aws_db_instance.this.identifier
}

output "instance_name" {
  description = "データベース名"
  value       = aws_db_instance.this.name
}

output "instance_username" {
  description = "ユーザー名"
  value       = aws_db_instance.this.username
}

output "instance_address" {
  description = "RDSインスタンスへアクセスするためのアドレス"
  value       = aws_db_instance.this.address
}

output "instance_port" {
  description = "RDSインスタンスへアクセスするためのポート"
  value       = aws_db_instance.this.port
}

output "instance_endpoint" {
  description = "RDSインスタンスのエンドポイント"
  value       = aws_db_instance.this.endpoint
}

output "instance_engine" {
  description = "RDSインスタンスのデータベースエンジン"
  value       = aws_db_instance.this.engine
}

output "instance_engine_version" {
  description = "RDSインスタンスのデータベースエンジンのバージョン"
  value       = aws_db_instance.this.engine_version
}
