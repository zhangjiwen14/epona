variable "zone_name" {
  description = "レコードを登録するホストゾーン名"
  type        = string
}

variable "record_name" {
  description = "DNSレコード名（ドメイン名）"
  type        = string
}

variable "alias_target" {
  description = <<DESCRIPTION
ALIASレコードの転送先設定
dns_name = 転送先リソースのDNS名
zone_id = 転送先リソースのホストゾーンID
DESCRIPTION
  type        = map(any)
}