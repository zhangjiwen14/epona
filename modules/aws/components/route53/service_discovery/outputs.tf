output "namespace_arn" {
  description = "Service DiscoveryのNamespaceのARN"
  value       = aws_service_discovery_private_dns_namespace.this.arn
}

output "namespace_id" {
  description = "Service DiscoveryのNamespaceのID"
  value       = aws_service_discovery_private_dns_namespace.this.id
}

output "service_arn" {
  description = "Service DiscoveryのサービスのARN"
  value       = aws_service_discovery_service.this.arn
}

output "service_id" {
  description = "Service DiscoveryのサービスのID"
  value       = aws_service_discovery_service.this.id
}
