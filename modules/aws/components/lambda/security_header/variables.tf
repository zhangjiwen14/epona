variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "lambda_function_name" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数の名前"
  type        = string
}

variable "lambda_timeout" {
  description = "Lambdaが停止するまでのタイムアウト設定[s]"
  type        = number
  default     = 3
}

variable "lambda_source_dir" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数のソースコードが配置されたディレクトリのパス（デフォルト以外のコードを使用したい場合に指定）"
  type        = string
}

variable "lambda_handler" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数のエントリーポイント"
  type        = string
}

variable "lambda_runtime" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数のランタイム"
  type        = string
}

variable "iam_role_arn" {
  description = "CloudFrontからのレスポンスにSecurityHeaderを付与するLambda関数に設定するIAM RoleのARN"
  type        = string
}
