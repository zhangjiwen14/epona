# 通知先へ検知を送信するLambda関数のコードをTerraformで送るためにZIP圧縮をする
data "archive_file" "threat_detection" {
  type        = "zip"
  source_dir  = var.notification_lambda_source_dir != null ? var.notification_lambda_source_dir : "${path.module}/codes/send-notification"
  output_path = "${path.root}/.terraform/.epona/components/lambda/threat_detection/codes/send-notification.zip"
}

# Lambdaの作成
resource "aws_lambda_function" "threat_detection" {
  description      = "通知先へ検知を送信するLambda関数"
  filename         = data.archive_file.threat_detection.output_path
  function_name    = var.lambda_function_name
  role             = var.iam_role_threat_detection_arn
  handler          = var.notification_lambda_handler
  source_code_hash = data.archive_file.threat_detection.output_base64sha256
  runtime          = var.notification_lambda_runtime
  timeout          = var.lambda_timeout
  tags             = var.tags

  vpc_config {
    subnet_ids         = var.lambda_subnet_ids
    security_group_ids = var.lambda_security_group_arns
  }

  environment {
    variables = {
      SERVICE_TYPE    = var.notification_lambda_config["type"]
      ENDPOINT_LOW    = contains(keys(var.notification_lambda_config["endpoints"]), "low") ? var.notification_lambda_config["endpoints"]["low"] : null
      ENDPOINT_MEDIUM = contains(keys(var.notification_lambda_config["endpoints"]), "medium") ? var.notification_lambda_config["endpoints"]["medium"] : null
      ENDPOINT_HIGH   = contains(keys(var.notification_lambda_config["endpoints"]), "high") ? var.notification_lambda_config["endpoints"]["high"] : null
    }
  }
}

# GuardDuty検知イベントが通知先へ検知を送信するLambda関数を呼び出せるようにする権限を設定する
resource "aws_lambda_permission" "threat_detection" {
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_function_name
  principal     = "events.amazonaws.com"
  source_arn    = var.cloudwatch_event_rule_threat_detection_arn
}
