variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグを設定する。"
  type        = map(string)
  default     = {}
}

variable "function_name" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数の名前を設定する。"
  type        = string
}

variable "notification_type" {
  description = "結果の通知先となるチャットツールの種類（`slack`: Slack | `teams`: Microsoft Teams）を設定する。"
  type        = string
}

variable "notification_channel" {
  description = "結果の通知先となるチャットツールのIncoming Webhook URLを設定する。`changeDetectionNotice`キーに対する値としては、対象リソース変更時の通知先、`ruleViolationNotice`キーに対する値としては、ルール違反情報の通知先を設定すること"
  type        = map(string)
  # 検知結果の範囲ごとに通知先のチャンネルのパスを指定（通知しない場合は未定義またはnullを指定）
  default = {
    changeDetectionNotice = null
    ruleViolationNotice   = null
  }
}

variable "notification_lambda_source_dir" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数のソースコードが配置されたディレクトリのパスを設定する。"
  type        = string
}

variable "notification_lambda_handler" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数の、エントリーポイントとなる関数名を設定する。"
  type        = string
}

variable "notification_lambda_runtime" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数のランタイムを設定する。設定内容については[AWS Lambdaランタイム](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/lambda-runtimes.html)を参照してください。"
  type        = string
}

variable "notification_lambda_timeout" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数のタイムアウト時間（秒）を設定する。"
  type        = number
}

variable "iam_role_rule_violation_arn" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数が使用するIAM RoleのARNを設定する"
  type        = string
}

variable "notification_topic_arn" {
  description = "AWS Configがリソースの設定変更またはルール違反を検知した場合に通知を送るSNSトピックのARNを設定する。"
  type        = string
}
