# 通知先へ検知を送信するLambda関数のコードをTerraformで送るためにZIP圧縮をする
data "archive_file" "rule_violation" {
  type        = "zip"
  source_dir  = var.notification_lambda_source_dir != null ? var.notification_lambda_source_dir : "${path.module}/codes/send-slack"
  output_path = "${path.root}/.terraform/.epona/components/lambda/rule_violation/codes/send-slack.zip"
}

# Lambdaの作成
resource "aws_lambda_function" "rule_violation" {
  description      = "設定変更またはルール違反に関する情報を通知先へ送信する関数"
  filename         = data.archive_file.rule_violation.output_path
  function_name    = var.function_name
  role             = var.iam_role_rule_violation_arn
  handler          = var.notification_lambda_handler
  source_code_hash = data.archive_file.rule_violation.output_base64sha256
  runtime          = var.notification_lambda_runtime
  timeout          = var.notification_lambda_timeout
  tags             = var.tags

  environment {
    variables = {
      SERVICE_TYPE            = var.notification_type
      CHANGE_DETECTION_NOTICE = contains(keys(var.notification_channel), "changeDetectionNotice") ? var.notification_channel["changeDetectionNotice"] : null
      RULE_VIOLATION_NOTICE   = contains(keys(var.notification_channel), "ruleViolationNotice") ? var.notification_channel["ruleViolationNotice"] : null
    }
  }
}

resource "aws_lambda_permission" "rule_violation" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = var.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = var.notification_topic_arn
}
