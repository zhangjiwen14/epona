const https = require('https');
let hostName;
let notificationChannel;
const SUPPORT_SERVICES = {
  SLACK: 'slack',
  TEAMS: 'teams',
};
const SUPPORT_SERVICES_HOSTNAME = {
  SLACK: 'hooks.slack.com',
  TEAMS: 'outlook.office.com',
};
const MESSAGE_TYPE = {
  CHANGE_NOTICE: 'ConfigurationItemChangeNotification',
  RULE_NOTICE: 'ComplianceChangeNotification',
};

/**
 * EventデータをSlack通知に使用する文字列に変換する。
 * @param {Object} event AWS Configが送信するEventデータ
 * @return {string} JSON形式の通知文字列
 */
const createSlackData = (event) => {
  hostName = SUPPORT_SERVICES_HOSTNAME.SLACK;
  const message = JSON.parse(event.Records[0].Sns.Message);
  if (message.messageType === MESSAGE_TYPE.CHANGE_NOTICE) {
    if (process.env['CHANGE_DETECTION_NOTICE']) {
      notificationChannel = process.env['CHANGE_DETECTION_NOTICE'];
      const data = JSON.stringify({
        'text': '```' + JSON.stringify({
          'subject': message.messageType,
          'awsAccountId': message.configurationItem.awsAccountId,
          'awsRegion': message.configurationItem.awsRegion,
          'resourceType': message.configurationItem.resourceType,
          'resourceId': message.configurationItem.resourceId,
          'changeType': message.configurationItemDiff.changeType,
          //        'message': message
        }, null, 2) + '```',
      });
      return data;
    }
  } else if (message.messageType === MESSAGE_TYPE.RULE_NOTICE) {
    if (process.env['RULE_VIOLATION_NOTICE']) {
      notificationChannel = process.env['RULE_VIOLATION_NOTICE'];
      const data = JSON.stringify({
        'text': '```' + JSON.stringify({
          'subject': message.messageType,
          'awsAccountId': message.awsAccountId,
          'awsRegion': message.awsRegion,
          'resourceType': message.resourceType,
          'resourceId': message.resourceId,
          'configRuleName': message.configRuleName,
          'configRuleArn': message.configRuleARN,
          'newComplianceType': message.newEvaluationResult.complianceType,
          //        'message': message
        }, null, 2) + '```',
      });
      return data;
    }
  }
  return '';
};

/**
 * EventデータをTeams通知に使用する文字列に変換する。
 * @details Teams向けの書式設定として、アダプティブカードを利用
 * ref: https://docs.microsoft.com/ja-jp/microsoftteams/platform/task-modules-and-cards/cards/cards-format?tabs=adaptive-md%2Cconnector-html
 * @param {Object} event AWS Configが送信するEventデータ
 * @return {string} JSON形式の通知文字列
 */
const createTeamsData = (event) => {
  hostName = SUPPORT_SERVICES_HOSTNAME.TEAMS;
  const message = JSON.parse(event.Records[0].Sns.Message);
  if (message.messageType === MESSAGE_TYPE.CHANGE_NOTICE) {
    if (process.env['CHANGE_DETECTION_NOTICE']) {
      notificationChannel = process.env['CHANGE_DETECTION_NOTICE'];
      const data = JSON.stringify({
        '@type': 'MessageCard',
        '@context': 'https://schema.org/extensions',
        'summary': 'Notification from AWS Config',
        'title': message.messageType,
        'sections': [{
          'facts': [{
            'name': 'awsAccountId',
            'value': message.configurationItem.awsAccountId,
          }, {
            'name': 'awsRegion',
            'value': message.configurationItem.awsRegion,
          }, {
            'name': 'resourceType',
            'value': message.configurationItem.resourceType,
          }, {
            'name': 'resourceId',
            'value': message.configurationItem.resourceId,
          }, {
            'name': 'changeType',
            'value': message.configurationItemDiff.changeType,
          }],
        }],
      });
      return data;
    }
  } else if (message.messageType === 'ComplianceChangeNotification') {
    if (process.env['RULE_VIOLATION_NOTICE']) {
      notificationChannel = process.env['RULE_VIOLATION_NOTICE'];
      const data = JSON.stringify({
        '@type': 'MessageCard',
        '@context': 'https://schema.org/extensions',
        'summary': 'Notification from AWS Config',
        'title': message.messageType,
        'sections': [{
          'facts': [{
            'name': 'awsAccountId',
            'value': message.awsAccountId,
          }, {
            'name': 'awsRegion',
            'value': message.awsRegion,
          }, {
            'name': 'resourceType',
            'value': message.resourceType,
          }, {
            'name': 'resourceId',
            'value': message.resourceId,
          }, {
            'name': 'configRuleName',
            'value': message.configRuleName,
          }, {
            'name': 'configRuleArn',
            'value': message.configRuleARN,
          }, {
            'name': 'newComplianceType',
            'value': message.newEvaluationResult.complianceType,
          }],
        }],
      });
      return data;
    }
  }
  return '';
};

const createSendData = (type, event) => {
  switch (type) {
    case SUPPORT_SERVICES.SLACK:
      return createSlackData(event);
    case SUPPORT_SERVICES.TEAMS:
      return createTeamsData(event);
  }
};

exports.handler = async (event) => {
  hostName = '';
  notificationChannel = '';
  const data = createSendData(process.env['SERVICE_TYPE'], event);
  const options = {
    hostname: hostName,
    path: notificationChannel,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(data),
    },
  };

  if (notificationChannel) {
    const req = https.request(options, (res) => {
      if (res.statusCode === 200) {
        console.log('OK:' + res.statusCode);
      } else {
        console.log('Error:' + res.statusCode);
      }
    });
    req.write(data);
    req.end();
    return new Promise((resolve) => req.on('response', () => resolve()));
  } else {
    return null;
  }
};
