output "lambda_function_rule_violation_arn" {
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数が使用するIAM RoleのARNを設定する。"
  value       = aws_lambda_function.rule_violation.arn
  sensitive   = true
}
