output "codebuild_project_name" {
  description = "CodeBuildのプロジェクト名"
  value       = aws_codebuild_project.this.name
}

output "codebuild_arn" {
  description = "CodeBuildのARN"
  value       = aws_codebuild_project.this.arn
}

output "codebuild_service_role_arn" {
  description = "作成したパイプラインが用いるサービスロールのARN"
  value       = aws_iam_role.codebuild.arn
}
