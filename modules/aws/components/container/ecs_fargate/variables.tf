variable "name" {
  description = "ECSクラスター、タスク定義のNameタグの一部に使用する"
  type        = string
}

variable "tags" {
  description = "ECSリソースに付与するタグ"
  type        = map(string)
}

variable "cluster_name" {
  description = "ECSクラスター名"
  type        = string
}

variable "desired_count" {
  description = "ECSタスクのインスタンス数"
  type        = number
}

variable "platform_version" {
  description = "ECSサービス内のタスクが実行される、プラットフォームのバージョン"
  type        = string
}

variable "cpu" {
  description = "ECSタスクの使用するCPU数"
  type        = string
}

variable "memory" {
  description = "ECSタスクに適用されるメモリのハード制限"
  type        = string
}

variable "task_role_arn" {
  description = "ECSタスクに与えるIAMロールのARN"
  type        = string
}

variable "execution_role_arn" {
  description = "ECSタスクに付与する実行IAMロールのARN"
  type        = string
}

variable "subnets" {
  description = "ECSサービスを配置するサブネットID"
  type        = list(string)
}

variable "security_groups" {
  description = "ECSサービスに適用するセキュリティグループID"
  type        = list(string)
}

variable "container_definitions" {
  description = "タスクとして動作するコンテナ定義"
  type        = string
}


variable "service_association" {
  description = "コンテナへ接続する際のエンドポイントに使うサービス（`LoadBalancer`または`ServiceDiscovery`を指定）"
  type        = string
  default     = "LoadBalancer"
}

variable "load_balancer_target_group_arn" {
  description = "ECSサービスに紐付けるロードバランサーのARN（`service_association`で`LoadBalancer`を指定した場合は指定必須）"
  type        = string
  default     = null
}

variable "container_port" {
  description = "ロードバランサーからコンテナに転送するポート（`service_association`で`LoadBalancer`を指定した場合は指定必須）"
  type        = number
  default     = null
}

variable "service_registry_arn" {
  description = "Service DiscoveryのサービスのARN（`service_association`で`ServiceDiscovery`を指定した場合は指定必須）"
  type        = string
  default     = null
}

variable "enable_code_deploy" {
  description = "AWS Code DeployによるCDの有効化（タスク定義変更によるコンテナ再デプロイが行われなくなります）"
  type        = bool
  default     = true
}
