output "cluster_name" {
  description = "ECSクラスター名"
  value       = aws_ecs_cluster.this.name
}

output "service_name" {
  description = "ECSサービス名"
  value       = var.enable_code_deploy ? aws_ecs_service.code_deploy_type[0].name : aws_ecs_service.ecs_type[0].name
}
