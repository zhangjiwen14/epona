output "arns" {
  description = "ECRリポジトリの名前とARNのマップ"
  value       = { for r in aws_ecr_repository.this : r.name => r.arn }
}

output "urls" {
  description = "ECRリポジトリの名前とURLのマップ"
  value       = { for r in aws_ecr_repository.this : r.name => r.repository_url }
}
