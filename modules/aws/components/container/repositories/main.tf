locals {
  repositories_with_lifecycle_policy  = [for r in var.repositories : r if lookup(r, "lifecycle_policy", null) != null]
  repositories_with_repository_policy = [for r in var.repositories : r if lookup(r, "repository_policy", null) != null]
}

resource "aws_ecr_repository" "this" {
  count = length(var.repositories)
  name  = var.repositories[count.index].name

  image_tag_mutability = lookup(var.repositories[count.index], "image_tag_mutability", "MUTABLE")

  image_scanning_configuration {
    scan_on_push = lookup(var.repositories[count.index], "image_scan_on_push", true)
  }

  tags = merge(
    {
      "Name" = var.repositories[count.index].name
    },
    var.tags
  )
}

resource "aws_ecr_lifecycle_policy" "this" {
  count = length(local.repositories_with_lifecycle_policy) > 0 ? length(local.repositories_with_lifecycle_policy) : 0

  repository = local.repositories_with_lifecycle_policy[count.index].name

  policy = local.repositories_with_lifecycle_policy[count.index].lifecycle_policy

  # ECR作成後に実行する必要があるため、aws_ecr_repository.thisへの依存関係を設定
  depends_on = [
    aws_ecr_repository.this
  ]
}

resource "aws_ecr_repository_policy" "this" {
  count = length(local.repositories_with_repository_policy) > 0 ? length(local.repositories_with_repository_policy) : 0

  repository = local.repositories_with_repository_policy[count.index].name

  policy = local.repositories_with_repository_policy[count.index].repository_policy

  # ECR作成後に実行する必要があるため、aws_ecr_repository.thisへの依存関係を設定
  depends_on = [
    aws_ecr_repository.this
  ]
}
