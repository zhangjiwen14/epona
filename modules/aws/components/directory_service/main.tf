resource "aws_directory_service_directory" "this" {
  name        = var.name
  description = "virtual secure room directory"
  password    = var.password
  short_name  = var.short_name
  type        = "MicrosoftAD"
  edition     = var.edition

  vpc_settings {
    vpc_id     = var.vpc_id
    subnet_ids = var.subnet_ids
  }

  tags = var.tags
}
