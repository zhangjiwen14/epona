variable "name" {
  description = "アプリケーション名"
  type        = string
}

variable "deployment_group_name_ecs" {
  description = "ECSへデプロイするデプロイメントグループ名"
  type        = string
}

variable "deployment_config_name" {
  description = "デプロイにおいてCodeDeployが使用するルール名。https://docs.aws.amazon.com/ja_jp/codedeploy/latest/userguide/deployment-configurations.html#deployment-configuration-ecsを参照"
  default     = "CodeDeployDefault.ECSAllAtOnce"
  type        = string
}

variable "auto_rollback_events" {
  description = "自動ロールバックを行うイベント名のリスト"
  type        = list(string)
  default     = ["DEPLOYMENT_FAILURE"]
}

variable "action_on_timeout" {
  description = "新環境へトラフィックをルーティングするタイミング指定"
  type        = string
  default     = "CONTINUE_DEPLOYMENT"
}

variable "wait_time_in_minutes" {
  description = "action_on_timeoutにSTOP_DEPLOYMENTに指定された場合、手動でのルーティング切り替えが指示されなかったときにデプロイを失敗させるタイムアウト指定"
  type        = number
  default     = 10
}

variable "termination_wait_time_in_minutes" {
  description = "Blue-Green Deploymentが成功したとき、何分待って旧環境のサービスを終了させるか"
  type        = number
  default     = 1
}

variable "cluster_name" {
  description = "デプロイ対象のECSクラスター名"
  type        = string
}

variable "service_name" {
  description = "デプロイ対象のECSサービス名"
  type        = string
}

variable "prod_listener_arns" {
  description = "本番環境用トラフィックを受け持つ、ロードバランサーのリスナーARNのリスト"
  type        = list(string)
}

variable "test_listener_arns" {
  description = "テスト用トラフィックを受け持つ、ロードバランサーのリスナーARNのリスト"
  type        = list(string)
  default     = []
}

variable "target_group_names" {
  description = "Blue-Green Deploymentに使用するターゲットグループ名のリスト"
  type        = list(string)
}
