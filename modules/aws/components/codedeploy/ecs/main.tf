resource "aws_codedeploy_app" "this" {
  compute_platform = "ECS"
  name             = var.name
}

resource "aws_codedeploy_deployment_group" "ecs" {
  app_name               = aws_codedeploy_app.this.name
  deployment_group_name  = var.deployment_group_name_ecs
  service_role_arn       = aws_iam_role.codedeploy.arn
  deployment_config_name = var.deployment_config_name

  auto_rollback_configuration {
    enabled = true
    events  = var.auto_rollback_events
  }

  blue_green_deployment_config {
    deployment_ready_option {
      action_on_timeout    = var.action_on_timeout
      wait_time_in_minutes = var.action_on_timeout == "STOP_DEPLOYMENT" ? var.wait_time_in_minutes : null
    }

    terminate_blue_instances_on_deployment_success {
      action                           = "TERMINATE"
      termination_wait_time_in_minutes = var.termination_wait_time_in_minutes
    }
  }

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }

  ecs_service {
    cluster_name = var.cluster_name
    service_name = var.service_name
  }

  load_balancer_info {
    target_group_pair_info {
      prod_traffic_route {
        listener_arns = var.prod_listener_arns
      }

      dynamic "test_traffic_route" {
        for_each = length(var.test_listener_arns) > 0 ? ["dummy"] : []

        content {
          listener_arns = var.test_listener_arns
        }
      }

      dynamic "target_group" {
        for_each = var.target_group_names

        content {
          name = target_group.value
        }
      }
    }
  }
}
