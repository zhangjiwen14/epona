# GuardDutyを有効化する
resource "aws_guardduty_detector" "threat_detection" {
  count = var.create ? 1 : 0

  enable                       = true
  finding_publishing_frequency = var.finding_publishing_frequency
  tags                         = var.tags
}
