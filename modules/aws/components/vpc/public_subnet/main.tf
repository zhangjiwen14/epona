resource "aws_subnet" "this" {
  vpc_id = var.vpc_id

  count = length(var.subnets)

  cidr_block        = var.subnets[count.index]
  availability_zone = var.availability_zones[count.index]

  map_public_ip_on_launch = true

  tags = merge(
    {
      "Name" = format("%s-public-%s", var.name, var.availability_zones[count.index])
    },
    var.tags
  )
}

resource "aws_internet_gateway" "this" {
  vpc_id = var.vpc_id

  count = length(var.subnets) > 0 ? 1 : 0

  tags = merge(
    {
      "Name" = format("%s", var.name)
    },
    var.tags
  )
}

resource "aws_route_table" "this" {
  vpc_id = var.vpc_id

  count = length(var.subnets) > 0 ? 1 : 0

  tags = merge(
    {
      "Name" = format("%s-public", var.name)
    },
    var.tags
  )
}

resource "aws_route" "internet_gateway" {
  count = length(var.subnets) > 0 ? 1 : 0

  route_table_id         = aws_route_table.this[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this[0].id
}

resource "aws_route_table_association" "this" {
  count = length(var.subnets)

  subnet_id      = aws_subnet.this[count.index].id
  route_table_id = aws_route_table.this[0].id
}
