variable "name" {
  description = "パブリックサブネットに関するリソースの、Nameタグの一部に使用する"
  type        = string
}

variable "tags" {
  description = "パブリックサブネットに関するリソースに、共通的に付与するタグ"
  type        = map(string)
}

variable "vpc_id" {
  description = "パブリックサブネットを配置する、VPC IC"
  type        = string
}

variable "availability_zones" {
  description = "パブリックサブネットを配置するAZ"
  type        = list(string)
}

variable "subnets" {
  description = "パブリックサブネットに割り当てる、CIDRブロック"
  type        = list(string)
}
