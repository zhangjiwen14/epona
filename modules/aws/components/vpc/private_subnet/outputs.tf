output "subnets" {
  value = aws_subnet.this.*.id
}

output "private_route_tables" {
  value = aws_route_table.this.*.id
}