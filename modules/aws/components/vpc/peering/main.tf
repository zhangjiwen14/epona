resource "aws_vpc_peering_connection" "this" {
  peer_vpc_id = var.accepter_vpc_id
  vpc_id      = var.requester_vpc_id
  auto_accept = true

  tags = merge(
    {
      "Name" = var.name
    },
    var.tags
  )
}
