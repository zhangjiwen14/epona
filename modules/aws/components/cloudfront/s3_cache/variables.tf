variable "enabled" {
  description = "作成するCloudFrontの有効化"
  type        = bool
  default     = true
}

variable "default_root_object" {
  description = "ルートへのアクセス時に表示させるファイル名（例: index.html）"
  type        = string
  default     = null
}

variable "aliases" {
  description = "CloudFrontに設定するCNAME"
  type        = list(string)
  default     = []
}

variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "http_version" {
  description = "CloudFrontでサポートする最大のHTTPのバージョン（http1.1 or http2）"
  type        = string
  default     = null
}

variable "is_ipv6_enabled" {
  description = "CloudFrontでのIPv6有効化"
  type        = bool
  default     = null
}

variable "price_class" {
  description = "CloudFrontで利用する価格クラス（PriceClass_All or PriceClass_200 or PriceClass_100）"
  type        = string
  default     = null
}

variable "web_acl_id" {
  description = "CloudFrontで利用するWebACLのARN"
  type        = string
  default     = null
}



variable "origin" {
  description = "CloudFrontの配信ソースの設定"
  type = object({
    domain_name    = string
    origin_id      = string
    origin_path    = string
    custom_headers = list(map(string))
    s3_origin_config = object({
      origin_access_identity = string
    })
  })
}

variable "default_cache_behavior" {
  description = "CloudFrontのデフォルトのキャッシュ動作設定"
  type = object({
    target_origin_id           = string
    allowed_methods            = list(string)
    cached_methods             = list(string)
    viewer_protocol_policy     = string
    compress                   = bool
    min_ttl                    = number
    default_ttl                = number
    max_ttl                    = number
    viewer_response_lambda_arn = string
  })
}

variable "viewer_certificate" {
  description = "CloudFrontのSSL化の設定"
  type        = map(string)
}

variable "custom_error_responses" {
  description = "CloudFrontのエラーページアクセス時の動作設定"
  type        = list(map(string))
}

variable "logging_config" {
  description = "CloudFrontのアクセスログの設定"
  type        = map(string)
}
