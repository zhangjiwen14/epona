output "domain_name" {
  description = "作成したCloudFront Distributionのドメイン名"
  value       = aws_cloudfront_distribution.s3_cache.domain_name
}

output "hosted_zone_id" {
  description = "作成したCloudFront DistributionのゾーンID"
  value       = aws_cloudfront_distribution.s3_cache.hosted_zone_id
}

output "id" {
  description = "CloudFrontのID"
  value       = aws_cloudfront_distribution.s3_cache.id
}

output "aliases" {
  description = "CloudFrontのドメインに設定した別名（CNAME）"
  value       = aws_cloudfront_distribution.s3_cache.aliases
}
