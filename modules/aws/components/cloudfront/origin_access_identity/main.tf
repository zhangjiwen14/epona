resource "aws_cloudfront_origin_access_identity" "this" {
  comment = "for ${var.s3_frontend_bucket_name}"
}
