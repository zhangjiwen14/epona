variable "s3_frontend_bucket_name" {
  description = "フロントの静的ファイルを配置する場所として作成するS3バケット名"
  type        = string
  default     = null
}
