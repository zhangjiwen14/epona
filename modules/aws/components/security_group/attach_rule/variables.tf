variable "target_security_group_id" {
  description = "ルールを適用するセキュリティグループのID"
  type        = string
}

variable "ingresses" {
  description = "CIDR BLOCKを対象とするingressのリスト"
  type = list(object({
    port        = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  default = []
}

variable "sg_ingresses" {
  description = "SecurityGroupを対象とするingressのリスト"
  type = list(object({
    port              = number
    protocol          = string
    security_group_id = string
    description       = string
  }))
  default = []
}

variable "egresses" {
  description = "CIDR BLOCKを対象とするegressのリスト"
  type = list(object({
    port        = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
  default = []
}

variable "sg_egresses" {
  description = "SecurityGroupを対象とするegressのリスト"
  type = list(object({
    port              = number
    protocol          = string
    security_group_id = string
    description       = string
  }))
  default = []
}
