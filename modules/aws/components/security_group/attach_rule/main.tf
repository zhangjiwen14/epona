# cidr blockを指定するingress定義
resource "aws_security_group_rule" "cidr_ingress" {
  for_each = { for ingress in var.ingresses : join(":", [ingress.protocol, ingress.port, join("-", ingress.cidr_blocks)]) => ingress }

  type              = "ingress"
  from_port         = each.value.port
  to_port           = each.value.port
  protocol          = each.value.protocol
  cidr_blocks       = each.value.cidr_blocks
  description       = each.value.description
  security_group_id = var.target_security_group_id
}

# security groupを指定するingress
resource "aws_security_group_rule" "sg_ingress" {
  for_each = { for ingress in var.sg_ingresses : join(":", [ingress.protocol, ingress.port, ingress.security_group_id]) => ingress }

  type                     = "ingress"
  from_port                = each.value.port
  to_port                  = each.value.port
  protocol                 = each.value.protocol
  source_security_group_id = each.value.security_group_id
  description              = each.value.description
  security_group_id        = var.target_security_group_id
}

# cidr blockを指定するegress定義
resource "aws_security_group_rule" "cidr_egress" {
  for_each = { for egress in var.egresses : join(":", [egress.protocol, egress.port, join("-", egress.cidr_blocks)]) => egress }

  type              = "egress"
  from_port         = each.value.port
  to_port           = each.value.port
  protocol          = each.value.protocol
  cidr_blocks       = each.value.cidr_blocks
  description       = each.value.description
  security_group_id = var.target_security_group_id
}

# security groupを指定するegress定義
resource "aws_security_group_rule" "sg_egress" {
  for_each = { for egress in var.sg_egresses : join(":", [egress.protocol, egress.port, egress.security_group_id]) => egress }

  type                     = "egress"
  from_port                = each.value.port
  to_port                  = each.value.port
  protocol                 = each.value.protocol
  source_security_group_id = each.value.security_group_id
  description              = each.value.description
  security_group_id        = var.target_security_group_id
}
