resource "aws_kinesis_firehose_delivery_stream" "this" {
  name        = "aws-waf-logs-${var.name_suffix}"
  destination = "extended_s3"
  tags        = var.tags

  server_side_encryption {
    enabled = true
  }

  extended_s3_configuration {
    role_arn           = var.iam_role_arn
    bucket_arn         = var.bucket_arn
    prefix             = var.prefix
    compression_format = var.compression_format
  }
}

resource "aws_wafv2_web_acl_logging_configuration" "this" {
  log_destination_configs = [aws_kinesis_firehose_delivery_stream.this.arn]
  resource_arn            = var.waf_webacl_arn
}
