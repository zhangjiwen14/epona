variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "lambda_function_name" {
  description = "検出結果を送信するLambda関数の名前"
  type        = string
}

variable "lambda_function_threat_detection_arn" {
  description = "検出結果を送信するLambda関数のARN"
  type        = string
}
