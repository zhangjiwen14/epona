output "cloudwatch_event_rule_threat_detection_arn" {
  description = "GuardDutyによる検出をトリガーとするイベントのルールのARN"
  value       = aws_cloudwatch_event_rule.threat_detection.arn
  sensitive   = true
}
