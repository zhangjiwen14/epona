# Eventのルール作成
resource "aws_cloudwatch_event_rule" "threat_detection" {
  description   = "GuardDuty検知をトリガーとするイベントのルール"
  name          = "${var.lambda_function_name}_rule"
  tags          = var.tags
  event_pattern = <<PATTERN
{
  "source": [
    "aws.guardduty"
  ],
  "detail-type": [
    "GuardDuty Finding"
  ]
}
PATTERN
}

# GuardDuty検知イベントにターゲットを設定する
resource "aws_cloudwatch_event_target" "threat_detection" {
  rule = aws_cloudwatch_event_rule.threat_detection.name
  arn  = var.lambda_function_threat_detection_arn
}
