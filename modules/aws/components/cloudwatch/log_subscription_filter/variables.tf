variable "name" {
  description = "サブスクリプションフィルター名"
  type        = string
}

variable "log_group_name" {
  description = "サブスクリプションフィルターを紐付けるロググループ名"
  type        = string
}

variable "filter_pattern" {
  description = "サブスクライブ対象となるイベントを絞り込む、パターン"
  type        = string
  default     = ""
}

variable "destination_arn" {
  description = "イベントを受信する、Kinesis StreamまたはLambdaのARN"
  type        = string
}

variable "role_arn" {
  description = "CloudWatch Logsがイベントを送信先に送るために必要な権限を含んだIAMロールARN"
  type        = string
  default     = null
}

variable "distribution" {
  description = "Kinesis Streamを使う場合に、RandomまたはByLogStreamを指定する"
  type        = string
  default     = null
}