locals {
  computed_log_group_name = var.lambda_function_name != null ? "/aws/lambda/${var.lambda_function_name}" : var.log_group_name
}

resource "aws_cloudwatch_log_group" "this" {
  name              = local.computed_log_group_name
  retention_in_days = var.log_group_retention_in_days
  kms_key_id        = var.log_group_kms_key_id
  tags              = var.tags
}
