output "log_group_arn" {
  description = "作成した、CloudWatch LogsロググループのARN"
  value       = aws_cloudwatch_log_group.this.arn
}

output "log_group_name" {
  description = "作成した、CloudWatch Logsロググループ名"
  value       = aws_cloudwatch_log_group.this.name
}
