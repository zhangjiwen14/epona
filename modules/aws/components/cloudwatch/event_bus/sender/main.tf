# Cloudwatch events と Event bus の紐付け
resource "aws_cloudwatch_event_target" "this" {
  for_each = toset(var.cloudwatch_event_rule_names)

  rule      = each.value
  target_id = each.value
  arn       = var.target_event_bus_arn
  role_arn  = var.role_arn
}
