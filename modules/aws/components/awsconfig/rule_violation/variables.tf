variable "s3_bucket_name" {
  type        = string
  description = "AWS Configが収集したリソースの情報を送信するS3バケット名を設定する。"
}
variable "aws_config_role_arn" {
  type        = string
  description = "AWS Configが情報を収集するために使用するIAM RoleのARNを設定する。"
}
variable "aws_config_lambda_arn" {
  type        = string
  description = "設定変更またはルール違反に関する情報を通知先へ送信する関数のARNを設定する。"
}
variable "notification_topic_name" {
  type        = string
  description = "AWS Configがリソースの設定変更またはルール違反を通知するトピック名を設定する。"
}
variable "aws_config_recorder_name" {
  type        = string
  description = "AWS Configがリソースの設定変更を検出するための「設定レコーダー」の名称を設定する。"
}
variable "aws_config_channel_name" {
  type        = string
  description = "AWS Configがリソースの状態を配信するための「配信チャネル」の名称を設定する。"
}
variable "s3_bucket_force_destroy" {
  description = "AWS Configが使用するS3バケットを強制的に削除するかどうかを設定する。"
  type        = bool
}
variable "bucket_transitions" {
  description = "AWS Configが使用するS3バケットに対するポリシーを設定する。未設定の場合は30日後にAmazon S3 Glacierへ移行する。"
  type        = list(map(string))
}
variable "recorder_status" {
  description = "AWS Configを有効にするかどうかを設定する。"
  type        = bool
}
variable "delivery_frequency" {
  description = "AWSConfigが設定スナップショットを配信する頻度を設定する。"
  type        = string
}
variable "configrule_prefix" {
  description = "既存ルールとの重複を避けるため、ルール名に付与するプレフィックスを設定する。"
  type        = string
}
variable "configrule_access_key_max_age" {
  description = "ACCESS_KEYS_ROTATEDルールのアクセスキーのローテーション期間を設定する。"
  type        = string
}
variable "configrule_access_key_frequency" {
  description = "ACCESS_KEYS_ROTATEDルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_acm_days_to_expiration" {
  description = "ACM_CERTIFICATE_EXPIRATION_CHECKルールのACM証明書の有効期限を設定する。"
  type        = string
}
variable "configrule_acm_certificate_frequency" {
  description = "ACM_CERTIFICATE_EXPIRATION_CHECKルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_cloud_trail_encryption_frequency" {
  description = "CLOUD_TRAIL_ENCRYPTION_ENABLEDルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_cloud_trail_enabled_frequency" {
  description = "CLOUD_TRAIL_ENABLEDルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_cloud_trail_validation_frequency" {
  description = "CLOUD_TRAIL_LOG_FILE_VALIDATION_ENABLEDルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_password_max_age" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、パスワードの有効期限を指定する場合に設定する。"
  type        = string
}
variable "configrule_password_min_length" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、パスワードの最小の長さを指定する場合に設定する。"
  type        = string
}
variable "configrule_password_reuse_prevention" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、パスワードの再使用を禁止する世代数を指定する場合に設定する。"
  type        = string
}
variable "configrule_password_require_lower_case" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、小文字の使用有無を指定する場合に設定する。"
  type        = string
}
variable "configrule_password_require_numbers" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、数字の使用有無を指定する場合に設定する。"
  type        = string
}
variable "configrule_password_require_symbols" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、記号の使用有無を指定する場合に設定する。"
  type        = string
}
variable "configrule_password_require_upper_case" {
  description = "IAM_PASSWORD_POLICYルールのチェック時、大文字の使用有無を指定する場合に設定する。"
  type        = string
}
variable "configrule_password_policy_frequency" {
  description = "IAM_PASSWORD_POLICYルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_iam_root_access_key_frequency" {
  description = "IAM_ROOT_ACCESS_KEY_CHECKルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_iam_mfa_enabled_frequency" {
  description = "MFA_ENABLED_FOR_IAM_CONSOLE_ACCESSルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_cloud_trail_multi_region_frequency" {
  description = "MULTI_REGION_CLOUD_TRAIL_ENABLEDルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_root_mfa_frequency" {
  description = "ROOT_ACCOUNT_HARDWARE_MFA_ENABLEDルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_s3_public_read_frequency" {
  description = "S3_BUCKET_PUBLIC_READ_PROHIBITEDルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_s3_public_write_frequency" {
  description = "S3_BUCKET_PUBLIC_WRITE_PROHIBITEDルールのチェック頻度を設定する。"
  type        = string
}
variable "configrule_waf_logging_frequency" {
  description = "WAFV2_LOGGING_ENABLEDルールのチェック頻度を設定する。"
  type        = string
}
variable "enable_default_rule" {
  description = "デフォルトルールを適用するかどうかを設定する。"
  type        = bool
}
variable "create_config_recorder_delivery_channel" {
  description = "AWS Configのリソースのうち、Config Recorder, Deliverly Channelを作成するかどうかを設定する。すでにAWS Configを使用しているリージョンにパターンを適用する場合はfalseとする。"
  type        = bool
}
variable "include_grobal_resources" {
  description = "グローバルリソースを監視対象に含めるかどうかを設定する。"
  type        = bool
}
variable "all_supported" {
  description = "リージョン内の全リソースを監視対象にするかどうかを設定する。`recording_resource_types`を設定する場合は`false`にする必要がある。"
  type        = bool
}
variable "recording_resource_types" {
  description = "監視対象とするリソースを指定する。このパラメータを設定する場合は`all_supported`は`false`にする必要がある。"
  type        = list(string)
}

