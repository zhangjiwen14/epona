# access-keys-rotated
resource "aws_config_config_rule" "access_keys_rotated" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-access-keys-rotated"
  description = "maxAccessKeyAgeで指定された日数以内にアクティブなアクセスキーがローテーションされるかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "ACCESS_KEYS_ROTATED"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
  input_parameters = jsonencode(
    {
      maxAccessKeyAge = var.configrule_access_key_max_age
    }
  )
  maximum_execution_frequency = var.configrule_access_key_frequency
}

# acm-certificate-expiration-check
resource "aws_config_config_rule" "acm_certificate_expiration_check" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-acm-certificate-expiration-check"
  description = "アカウントのACM証明書に、指定された日数内に有効期限がマークされているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "ACM_CERTIFICATE_EXPIRATION_CHECK"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
  input_parameters = jsonencode(
    {
      daysToExpiration = var.configrule_acm_days_to_expiration
    }
  )
  maximum_execution_frequency = var.configrule_acm_certificate_frequency
}

# cloud-trail-encryption-enabled
resource "aws_config_config_rule" "cloud_trail_encryption_enabled" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-cloud-trail-encryption-enabled"
  description = "AWS CloudTrailがサーバー側の暗号化（SSE）AWS Key Management Service（AWS KMS）カスタマーマスターキー（CMK）暗号化を使用するように構成されているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "CLOUD_TRAIL_ENCRYPTION_ENABLED"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_cloud_trail_encryption_frequency
}

# cloud-trail-enabled
resource "aws_config_config_rule" "cloud_trail_enabled" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-cloud-trail-enabled"
  description = "AWSアカウントでAWS CloudTrailが有効になっているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "CLOUD_TRAIL_ENABLED"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_cloud_trail_enabled_frequency
}

# cloud-trail-file-validation-enabled
resource "aws_config_config_rule" "cloud_trail_log_file_validation_enabled" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-cloud-trail-log-file-validation-enabled"
  description = "AWS CloudTrailがログ付きの署名済みダイジェストファイルを作成するかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "CLOUD_TRAIL_LOG_FILE_VALIDATION_ENABLED"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_cloud_trail_validation_frequency
}

# db-instance-backup-enabled
resource "aws_config_config_rule" "db_instance_backup_enabled" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-db-instance-backup-enabled"
  description = "RDS DBインスタンスでバックアップが有効になっているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "DB_INSTANCE_BACKUP_ENABLED"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

# ec2-instance-no-public-ip
resource "aws_config_config_rule" "ec2_instance_no_public_ip" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-ec2-instance-no-public-ip"
  description = "Amazon Elastic Compute Cloud（Amazon EC2）インスタンスにパブリックIPアソシエーションがあるかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "EC2_INSTANCE_NO_PUBLIC_IP"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

# ec2-instances-in-vpc
resource "aws_config_config_rule" "ec2_instances_in_vpc" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-ec2-instances-in-vpc"
  description = "EC2インスタンスが仮想プライベートクラウド（VPC）に属しているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "INSTANCES_IN_VPC"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

# elb-logging-enabled
resource "aws_config_config_rule" "elb_logging_enabled" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-elb-logging-enabled"
  description = "Application Load BalancerとClassic Load Balancerのログが有効になっているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "ELB_LOGGING_ENABLED"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

# iam-password-policy
resource "aws_config_config_rule" "iam_password_policy" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-iam-password-policy"
  description = "IAMユーザーのアカウントパスワードポリシーが指定された要件を満たしているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "IAM_PASSWORD_POLICY"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
  input_parameters = jsonencode(
    {
      MaxPasswordAge             = var.configrule_password_max_age
      MinimumPasswordLength      = var.configrule_password_min_length
      PasswordReusePrevention    = var.configrule_password_reuse_prevention
      RequireLowercaseCharacters = var.configrule_password_require_lower_case
      RequireNumbers             = var.configrule_password_require_numbers
      RequireSymbols             = var.configrule_password_require_symbols
      RequireUppercaseCharacters = var.configrule_password_require_upper_case
    }
  )
  maximum_execution_frequency = var.configrule_password_policy_frequency
}

### iam-policy-no-statements-with-admin-access
resource "aws_config_config_rule" "iam_policy_no_statements_with_admin_access" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-iam-policy-no-statements-with-admin-access"
  description = "全リソースに対する全アクションをAllowするIAMポリシーを作成していないかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "IAM_POLICY_NO_STATEMENTS_WITH_ADMIN_ACCESS"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

# iam-root-access-key-check
resource "aws_config_config_rule" "iam_root_access_key_check" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-iam-root-access-key-check"
  description = "rootユーザーアクセスキーが使用可能かどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "IAM_ROOT_ACCESS_KEY_CHECK"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_iam_root_access_key_frequency
}

# mfa-enabled-for-iam-console-access
resource "aws_config_config_rule" "mfa_enabled_for_iam_console_access" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-mfa-enabled-for-iam-console-access"
  description = "コンソールパスワードを使用するすべてのAWS Identity and Access Management（IAM）ユーザーに対してAWS Multi-Factor Authentication（MFA）が有効になっているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "MFA_ENABLED_FOR_IAM_CONSOLE_ACCESS"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_iam_mfa_enabled_frequency
}

### multi-region-cloudtrail-enabled
resource "aws_config_config_rule" "multi_region_cloudtrail_enabled" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-multi-region-cloudtrail-enabled"
  description = "少なくとも1つのマルチリージョンAWS CloudTrailがあることを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "MULTI_REGION_CLOUD_TRAIL_ENABLED"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_cloud_trail_multi_region_frequency
}

# rds-logging-enabled
resource "aws_config_config_rule" "rds_logging_enabled" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-rds-logging-enabled"
  description = "Amazon Relational Database Service（Amazon RDS）の各ログが有効になっていることを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "RDS_LOGGING_ENABLED"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

# rds-instance-public-access-check
resource "aws_config_config_rule" "rds_instance_public_access_check" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-rds-instance-public-access-check"
  description = "Amazon Relational Database Serviceインスタンスがパブリックにアクセスできないかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "RDS_INSTANCE_PUBLIC_ACCESS_CHECK"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

# rds-snapshots-public-prohibited
resource "aws_config_config_rule" "rds_snapshots_public_prohibited" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-rds-snapshots-public-prohibited"
  description = "Amazon Relational Database Service（Amazon RDS）スナップショットが公開されているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "RDS_SNAPSHOTS_PUBLIC_PROHIBITED"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

# restricted-ssh
resource "aws_config_config_rule" "restricted_ssh" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-restricted-ssh"
  description = "セキュリティグループの受信SSHトラフィックにアクセスできるかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "INCOMING_SSH_DISABLED"
  }
  depends_on = [aws_config_configuration_recorder.aws_config]
}

# root-account-hardware-mfa-enabled
resource "aws_config_config_rule" "root_account_hardware_mfa_enabled" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-root-account-hardware-mfa-enabled"
  description = "AWSアカウントがマルチ要素認証（MFA）ハードウェアデバイスを使用してルート認証情報でサインインできるかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "ROOT_ACCOUNT_HARDWARE_MFA_ENABLED"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_root_mfa_frequency
}

# s3-bucket-public-read-prohibited
resource "aws_config_config_rule" "s3_bucket_public_read_prohibited" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-s3-bucket-public-read-prohibited"
  description = "Amazon S3バケットがパブリック読み取りアクセスを許可していないことを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "S3_BUCKET_PUBLIC_READ_PROHIBITED"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_s3_public_read_frequency
}

# s3-bucket-public-write-prohibited
resource "aws_config_config_rule" "s3_bucket_public_write_prohibited" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-s3-bucket-public-write-prohibited"
  description = "Amazon S3バケットがパブリック書き込みアクセスを許可していないことを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "S3_BUCKET_PUBLIC_WRITE_PROHIBITED"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_s3_public_write_frequency
}

# wafv2-logging-enabled
resource "aws_config_config_rule" "wafv2_logging_enabled" {
  count       = var.enable_default_rule ? 1 : 0
  name        = "${var.configrule_prefix}-wafv2-logging-enabled"
  description = "AWSウェブアプリケーションファイアウォール（WAFV2）のリージョンおよびグローバルウェブアクセスコントロールリスト（ACL）でロギングが有効になっているかどうかを確認します。"
  source {
    owner             = "AWS"
    source_identifier = "WAFV2_LOGGING_ENABLED"
  }
  depends_on                  = [aws_config_configuration_recorder.aws_config]
  maximum_execution_frequency = var.configrule_waf_logging_frequency
}
