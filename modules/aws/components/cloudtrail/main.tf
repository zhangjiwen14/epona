# TODO: VPC -> CloudTrail へは VPC Endpoint 経由にすべき?
# https://aws.amazon.com/jp/about-aws/whats-new/2018/08/aws-cloudtrail-adds-vpc-endpoint-support-to-aws-privatelink/
resource "aws_cloudtrail" "this" {
  name                          = var.name
  enable_logging                = true
  s3_bucket_name                = var.bucket_name
  enable_log_file_validation    = true
  is_multi_region_trail         = var.is_multi_region_trail
  include_global_service_events = true

  # TODO: 以下は現状未使用のため現状コメントアウト。CloudTrail の証跡を CloudWatch で監視するなら必要になり得る
  # cloud_watch_logs_role_arn = var.cloud_watch_logs_role_arn != null? var.cloud_watch_logs_role_arn : null
  # cloud_watch_logs_group_arn = var.cloud_watch_logs_group_arn != null? var.cloud_watch_logs_group_arn : null

  tags = merge(
    {
      "Name" = var.name
    },
    var.tags
  )
  kms_key_id = var.kms_key_arn
  depends_on = [aws_s3_bucket.cloudtrail]

  event_selector {
    read_write_type           = var.read_write_type
    include_management_events = true
    # FIXME: exclude_management_event_sourcesがProviderに追加されたら設定を追加

    # data event を監視する
    dynamic "data_resource" {
      for_each = var.data_resources

      content {
        type   = data_resource.value["type"]
        values = data_resource.value["values"]
      }
    }
  }
}
