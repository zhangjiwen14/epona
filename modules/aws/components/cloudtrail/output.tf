output "cloudtrail_arn" {
  description = "CloudTrailのARN"
  value       = aws_cloudtrail.this.arn
}

output "bucket_arn" {
  description = "証跡用bucketのARN"
  value       = aws_s3_bucket.cloudtrail.arn
}

output "bucket_id" {
  description = "証跡用bucketのID"
  value       = aws_s3_bucket.cloudtrail.id
}

output "bucket" {
  description = "証跡用bucket名"
  value       = aws_s3_bucket.cloudtrail.bucket
}
