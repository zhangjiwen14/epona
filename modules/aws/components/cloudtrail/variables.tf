variable "name" {
  description = "Trailの名称"
  type        = string
}

variable "tags" {
  description = "Trailに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "is_multi_region_trail" {
  description = "全リージョンの証跡を記録するか否か。https://aws.amazon.com/jp/cloudtrail/faqs/ を参照"
  type        = bool
  default     = true
}

variable "kms_key_arn" {
  description = "証跡を暗号化するのに使用するKMSの鍵のARN"
  type        = string
  default     = null
}

variable "read_write_type" {
  description = "管理イベントのうち読み取りイベント、書き込みイベントを証跡として残すかどうかの設定。読み取りイベントのみを証跡とする場合は\"ReadOnly\"、書き込みイベントのみの場合は \"WriteOnly\"、双方の場合は \"All\" を指定する"
  type        = string
  default     = "All"
}

variable "bucket_name" {
  description = "証跡を保存するS3 bucket名"
  type        = string
  default     = "cloudtrail"
}

variable "bucket_force_destroy" {
  description = "bucketとともに保存されているデータを強制的に削除可能にする。証跡は監査上重要なものであるため、削除可能にする(trueを設定する)のは開発用途に限定すること"
  type        = bool
  default     = false
}

variable "bucket_mfa_delete" {
  description = "証跡が保存されているS3 Bucketを削除するとき、ルートアカウントのMFAを要求する"
  type        = bool
  default     = true
}

variable "bucket_transitions" {
  description = "証跡の移行に対するポリシー設定。未設定の場合は30日後にAmazon S3 Glacierへ移行する"
  type        = list(map(string))
  default = [{
    days          = 30
    storage_class = "GLACIER"
  }]
}

variable "data_resources" {
  description = "Trailで監視するデータイベントのマップ。keyは一意の任意の文字列。type、およびvaluesの指定についてはhttps://docs.aws.amazon.com/awscloudtrail/latest/APIReference/API_DataResource.htmlを参照"
  type = map(object({
    type   = string
    values = list(string)
  }))
  default = {}
}
