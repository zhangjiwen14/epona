output "arn" {
  description = "ロギングに使用するバケットのARN"
  value       = length(aws_s3_bucket.this) > 0 ? aws_s3_bucket.this[0].arn : try("arn:aws:s3:::${var.bucket}", null)
}

output "bucket_id" {
  description = "ロギングに使用するバケットのID"
  value       = length(aws_s3_bucket.this) > 0 ? aws_s3_bucket.this[0].id : var.bucket
}

output "bucket" {
  description = "ロギングに使用するバケットの名前"
  value       = length(aws_s3_bucket.this) > 0 ? aws_s3_bucket.this[0].bucket : var.bucket
}

output "bucket_domain_name" {
  description = "ロギングに使用するバケットのドメイン名"
  value       = length(aws_s3_bucket.this) > 0 ? aws_s3_bucket.this[0].bucket_domain_name : try("${var.bucket}.s3.amazonaws.com", null)
}
