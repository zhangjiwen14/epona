variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "bucket" {
  description = "ロギングに使用するバケットの名前"
  type        = string
}

variable "create_bucket" {
  description = "新規にS3バケットを作成するか既存のバケットを使用するかを指定するフラグ（trueでバケットを新規に作成）"
  type        = bool
  default     = true
}

variable "acl" {
  description = "バケットに指定するACL（grantsとは併用不可）"
  type        = string
  default     = null
}

variable "grants" {
  description = "バケットに指定するACL（aclとは併用不可）"
  type = list(object({
    type        = string
    permissions = list(string)
    id          = string
  }))
  default = []
}

variable "bucket_policy" {
  description = "作成するS3バケットに付与するポリシーをJSONで指定する"
  type        = string
  default     = null
}

variable "force_destroy" {
  description = "destroy時にbucketとともに保存されているデータを強制的に削除可能にする"
  type        = bool
  default     = false
}
