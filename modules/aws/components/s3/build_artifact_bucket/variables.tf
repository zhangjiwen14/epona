variable "tags" {
  description = "S3バケットに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "buckets" {
  description = "S3バケットの定義(bucket_name,force_destroy,runtime_account_id)"
  type        = list(map(string))
}
