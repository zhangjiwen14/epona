# ciでデプロイするファイルを配置するバケットを作成
resource "aws_s3_bucket" "this" {
  count  = length(var.buckets)
  bucket = var.buckets[count.index].bucket_name
  acl    = "private"
  tags = merge(
    {
      "Name" = var.buckets[count.index].bucket_name
    },
    var.tags
  )

  force_destroy = var.buckets[count.index].force_destroy

  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

data "aws_iam_policy_document" "this" {
  count = length(var.buckets)

  statement {
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion"
    ]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.this[count.index].arn}/*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.buckets[count.index].runtime_account_id}:root"]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    effect    = "Allow"
    resources = [aws_s3_bucket.this[count.index].arn]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.buckets[count.index].runtime_account_id}:root"]
    }
  }
}

# 作成したバケットポリシーをciでデプロイするファイルを配置するバケットに設定
resource "aws_s3_bucket_policy" "this" {
  count = length(var.buckets)

  # S3バケット作成後に実行する必要があるため、aws_s3_bucket.thisへの依存関係を持ち込んでいる
  bucket = aws_s3_bucket.this[count.index].id
  policy = data.aws_iam_policy_document.this[count.index].json
}
