variable "bucket" {
  description = "通知を設定するバケット名"
  type        = string
}

variable "lambda_function_notifications" {
  description = <<DESCRIPTION
  バケットイベントの通知先をLambdaにする場合に、その通知設定をlistで指定する。listの要素は、mapで構成する。

  mapの構成要素は、以下の通り。必須要素は、lambda_function_arnおよびevents。
  {
    id = "この通知設定のユニークなID"
    lambda_function_arn = "通知を受け取るLambdaのARN"
    events = "どのイベントに対して通知を行うか。未指定の場合は、複数のイベントを指定する場合は、,区切りの文字列で指定すること"
    filter_prefix = "通知をオブジェクトのキー名のprefixで絞り込む場合に使用"
    filter_suffix = "通知をオブジェクトのキー名のsuffixで絞り込む場合に使用"
  }
  DESCRIPTION

  type    = list(map(string))
  default = null
}
