variable "tags" {
  description = "このモジュールで作成されるリソースに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "s3_frontend_bucket_name" {
  description = "フロントの静的ファイルを配置する場所として作成するS3バケット名"
  type        = string
  default     = null
}

variable "force_destroy" {
  description = "ファイルが存在した場合であっても、destroyでバケットごと強制削除できるようにする"
  type        = bool
  default     = false
}

variable "enable_logging" {
  description = "ログ出力を有効にする場合、trueを指定する"
  type        = bool
}

variable "logging_bucket" {
  description = "ログ出力用バケット名"
  type        = string
}

variable "logging_prefix" {
  description = "ログオブジェクトのprefix"
  type        = string
}

variable "cloudfront_origin_access_identity_iam_arn" {
  description = "Origin Access IdentityのARN"
  type        = string
  default     = null
}
