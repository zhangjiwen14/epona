output "arn" {
  description = "作成したパイプラインのARN"
  value       = aws_codepipeline.this.arn
}

output "codepipeline_service_role_arn" {
  description = "作成したパイプラインが用いるサービスロールのARN"
  value       = aws_iam_role.codepipeline.arn
}

output "artifact_store_bucket_arn" {
  description = "Artifact store用bucketのARN"
  value       = aws_s3_bucket.artifact_store_bucket.arn
}

output "deployment_setting_key" {
  description = "CodeDeploy用の設定ファイルのS3 Bucket Key"
  value       = var.deployment_setting_key
}
