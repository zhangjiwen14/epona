locals {
  setting_artifacts_name = "settings"
  repository_names       = keys(var.ecr_repositories)
}

resource "aws_codepipeline" "this" {
  name     = var.name
  role_arn = aws_iam_role.codepipeline.arn
  tags = merge(
    {
      "Name" = format("%s-bucket", var.name)
    },
    var.tags
  )

  artifact_store {
    type     = "S3"
    location = aws_s3_bucket.artifact_store_bucket.bucket
    encryption_key {
      id   = var.artifact_store_bucket_encryption_key_arn
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    # デプロイの設定ファイル (taskdef.json、appspec.yaml)をS3から読み取る
    action {
      name             = "CodeDeploySettings"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = "1"
      output_artifacts = [local.setting_artifacts_name]
      run_order        = 1
      role_arn         = var.cross_account_codepipeline_access_role_arn
      configuration = {
        S3Bucket             = var.source_bucket_name
        S3ObjectKey          = var.deployment_setting_key
        PollForSourceChanges = false
      }
    }

    dynamic "action" {
      for_each = toset(keys(var.ecr_repositories))

      content {
        name             = "Image${title(action.key)}"
        category         = "Source"
        owner            = "AWS"
        provider         = "ECR"
        version          = "1"
        output_artifacts = [var.ecr_repositories[action.key].artifact_name]
        run_order        = 2
        role_arn         = var.cross_account_codepipeline_access_role_arn

        configuration = {
          RepositoryName = action.key
          ImageTag       = var.ecr_repositories[action.key].tag
        }
      }
    }
  }

  dynamic "stage" {
    for_each = var.require_approval == true ? ["dummy"] : []

    content {
      name = "Approval"

      action {
        name     = "Approval"
        category = "Approval"
        owner    = "AWS"
        provider = "Manual"
        version  = "1"
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CodeDeployToECS"
      version         = "1"
      input_artifacts = concat([local.setting_artifacts_name], values(var.ecr_repositories)[*].artifact_name)

      configuration = {
        ApplicationName                = var.codedeploy_app_name
        DeploymentGroupName            = var.deployment_group_name
        TaskDefinitionTemplateArtifact = local.setting_artifacts_name
        TaskDefinitionTemplatePath     = var.task_definition_template_file
        AppSpecTemplateArtifact        = local.setting_artifacts_name
        AppSpecTemplatePath            = var.appspec_template_file

        # 現時点では 4 つまでしか指定できない
        # see: https://docs.aws.amazon.com/ja_jp/codepipeline/latest/userguide/reference-pipeline-structure.html#action-requirements
        Image1ArtifactName = length(local.repository_names) > 0 ? var.ecr_repositories[local.repository_names[0]].artifact_name : null
        Image2ArtifactName = length(local.repository_names) > 1 ? var.ecr_repositories[local.repository_names[1]].artifact_name : null
        Image3ArtifactName = length(local.repository_names) > 2 ? var.ecr_repositories[local.repository_names[2]].artifact_name : null
        Image4ArtifactName = length(local.repository_names) > 3 ? var.ecr_repositories[local.repository_names[3]].artifact_name : null

        Image1ContainerName = length(local.repository_names) > 0 ? var.ecr_repositories[local.repository_names[0]].container_name : null
        Image2ContainerName = length(local.repository_names) > 1 ? var.ecr_repositories[local.repository_names[1]].container_name : null
        Image3ContainerName = length(local.repository_names) > 2 ? var.ecr_repositories[local.repository_names[2]].container_name : null
        Image4ContainerName = length(local.repository_names) > 3 ? var.ecr_repositories[local.repository_names[3]].container_name : null
      }
    }
  }
}

# TODO: 承認者のロールhttps://docs.aws.amazon.com/ja_jp/codepipeline/latest/userguide/approvals-iam-permissions.html
