# デプロイ用のPipelineを起動可能とするポリシー
resource "aws_iam_role" "cloudwatch_events" {
  description        = "Role to start pipeline ${var.name}"
  name               = "StartPipelineRoleFor${var.name}"
  assume_role_policy = data.aws_iam_policy_document.cloudwatch_events_assume_role.json
}

data "aws_iam_policy_document" "cloudwatch_events_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "cloudwatch_events" {
  statement {
    effect = "Allow"
    resources = [
      aws_codepipeline.this.arn
    ]
    actions = [
      "codepipeline:StartPipelineExecution"
    ]
  }
}

resource "aws_iam_policy" "cloudwatch_events" {
  name   = format("StartPipeline%sPolicy", title(var.name))
  policy = data.aws_iam_policy_document.cloudwatch_events.json
}

resource "aws_iam_role_policy_attachment" "cloudwatch_events" {
  role       = aws_iam_role.cloudwatch_events.name
  policy_arn = aws_iam_policy.cloudwatch_events.arn
}

resource "aws_cloudwatch_event_rule" "ecr" {
  for_each = toset(keys(var.ecr_repositories))

  name          = format("Start%sWhenPushedTo%s", title(var.name), title(each.key))
  description   = "ECRに対して特定タグのコンテナがPUSHされた場合にPipelineを起動するルール"
  event_pattern = <<-JSON
  {
    "source": [
      "aws.ecr"
    ],
    "detail-type": [
      "ECR Image Action"
    ],
    "detail": {
        "action-type": [
          "PUSH"
        ],
        "image-tag": [
          ${jsonencode(var.ecr_repositories[each.key].tag)}
        ],
        "repository-name": [
          ${jsonencode(each.key)}
        ],
        "result": [
          "SUCCESS"
        ]
    }
  }
  JSON

  depends_on = [aws_codepipeline.this]
}

resource "aws_cloudwatch_event_rule" "s3" {
  name          = "Start${var.name}WhenPushedToBucket${var.artifact_store_bucket_name}"
  description   = "S3の特定のバケットに特定のファイル名のオブジェクトがPutされた場合にPipelineを起動するルール"
  event_pattern = <<-JSON
  {
    "source": [
      "aws.s3"
    ],
    "detail-type": [
      "AWS API Call via CloudTrail"
    ],
    "detail": {
        "eventSource": [
          "s3.amazonaws.com"
        ],
        "eventName": [
          "PutObject"
        ],
        "requestParameters": {
          "bucketName": [
            ${jsonencode(var.source_bucket_name)}
          ],
          "key": [
            ${jsonencode(var.deployment_setting_key)}
          ]
        }
    }
  }
  JSON

  depends_on = [aws_codepipeline.this]
}

resource "aws_cloudwatch_event_target" "ecr" {
  for_each = toset(keys(var.ecr_repositories))

  rule      = aws_cloudwatch_event_rule.ecr[each.key].name
  target_id = aws_cloudwatch_event_rule.ecr[each.key].name
  arn       = aws_codepipeline.this.arn
  role_arn  = aws_iam_role.cloudwatch_events.arn
}

resource "aws_cloudwatch_event_target" "s3" {
  rule      = aws_cloudwatch_event_rule.s3.name
  target_id = aws_cloudwatch_event_rule.s3.name
  arn       = aws_codepipeline.this.arn
  role_arn  = aws_iam_role.cloudwatch_events.arn
}
