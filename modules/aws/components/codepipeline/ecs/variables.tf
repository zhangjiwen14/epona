variable "name" {
  description = "パイプライン名"
  type        = string
}

variable "tags" {
  description = "パイプラインに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "require_approval" {
  description = "デプロイに管理者の承認を必要とするか"
  type        = bool
  default     = true
}

variable "source_bucket_name" {
  description = "デプロイ設定を配置するS3 bucket名"
  type        = string
}

variable "artifact_store_bucket_name" {
  description = "アーティファクトストアとして利用するS3 bucket名"
  type        = string
}

variable "artifact_store_bucket_transitions" {
  description = "Artifact Storeの移行に対するポリシー設定。最新でなくなったファイルに対して適用される。未設定の場合は30日後にAmazon S3 Glacierへ移行する"
  type        = list(map(string))
  default = [{
    days          = 30
    storage_class = "GLACIER"
  }]
}

variable "artifact_store_bucket_encryption_key_arn" {
  description = "Artifact Storeの暗号化に使うCMKのARN"
  type        = string
}

variable "artifact_store_bucket_force_destroy" {
  description = "Artifact Storeとして使っているS3を強制的に削除可能にするか否か"
  type        = bool
  default     = false
}

variable "ecr_repositories" {
  description = "デプロイ対象のECRリポジトリ名とobjectのマップ。objectは、tagキーの値がデプロイ対象のタグ名、artifact_nameキー、container_nameキーはそれぞれ当該リポジトリのイメージが対応するアーティファクト名、および、taskdef.json 上のコンテナ名を示す。"
  type = map(object({
    tag            = string
    artifact_name  = string
    container_name = string
  }))
}

variable "cross_account_codepipeline_access_role_arn" {
  description = "デプロイの設定ファイル(taskdef.json、appspec.yaml)を読み取るためのS3バケットへのアクセス、および、ECRリポジトリにアクセスする際に利用するRoleのARN。Delivery環境上のS3バケットやECRリポジトリにアクセスするためには、クロスアカウントのロールを指定する必要がある。"
  type        = string
}

variable "deployment_setting_key" {
  description = "デプロイの設定ファイル(taskdef.json、appspec.yaml)が入ったzipファイルを指すS3 Object Key"
  type        = string
  default     = "settings.zip"
}

variable "task_definition_template_file" {
  description = "TaskDefinitionのテンプレートファイル名"
  type        = string
  default     = "taskdef.json"
}

variable "appspec_template_file" {
  description = "CodeDeployが使用する設定ファイル"
  type        = string
  default     = "appspec.yaml"
}

variable "codedeploy_app_name" {
  description = "CodeDeployのアプリケーション名"
  type        = string
}

variable "deployment_group_name" {
  description = "CodeDeployのデプロイメントグループ名"
  type        = string
}
