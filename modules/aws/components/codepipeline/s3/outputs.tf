output "codepipeline_arn" {
  description = "作成したパイプラインのARN"
  value       = aws_codepipeline.this.arn
}

output "codepipeline_service_role_arn" {
  description = "作成したパイプラインが用いるサービスロールのARN"
  value       = aws_iam_role.codepipeline.arn
}

output "artifact_store_bucket_name" {
  description = "Artifact Store用S3 Bucketのバケット名"
  value       = aws_s3_bucket.s3_artifact_store.id
}

output "artifact_store_bucket_arn" {
  description = "Artifact Store用S3 BucketのARN"
  value       = aws_s3_bucket.s3_artifact_store.arn
}
