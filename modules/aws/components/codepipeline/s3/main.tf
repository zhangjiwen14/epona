locals {
  source_output_artifact = "source"
}

resource "aws_codepipeline" "this" {
  name     = var.pipeline_name
  role_arn = aws_iam_role.codepipeline.arn
  tags = merge(
    {
      "Name" = format("%s-pipeline", var.pipeline_name)
    },
    var.tags
  )

  artifact_store {
    type     = "S3"
    location = aws_s3_bucket.s3_artifact_store.bucket
    encryption_key {
      id   = var.artifact_store_bucket_encryption_key_arn
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    # CloudWatch Event で S3 Push を拾えるようにする
    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = "1"
      output_artifacts = [local.source_output_artifact]
      run_order        = 1
      # Delivery 環境の S3 へのアクセスを担うARN
      role_arn = var.cross_account_codepipeline_access_role_arn
      configuration = {
        S3Bucket             = var.source_bucket_name
        S3ObjectKey          = var.source_object_key
        PollForSourceChanges = false
      }
    }
  }

  dynamic "stage" {
    for_each = var.require_approval ? ["dummy"] : []

    content {
      name = "Approval"

      action {
        name     = "Approval"
        category = "Approval"
        owner    = "AWS"
        provider = "Manual"
        version  = "1"
      }
    }
  }

  stage {
    name = "DeployToS3Bucket"

    action {
      name            = "DeployToS3Bucket"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = [local.source_output_artifact]
      version         = "1"

      configuration = {
        ProjectName   = var.deployment_codebuild_project_name
        PrimarySource = local.source_output_artifact
      }
    }
  }
}

# TODO: 承認者のロールhttps://docs.aws.amazon.com/ja_jp/codepipeline/latest/userguide/approvals-iam-permissions.html
