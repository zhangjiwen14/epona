resource "aws_workspaces_directory" "this" {
  directory_id = var.directory_id
  subnet_ids   = var.subnet_ids

  self_service_permissions {
    change_compute_type  = var.change_compute_type
    increase_volume_size = var.increase_volume_size
    rebuild_workspace    = var.rebuild_workspace
    restart_workspace    = var.restart_workspace
    switch_running_mode  = var.switch_running_mode
  }
}

data "aws_workspaces_bundle" "standarad_win_10_j" {
  owner = "AMAZON"
  name  = "Standard with Windows 10(Japanese)"
}

resource "aws_workspaces_workspace" "this" {
  for_each = toset(var.user_names)

  directory_id = var.directory_id
  bundle_id    = var.bundle_id != null ? var.bundle_id : data.aws_workspaces_bundle.standarad_win_10_j.bundle_id
  user_name    = each.value

  root_volume_encryption_enabled = var.root_volume_encryption_enabled
  user_volume_encryption_enabled = var.user_volume_encryption_enabled
  volume_encryption_key          = var.volume_encryption_key

  workspace_properties {
    compute_type_name                         = var.compute_type_name
    user_volume_size_gib                      = var.user_volume_size_gib
    root_volume_size_gib                      = var.root_volume_size_gib
    running_mode                              = var.running_mode
    running_mode_auto_stop_timeout_in_minutes = var.running_mode_auto_stop_timeout_in_minutes
  }

  tags = merge(
    {
      "Name" = each.value
    },
    var.tags
  )

  depends_on = [aws_workspaces_directory.this]
}

resource "aws_workspaces_ip_group" "this" {
  name        = "${var.directory_id}-workspaces-control"
  description = "allow access IPs"

  dynamic "rules" {
    for_each = var.rules

    content {
      source      = rules.value.source_ip
      description = rules.value.description
    }

  }
}
