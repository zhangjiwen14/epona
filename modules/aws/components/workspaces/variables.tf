variable "tags" {
  description = "workspacesに関連するリソースに、共通的に付与するタグ"
  type        = map(string)
}

variable "directory_id" {
  description = "参加するDirectoryServiceのID"
  type        = string
}

variable "subnet_ids" {
  description = "DirectoryServiceが存在するサブネットIDのリスト"
  type        = list(string)
}

variable "change_compute_type" {
  description = "DirectoryServiceのユーザがVDIのbundleを変更できるかどうか"
  type        = bool
  default     = false
}

variable "increase_volume_size" {
  description = "DirectoryServiceのユーザがVDIのボリュームサイズを変更できるかどうか"
  type        = bool
  default     = false
}

variable "rebuild_workspace" {
  description = "DirectoryServiceのユーザがVDIを元の状態に再構築できるかどうか"
  type        = bool
  default     = false
}

variable "restart_workspace" {
  description = "DirectoryServiceのユーザがVDIを再起動できるかどうか"
  type        = bool
  default     = true
}

variable "switch_running_mode" {
  description = "DirectoryServiceのユーザがVDIの実行モードを変更できるかどうか"
  type        = bool
  default     = false
}

variable "user_names" {
  description = "Workspacesを利用するDirectory Serviceのユーザのリスト"
  type        = list(string)
}

variable "bundle_id" {
  description = "WorkspacesのOS種別。とくに指定しない場合「Standard with Windows 10(Japanese)」で作られる。"
  type        = string
  default     = null
}

variable "root_volume_encryption_enabled" {
  description = "ルートボリュームの暗号化"
  type        = bool
  default     = true
}

variable "user_volume_encryption_enabled" {
  description = "ユーザボリュームの暗号化"
  type        = bool
  default     = true
}

variable "volume_encryption_key" {
  description = "ボリューム暗号化に使用するCMKのエイリアス名"
  type        = string
}

variable "compute_type_name" {
  description = "Workspacesのタイプ(VALUE, STANDARD, PERFORMANCE, POWER, GRAPHICS, POWERPRO and GRAPHICSPRO)。それぞれのタイプの違いについては[Amazon WorkSpaces の特徴](https://aws.amazon.com/jp/workspaces/features/#Amazon_WorkSpaces_Bundles)を参照。"
  type        = string
}

variable "user_volume_size_gib" {
  description = "Workspacesのユーザボリューム(Dドライブ)の容量"
  type        = number
}

variable "root_volume_size_gib" {
  description = "Workspacesのルートボリューム(Cドライブ)の容量"
  type        = number
}

variable "running_mode" {
  description = "Workspacesの起動モード(AUTO_STOP or ALWAYS_ON)"
  type        = string
}

variable "running_mode_auto_stop_timeout_in_minutes" {
  description = "Workspaces起動から自動停止するまでの分数を60分単位で指定する。例：60, 120, 180"
  type        = number
}


variable "rules" {
  description = <<-DESC
  Workspacesへの接続を許可する送信元IPアドレス (CIDR で指定可能)、およびその説明。記述例は以下の通り。
  ```
  rules = [{
    source_ip   = "192.0.2.0/24",
    description = "運用者が所属するネットワーク"
  }]
  ```
  DESC
  type        = list(map(string))
}
