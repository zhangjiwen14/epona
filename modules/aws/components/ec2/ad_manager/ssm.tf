resource "aws_ssm_document" "domain_join_document" {
  name          = "${var.ssm_domain_name}_domain_join_doc"
  document_type = "Command"

  content = <<DOC
{
        "schemaVersion": "2.2",
        "description": "Join an instance to a domain",
        "mainSteps": [
          {
            "action": "aws:domainJoin",
            "name": "domainJoin",
            "inputs": {
              "directoryId": "${var.ssm_directory_service_id}",
              "directoryName": "${var.ssm_domain_name}",
              "directoryOU": "${var.ssm_ou}",
              "dnsIpAddresses": [
                 "${sort(var.ssm_dns_ip_addresses)[0]}",
                 "${sort(var.ssm_dns_ip_addresses)[1]}"
              ]
            }
          }
        ]
}
DOC

  tags = merge(
    {
      "Name" = "${var.name}-doc"
    },
    var.tags
  )

}

resource "aws_ssm_association" "ad_manager" {
  name = aws_ssm_document.domain_join_document.name

  targets {
    key    = "InstanceIds"
    values = [aws_instance.ad_manager.id]
  }
}