# common
variable "name" {
  description = "AD管理用EC2インスタンスのNameタグの値"
  type        = string
}

variable "tags" {
  description = "AD管理用EC2インスタンスに付与するタグ"
  type        = map(string)
  default     = {}
}

# EC2
variable "administrator_password" {
  description = "AD管理用EC2インスタンスのAdministratorのパスワード"
  type        = string
}

variable "ami" {
  description = "AD管理用EC2インスタンスインスタンスのAMI"
  type        = string
  default     = null
}

variable "instance_type" {
  description = "AD管理用EC2インスタンスのインスタンスタイプ"
  type        = string
}

variable "iam_instance_profile" {
  description = "AD管理用EC2インスタンスに付与するIAMインスタンスプロファイル"
  type        = string
}

variable "subnet_id" {
  description = "AD管理用EC2インスタンスを配置する、サブネットのID"
  type        = string
}

variable "vpc_security_group_ids" {
  description = "AD管理用EC2インスタンスに適用する、セキュリティグループID"
  type        = list(string)
}

variable "root_block_volume_size" {
  description = "AD管理用EC2インスタンスのルートデバイスボリュームのサイズ"
  type        = string
}

# SSM
variable "ssm_directory_service_id" {
  description = "AD管理用EC2インスタンスが参加するDirecotry ServiceのID"
  type        = string
}

variable "ssm_domain_name" {
  description = "AD管理用EC2インスタンスが参加するドメイン名"
  type        = string
}

variable "ssm_ou" {
  description = "AD管理用EC2インスタンスが参加するドメイン内の組織"
  type        = string
}

variable "ssm_dns_ip_addresses" {
  description = "AD管理用EC2インスタンスが参加するドメインのDNSリスト"
  type        = list(string)
}
