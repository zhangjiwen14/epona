resource "aws_eip" "ad_manager" {
  vpc      = true
  instance = aws_instance.ad_manager.id

  tags = merge(
    {
      "Name" = "${var.name}-eip"
    },
    var.tags
  )
}