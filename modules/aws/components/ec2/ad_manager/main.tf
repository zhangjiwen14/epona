locals {
  default_init_script = <<POWERSHELL
    <powershell>
    $Password = ConvertTo-SecureString "${var.administrator_password}" -AsPlainText -Force
    Get-LocalUser -Name "Administrator" | Set-LocalUser -Password $Password
    Install-WindowsFeature RSAT-ADDS
    Install-WindowsFeature RSAT-DNS-Server
    Install-WindowsFeature GPMC
    </powershell>
    POWERSHELL
}

data "aws_ami" "ad_mangaer" {
  most_recent = true

  filter {
    name   = "name"
    values = ["Windows_Server-2019-Japanese-Full-Base*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}

resource "aws_instance" "ad_manager" {
  ami                    = var.ami != null ? var.ami : data.aws_ami.ad_mangaer.id
  instance_type          = var.instance_type
  iam_instance_profile   = var.iam_instance_profile
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.vpc_security_group_ids

  root_block_device {
    volume_size = var.root_block_volume_size
  }

  user_data = local.default_init_script

  tags = merge(
    {
      "Name" = var.name
    },
    var.tags
  )
}
