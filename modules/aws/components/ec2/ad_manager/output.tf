output "public_dns" {
  description = "AD Manager の パブリックDNS"
  value       = aws_eip.ad_manager.public_dns
}

output "public_ip" {
  description = "AD Manager の パブリックIP"
  value       = aws_eip.ad_manager.public_ip
}