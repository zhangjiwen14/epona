variable "name" {
  description = "GitLab Runnerを稼働させるEC2インスタンスのNameタグの値"
  type        = string
}

variable "tags" {
  description = "GitLab Runnerを稼働させるEC2インスタンスに付与するタグ"
  type        = map(string)
  default     = {}
}

variable "ami" {
  description = "GitLab Runnerを稼働させるEC2インスタンスインスタンスのAMI"
  type        = string
}

variable "instance_type" {
  description = "GitLab Runnerを稼働させるEC2インスタンスのインスタンスタイプ"
  type        = string
}

variable "iam_instance_profile" {
  description = "GitLab Runnerを稼働させるEC2インスタンスに付与するIAMインスタンスプロファイル"
  type        = string
}

variable "root_block_volume_size" {
  description = "GitLab Runnerを稼働させるEC2インスタンスの、ルートデバイスボリュームのサイズ"
  type        = string
}

variable "subnet" {
  description = "GitLab Runnerを稼働させるEC2インスタンスを配置する、サブネットのID"
  type        = string
}

variable "security_groups" {
  description = "GitLab Runnerを稼働させるEC2インスタンスに適用する、セキュリティグループID"
  type        = list(string)
}

variable "gitlab_runner_gitlab_url" {
  description = "GitLab Runnerのアクセス先となる、GitLabのURL"
  type        = string
  default     = "https://gitlab.com"
}

variable "gitlab_runner_registration_token" {
  description = "GitLab RunnerのGitLabに登録するためのトークン。init_scriptを指定する場合は不要"
  type        = string
  default     = null # init_script を利用する場合は、registration-tokenは不要になるため
}

variable "gitlab_runner_pull_image_always" {
  description = <<DESCRIPTION
GitLab RunnerがCIジョブでコンテナイメージを取得する際に常に最新のイメージを利用するかどうかのフラグ。
値がtrueである場合、`pull_policy`に`always`が設定され、コンテナイメージは常に最新のイメージをpullする。
false(もしくはデフォルト値)を指定した場合、`pull_policy`に`always`が設定され、ローカルにイメージがある場合はローカルイメージを利用する。
DESCRIPTION
  type        = bool
  default     = false
}

variable "init_script" {
  description = "GitLab Runnerを稼働させるEC2インスタンスの構築時に適用する、Cloud Initスクリプト。GitLab RunnerおよびCIで必要なパッケージのインストールなどに使用する"
  type        = string
  default     = null
}
