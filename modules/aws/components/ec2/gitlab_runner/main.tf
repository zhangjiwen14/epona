locals {
  default_init_script = <<SHELLSCRIPT
#!/bin/bash

## install Docker
amazon-linux-extras install docker
systemctl enable docker
systemctl start docker

## install GitLab Runner
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
yum install -y gitlab-runner

usermod -a -G docker gitlab-runner
usermod -a -G docker ec2-user

gitlab-runner register \
  --non-interactive \
  --url ${var.gitlab_runner_gitlab_url} \
  --registration-token ${var.gitlab_runner_registration_token} \
  --name epona-gitlab-runner \
  --tag-list epona,docker \
  --run-untagged \
  --executor docker \
  --docker-image docker:stable \
  --docker-pull-policy ${var.gitlab_runner_pull_image_always ? "always" : "if-not-present"} \
  --docker-privileged \
  --docker-volumes /certs/client

systemctl enable gitlab-runner
systemctl restart gitlab-runner

## Docker resources prune
echo '
[Unit]
Description=GitLab Runner Docker Executor cleanup task

[Service]
Type=simple
ExecStart=/usr/bin/docker system prune --force
User=gitlab-runner
' > /etc/systemd/system/gitlab-runner-docker-executor-cleanup.service

echo '
[Unit]
Description=GitLab Runner Docker Executor cleanup task timer

[Timer]
OnCalendar=*-*-* *:00:00
Unit=gitlab-runner-docker-executor-cleanup.service

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/gitlab-runner-docker-executor-cleanup.timer

systemctl enable gitlab-runner-docker-executor-cleanup.timer
systemctl start gitlab-runner-docker-executor-cleanup.timer
    SHELLSCRIPT
}

resource "aws_instance" "gitlab_runner" {
  ami                  = var.ami
  instance_type        = var.instance_type
  iam_instance_profile = var.iam_instance_profile

  subnet_id = var.subnet

  vpc_security_group_ids = var.security_groups

  user_data = var.init_script != null ? var.init_script : local.default_init_script

  tags = merge(
    {
      "Name" = var.name
    },
    var.tags
  )

  root_block_device {
    volume_size = var.root_block_volume_size
  }
}
