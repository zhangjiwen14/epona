variable "name" {
  description = "ロードバランサーの名前"
  type        = string
}

variable "tags" {
  description = "ロードバランサーに関連するリソースに、共通的に付与するタグ"
  type        = map(string)
}

variable "vpc_id" {
  description = "ターゲットグループを配置するVPCのIC"
  type        = string
}

variable "load_balancer_type" {
  description = "ロードバランサーのタイプ"
  type        = string
}

variable "access_logs_enabled" {
  description = "ロードバランサーのアクセスログを有効にする場合、trueを指定する"
  type        = bool
}

variable "access_logs_bucket" {
  description = "ロードバランサーのアクセスログを保存するS3バケット名"
  type        = string
}

variable "access_logs_prefix" {
  description = "アクセスログをS3バケットに保存する時のprefix。指定しない場合は、ログはS3のルートに保存される"
  type        = string
}

variable "subnets" {
  description = "ロードバランサーに割り当てるサブネットIDのリスト"
  type        = list(string)
}

variable "security_groups" {
  description = "ロードバランサーに割り当てるセキュリティグループのリスト"
  type        = list(string)
}

variable "public_traffic_protocol" {
  description = "ロードバランサーが受け付けるトラフィックのプロトコル"
  type        = string
}

variable "public_traffic_port" {
  description = "ロードバランサーが受け付けるトラフィックのポート"
  type        = number
}

variable "public_traffic_certificate_arn" {
  description = "ロードバランサーが使用する、SSL/TLS証明書のARN"
  type        = string
}

variable "public_traffic_ssl_policy" {
  description = "ロードバランサーのSSL/TLSネゴシエーションポリシー"
  type        = string
}

variable "public_traffic_test_protocol" {
  description = "テスト用リスナーがListenするプロトコル。テスト用リスナーを使用する場合は、public_traffic_test_portと同時に指定すること"
  type        = string
  default     = null
}

variable "public_traffic_test_port" {
  description = "テスト用リスナーがListenするポート。テスト用リスナーを使用する場合は、public_traffic_test_protocolと同時に指定すること"
  type        = number
  default     = null
}

variable "backend_protocol" {
  description = "ロードバランサーのバックエンドのプロトコル"
  type        = string
}

variable "backend_port" {
  description = "ロードバランサーのバックエンドのポート"
  type        = number
}

variable "backend_health_check_path" {
  description = "ロードバランサーのバックエンドに対する、ヘルスチェックパス"
  type        = string
}
