locals {
  target_group_names = toset(["blue", "green"])
}

resource "aws_lb" "this" {
  name                       = var.name
  load_balancer_type         = var.load_balancer_type
  internal                   = false
  idle_timeout               = 60
  enable_deletion_protection = false # TODO

  subnets = var.subnets

  access_logs {
    enabled = var.access_logs_enabled
    bucket  = var.access_logs_bucket
    prefix  = var.access_logs_prefix
  }

  security_groups = var.security_groups

  tags = merge(
    {
      "Name" = var.name
    },
    var.tags
  )
}

resource "aws_lb_listener" "prod" {
  load_balancer_arn = aws_lb.this.arn
  port              = var.public_traffic_port
  protocol          = var.public_traffic_protocol

  certificate_arn = upper(var.public_traffic_protocol) == "HTTPS" ? var.public_traffic_certificate_arn : null
  ssl_policy      = upper(var.public_traffic_protocol) == "HTTPS" ? var.public_traffic_ssl_policy : null

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this["green"].arn
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [default_action] # for CodeDeploy update
  }
}

resource "aws_lb_listener" "test" {
  count = (var.public_traffic_test_port != null && var.public_traffic_test_protocol != null) ? 1 : 0

  load_balancer_arn = aws_lb.this.arn
  port              = var.public_traffic_test_port
  protocol          = var.public_traffic_test_protocol

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this["blue"].arn
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [default_action] # for CodeDeploy update
  }
}

resource "aws_lb_target_group" "this" {
  for_each = local.target_group_names

  # Error deleting Target Group: ResourceInUse: Target group 'xxxx' is currently in use by a listener or a rule status code
  # エラーの回避
  # https://github.com/terraform-providers/terraform-provider-aws/issues/636#issuecomment-637761075
  name_prefix          = each.value
  target_type          = "ip"
  vpc_id               = var.vpc_id
  port                 = var.backend_port
  protocol             = var.backend_protocol
  deregistration_delay = 30

  health_check {
    path                = var.backend_health_check_path
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 60
    matcher             = 200
    port                = var.backend_port
    protocol            = var.backend_protocol
  }

  tags = merge(
    {
      "Name" = format("%s-target-group", each.value)
    },
    var.tags
  )

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [aws_lb.this]
}

resource "aws_lb_listener_rule" "this" {
  listener_arn = aws_lb_listener.prod.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this["green"].arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }

  lifecycle {
    ignore_changes = [action] # for CodeDeploy update
  }
}
