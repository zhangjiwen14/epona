output "keys" {
  value = { for alias in aws_kms_alias.this : alias.name =>
    {
      key_alias_name = alias.name,
      key_arn        = alias.target_key_arn,
      key_id         = alias.target_key_id,
    }
  }
}
