resource "aws_kms_key" "this" {
  count = length(var.kms_keys)

  description = lookup(var.kms_keys[count.index], "description", null)
  key_usage   = lookup(var.kms_keys[count.index], "key_usage", "ENCRYPT_DECRYPT")

  customer_master_key_spec = lookup(var.kms_keys[count.index], "customer_master_key_spec", "SYMMETRIC_DEFAULT")

  policy = lookup(var.kms_keys[count.index], "policy", null)

  deletion_window_in_days = lookup(var.kms_keys[count.index], "deletion_window_in_days", null)
  is_enabled              = lookup(var.kms_keys[count.index], "is_enabled", true)
  enable_key_rotation     = lookup(var.kms_keys[count.index], "enable_key_rotation", true) # not default

  tags = merge(
    {
      "Name" = substr(var.kms_keys[count.index].alias_name, 6, 0) # "alias/" length 6
    },
    var.tags
  )
}

resource "aws_kms_alias" "this" {
  count = length(aws_kms_key.this) > 0 ? length(var.kms_keys) : 0

  name          = var.kms_keys[count.index]["alias_name"]
  target_key_id = aws_kms_key.this[count.index].key_id
}
