from setuptools import setup

setup(
    name='exclude_plugin',
    packages=['exclude_plugin'],
    entry_points={
        'mkdocs.plugins': [
            'exclude_plugin = exclude_plugin.exclude:ExcludePlugin',
        ]
    },
)