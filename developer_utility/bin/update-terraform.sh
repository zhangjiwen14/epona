#!/bin/bash

## usage
# $./update-terraform.sh [target-version]

SCRIPT_DIR=$(cd $(dirname $0); pwd)
PROJECT_DIR=$(cd ${SCRIPT_DIR}; cd ../../; pwd)

VERSION=$1

echo ${VERSION} > ${PROJECT_DIR}/.terraform-version
echo "updated... ${PROJECT_DIR}/.terraform-version"

perl -wpi -e "s!^(  TERRAFORM_VERSION:).+!\$1 ${VERSION}!" ${PROJECT_DIR}/.gitlab-ci.yml
echo "updated... ${PROJECT_DIR}/.gitlab-ci.yml"

FIX_VERSIONS_DIRS="${PROJECT_DIR}/tests ${PROJECT_DIR}/guide/templates/new_envionment"

for FILE in `find ${FIX_VERSIONS_DIRS} -name 'versions.tf' | grep -v '/\.terraform/'`
do
    tfupdate terraform -v ${VERSION} ${FILE}
    echo "updated... ${FILE}"
done


LOWER_BOUND_VERSIONS_DIRS="${PROJECT_DIR}/modules"

for FILE in `find ${LOWER_BOUND_VERSIONS_DIRS} -name 'versions.tf' | grep -v '/\.terraform/'`
do
    tfupdate terraform -v "~> ${VERSION}" ${FILE}
    echo "updated... ${FILE}"
done


