# CONTRIBUTING

## HTMLドキュメントを生成する

本リポジトリ内のMarkdownファイルからHTMLのドキュメントを生成する方法について説明します。

ドキュメントの生成には以下のソフトウェアを使用するので、それぞれインストールしてください。

- [Python 3.8](https://www.python.jp/)
- [MkDocs 1.1.2](https://www.mkdocs.org/)
- [Material for MkDocs 6.1.6](https://squidfunk.github.io/mkdocs-material/)

もしくは、これらがインストール済みのDockerコンテナを使う方法もあります。
その場合は、Dockerをインストールしてください。

- [Docker 19.03](https://www.docker.com/)

Dockerのイメージは、[mkdocs-material:6.1.6](https://hub.docker.com/layers/squidfunk/mkdocs-material/6.1.6/images/sha256-38c4bedd088419083fd91ff99ac3ba9bdac39d3d82c49a8c51af96b22b0c9891?context=explore)をベースに必要なプラグインをインストールしたものを使用します。
以下のコマンドでDockerイメージをビルドしてください。

```bash
$ docker image build -t mkdocs-material-epona -f ci_container_images/mkdocs/Dockerfile ci_container_images/mkdocs
```

以降の手順は、ここで作成されたDockerイメージを使うことを前提としています。

なお、本手順は[Epona AWS Getting Started](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master)のドキュメント生成にも適用できます。

ただし、DockerイメージはGetting Startedに含まれているDockerfileを使って、別名で作る必要があります。
Getting StartedのドキュメントをDockerで作る場合、以降の手順はイメージ名の部分を適宜読み替えるようにしてください。

### プラグインのインストール

MkDocsをローカルにインストールして実行する場合は、最初に以下のプラグインをインストールする必要があります。

- [mdx_truly_sane_lists](https://github.com/radude/mdx_truly_sane_lists)
  - スペース2つでもリストの階層を下げられるようにするプラグイン
- `exclude_plugin`
  - MkDocsの処理対象から特定のディレクトリを除外するためのカスタムプラグイン

:information_source: Dockerを使用する場合は、本手順は不要です。

`mdx_truly_sane_lists`は、以下のコマンドでインストールしてください。

```bash
$ pip install mdx_truly_sane_lists
```

`exclude_plugin`は、以下のコマンドでインストールしてください。

```bash
$ cd ci_container_images/mkdocs/plugins

$ python setup.py develop
```

`exclude_plugin`をアンインストールする場合は、以下のコマンドを実行してください。

```bash
$ cd ci_container_images/mkdocs/plugins

$ python setup.py develop -u
```

### ローカルでプレビューする

MkDocsをインストールした場合は、以下のコマンドを実行してください。

```bash
$ mkdocs serve -f guide/mkdocs.yml
```

Dockerを使う場合は、以下のコマンドを実行してください。

```bash
$ docker run -it --rm -p 8000:8000 -v $(pwd):/docs mkdocs-material-epona serve -f guide/mkdocs.yml -a 0.0.0.0:8000
```

HTTPサーバーが起動するので、ブラウザで[http://localhost:8000](http://localhost:8000)にアクセスしてください。
出力されたドキュメントを確認できます。

Markdownファイルを修正すると変更がリアルタイムで反映されるので、サーバーを止めることなくドキュメントの編集ができます。

サーバーの停止は、`Ctrl + C`です。

### ビルドする

MkDocsをインストールしている場合は、以下のコマンドを実行してください。

```bash
$ mkdocs build -f guide/mkdocs.yml
```

Dockerを使う場合は、まず先に出力先のディレクトリを作成してください。

```bash
$ mkdir ../site
```

つづいて、以下のコマンドを実行してください。

```bash
$ docker run --rm -v $(pwd):/docs -v $(pwd)/../site:/site mkdocs-material-epona build -f guide/mkdocs.yml
```

`../site`ディレクトリの下に、HTMLファイルが出力されます。

### Material for MkDocsのバージョンについての注意点

Material for MkDocsは、[HTMLドキュメントを生成する](#htmlドキュメントを生成する)で指定されているバージョン以外のものを使うと、正常に出力できない可能性があります。
レイアウトのテンプレート(`guide/overrides/base.html`)では、名前にハッシュ値を含むファイル(`main.<ハッシュ>.min.css`など)を参照しています。
このハッシュ値はバージョンが変わると別の値になる可能性があるため、指定されたバージョン以外を使用するとレイアウトが崩れる原因となります。

Material for MkDocsには、あらかじめレイアウトのテンプレート内で定義された部分に、任意のHTMLを埋め込む機能
([Overriding partials](https://squidfunk.github.io/mkdocs-material/customization/#overriding-partials))が用意されています。
しかし、標準で用意されているpartialsは、いずれもライセンスや商標の文言を埋め込む場所として適当ではありませんでした。

このため、`base.html`を組み込んで任意の場所にライセンスと商標の文言を埋め込めるようにしています。
