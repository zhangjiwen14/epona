# public_traffic_container_service pattern

## 概要

`public_traffic_container_service pattern`モジュールでは、インターネットからのトラフィックを受ける想定のロードバランサーおよびコンテナサービス環境を構築します。また、サービスを公開するにあたってのSSL/TLS証明書やDNSレコードの設定も行います。

このpatternにより構築されるコンテナサービスでは、バックエンドのサーバーアプリケーションがコンテナとして動作します。このアプリケーションは、ロードバランサーを介してインターネットに公開され、利用者に対してサービスや機能を提供します。

## 想定する適用対象環境

`public_traffic_container_service pattern`は、Runtime環境での使用を想定しています。

## 依存するpattern

`public_traffic_container_service pattern`は、事前に以下のpatternが適用されていることを前提としています。

| pattern名                       | 利用する情報                                 |
| :------------------------------ | :------------------------------------------- |
| [network pattern](./network.md) | パブリックサブネット、プライベートサブネット |

またコンテナサービスで使用するコンテナイメージおよび動作するコンテナが、次のようなリソース、情報を必要とする場合は依存するpatternが追加されます。

* Amazon ECRに格納されたDockerイメージを使用する
* コンテナ内からデータストアを使用する
* 環境変数（秘匿情報）を参照する

| pattern名                                       | 利用する情報                                             |
| :---------------------------------------------- | :------------------------------------------------------- |
| [ci_pipeline pattern](./ci_pipeline.md)         | Delivery環境に構築されたAmazon ECR上のDockerイメージ     |
| [parameter_store pattern](./parameter_store.md) | AWS Systems Manager パラメータストアに格納された秘匿情報 |
| [database pattern](./database.md)               | RDBMS                                                    |
| [redis pattern](./redis.md)                     | Redis                                                    |

本patternが依存するリソースを他の構築手段で代替する場合は、依存するpatternと[入出力リファレンス](#入出力リファレンス)の内容を参考に、本patternが必要とするリソースを構築してください。

## 構築されるリソース

このpatternでは、以下のリソースが構築されます。

| リソース名                                                                                                         | 説明                                                                 |
| :----------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------- |
| [Elastic Load Balancing（Application Load Balancer）](https://aws.amazon.com/jp/elasticloadbalancing/)             | インターネットからのトラフィックを受けるロードバランサーを構築します |
| [AWS Fargate](https://aws.amazon.com/jp/fargate/)                                                                  | アプリケーションコンテナを動作させるサービスを構築します             |
| [AWS Certificate Manager](https://aws.amazon.com/jp/certificate-manager/)                                          | SSL/TLS証明書を作成します                                            |
| [Amazon Route 53](https://aws.amazon.com/jp/route53/)                                                              | DNSレコードを作成します                                              |
| [Amazon S3](https://aws.amazon.com/jp/s3/) | ALBのアクセスログ格納先                                                 |
 [Amazon CloudWatch Logs](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html) | コンテナログの格納先                                                 |

:warning: `public_traffic_container_service pattern`では、ドメインの取得は行いません。事前にドメインの取得が必要です。

## モジュールの理解に向けて

一般にサーバーアプリケーションをインターネットに公開する際には、以下のような環境を構築する必要があります。

* 負荷分散や可用性を考慮した、ロードバランサーや複数インスタンスで動作するサーバー環境
* インターネット公開に必要な、DNSレコードやSSL/TLS証明書の設定

`public_traffic_container_service pattern`では、これらを実現するリソースを構築します。

![public_traffic_container_serviceの全体像](./../../resources/public_traffic_container_service.png)

Eponaでは、アプリケーションがコンテナ化されていることを前提としています。  
`public_traffic_container_service pattern`を適用することにより、以下の環境が得られます。

* コンテナの稼働数を柔軟に増減（スケールイン・アウト）できる、AWS Fargateクラスター
  * コンテナを稼働させるインスタンスは、利用者は管理不要（AWS管理となる）
* インターネットからのトラフィックを受け付け、AWS Fargateクラスターへ転送するApplication Load Balancer

:information_source: Application Load Balancerは、AWS CodeDeployと連携することを想定しています。  
:information_source: 具体的には、Blue/Greenデプロイが行われることを想定し、ターゲットグループが2つ作成されます。  
:information_source: 詳しくは、[`cd_pipeline_backend pattern`](./cd_pipeline_backend.md)も参照してください。

また、今日のインターネットに公開されるWebサービス、Webサイトには固有のドメイン名でアクセスできることは当然として、HTTPSでのアクセスが求められるようになっている状況です。

このため、インターネット公開を前提として以下の環境を構成します。

* Amazon Route 53を使用した、サービス公開用のDNSレコードの取得
* サービス公開用のドメイン名に合わせた、AWS Certificate ManagerによるSSL/TLS証明書の取得

ここで作成するDNSレコードおよびSSL/TLS証明書は、同時に作成されるApplication Load Balancerへ紐付けられます。

### ドメイン取得について

`public_traffic_container_service pattern`では、ドメインの登録自体は範囲外となっています。

事前に、AWSのマネジメントコンソールで必要なドメインを取得してください。

[Amazon Route 53 を使用したドメイン名の登録](https://docs.aws.amazon.com/ja_jp/Route53/latest/DeveloperGuide/registrar.html)

ドメイン取得が`public_traffic_container_service pattern`の範囲外になっているのは、Terraformが対応していないことがひとつの理由です。加えて、ドメイン取得に関してはメール確認といった人手による作業が必要になるため、完全な自動化はできません。このため、Eponaの範囲外でドメインを取得する前提としています。

`public_traffic_container_service pattern`は、事前のドメイン取得およびドメイン取得に伴うホストゾーンの作成が完了した後に適用されることを想定しています。

:information_source: ホストゾーンの作成は、Amazon Route 53でドメインを取得した際に、自動的に作成されます。

`public_traffic_container_service pattern`では、作成済みのホストゾーンに対して、DNSレコードを作成します。

### ログの保存について

AWS Fargateのようなコンテナを管理するプラットフォーム上で動作するアプリケーションは、ログを標準出力として書き出すことで記録します。

ただ、AWS Fargateはコンテナインスタンスを実行している仮想マシンへは直接アクセスできないため、デフォルトではコンテナのログを確認できません。

このため、実際の利用にはログをクラスター外に保存することが必須となるでしょう。

`public_traffic_container_service pattern`では、ログはAmazon CloudWatch Logsへの保存を想定しています。

ロググループは`container_log_group_names`に、ロググループ名を指定して作成します。タスク内のコンテナ数分、ロググループを作成しましょう。

```terraform
  container_log_group_names             = ["ロググループ名1", "ロググループ名2", "ロググループ名3", ...]
```  

ここで指定したロググループ名は、タスクの`logConfiguration`に設定してコンテナのログを保存していきます。

Amazon CloudWatch Logsへのログ出力は、AWS FireLensを利用して行うことをおすすめします。詳しくは、以下のドキュメントを参照してください。

[Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する](../../how_to/aws/ecs_log_send_to_cloudwatch_logs_using_firelens.md)

さらにDatadogが利用可能な場合は、ログを集約できます。詳しくは[こちら](#ログの集約)を参照してください。

### タスク定義で使用するコンテナについて

本patternで設定するタスク定義のコンテナには、実際のアプリケーションのものではなく、ダミーのものを設定するようにしてください。

本patternで構築されるFargate環境の更新は、AWS CodeDeployから行う前提となっています。
つまり、本patternに実際のアプリケーションのタスク定義を書いた場合、AWS CodeDeploy用のタスク定義と二重管理になってしまいます。
また、本patternは環境を構築するための最初の1回のみしか適用しないため、タスク定義の管理にコストをかけることは好ましくありません。

したがって、本patternでデプロイする初回のコンテナは、ダミーのコンテナを使用することを推奨します。

具体的なダミーのコンテナとしては、HashiCorp社が提供している[http-echo](https://hub.docker.com/r/hashicorp/http-echo/)などがあります。
次節のサンプルコードでは、`http-echo`をコンテナイメージに設定した例を記載しています。

なお、本pattern適用時にFargateへデプロイされるコンテナイメージこそダミーですが、コンテナ定義以外のリソースはすべて本来利用すべきものを構築します。
したがって、DNSレコードやクラスター名、ロググループの定義等は実際の利用で必要なものを作成してコンテナのみを差し替えます。

たとえば、ロググループを例にして説明します。
`cd_pipeline_backend pattern`によりデプロイするコンテナが以下の３つだとします。

* アプリケーションコンテナ
* Fluent Bitコンテナ
* Datadog Agentコンテナ

この場合、`public_traffic_container_service pattern`適用時にデプロイするコンテナと作成するロググループは、次のようになります。

* デプロイするコンテナ
  * `http-echo`のみ
* 作成するロググループ
  * アプリケーションコンテナ用ロググループ
  * Fluent Bitコンテナ用ロググループ
  * Datadog Agentコンテナ用ロググループ

## サンプルコード

`public_traffic_container_service pattern`を使用したサンプルコードを、以下に記載します。

```terraform
module "public_traffic_container_service" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/public_traffic_container_service?ref=v0.2.2"

  name = "[リソースに共通的に付与する名前]"

  vpc_id                             = data.terraform_remote_state.production_network.outputs.network.vpc_id
  public_subnets                     = data.terraform_remote_state.production_network.outputs.network.public_subnets
  public_traffic_protocol            = "HTTPS"
  public_traffic_port                = 443
  public_traffic_inbound_cidr_blocks = ["0.0.0.0/0"]

  dns = {
    zone_name   = "example.com"
    record_name = "chat-example-backend.example.com"
  }

  container_subnets  = data.terraform_remote_state.production_network.outputs.network.private_subnets
  container_protocol = "HTTP"
  container_port     = 8080

  container_health_check_path           = "/"
  container_cluster_name                = "chat-example"
  container_traffic_inbound_cidr_blocks = ["AWS Fargateを配置するVPCのCIDRブロック"]
  container_service_desired_count       = 3
  container_service_platform_version    = "1.4.0"
  container_task_cpu                    = 512
  container_task_memory                 = "1024"


  default_ecs_task_iam_role_name             = "ChatExampleContainerServiceTaskRole"
  default_ecs_task_iam_policy_name           = "ChatExampleContainerServiceTaskRolePolicy"
  default_ecs_task_execution_iam_policy_name = "ChatExampleContainerServiceTaskExecution"
  default_ecs_task_execution_iam_role_name   = "ChatExampleContainerServiceTaskExecutionRole"

  container_log_group_names             = ["fargate-log-group/chat-example"]

  container_definitions = <<-JSON
  [
    {
      "name": "[タスク名]",
      "image": "hashicorp/http-echo:0.2.3",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": 8080
        }
      ],
      "logConfiguration": {
        # ログ設定
      },
      "command": [
          "-listen",
          ":8080",
          "-text",
          "echo"
      ]
    }
  ]
  JSON

  tags = {
    # 任意のタグ
    Environment        = "runtime"
    RuntimeEnvironment = "production"
    ManagedBy          = "epona"
  }
}
```

## 関連するpattern

`public_traffic_container_service pattern`に関連するpatternを、以下に記載します。

| pattern名                                                 | 説明                                                                                                 |
| :-------------------------------------------------------- | :--------------------------------------------------------------------------------------------------- |
| [`ci_pipeline pattern`](./ci_pipeline.md)         | アプリケーションをコンテナイメージとしてビルドし、Amazon ECRへPushできるようになります               |
| [`cd_pipeline_backend_trigger pattern`](./cd_pipeline_backend_trigger.md) | Amazon ECRへのコンテナイメージPushをトリガーに、デプロイメントパイプラインを起動できるようになります |
| [`cd_pipeline_backend pattern`](./cd_pipeline_backend.md)                 | デプロイメントパイプラインを構築して、AWS FaragateクラスターへのBlue/Greenデプロイが可能になります   |

## ログの集約

`public_traffic_container_service pattern`では、ALBのログをAmazon S3に出力します。  
また、コンテナのログはAmazon CloudWatch Logsに出力します。

Amazon CloudWatch Logsへのログ出力は、AWS FireLensを利用して行うことをおすすめします。

[Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する](../../how_to/aws/ecs_log_send_to_cloudwatch_logs_using_firelens.md)

また、Datadogが使用できる場合は[datadog_log_trigger pattern](datadog_log_trigger.md)を使ったログ集約を行いましょう。

## システムメトリクスの収集

`public_traffic_container_service pattern`では、コンテナのシステムメトリクス（CPU使用率やメモリ使用量など）を収集する仕組みは提供されません。

下記ガイドで、コンテナのメトリクスをDatadogへ送信する方法を解説しているので、そちらを参照してください。

* [ECS Fargate上のコンテナのメトリクスをDatadogへ送信する](../../how_to/aws/send_ecs_fargate_metrics_to_datadog.md)

## 入出力リファレンス

## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 0.13.5 |
| aws | >= 3.18.0, < 4.0.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| container\_cluster\_name | コンテナサービスのクラスター名 | `string` | n/a | yes |
| container\_definitions | コンテナサービスのタスクで実行する、コンテナ定義のリスト | `string` | n/a | yes |
| container\_health\_check\_path | ロードバランサーの、コンテナに対するヘルスチェックのパス | `string` | n/a | yes |
| container\_port | ロードバランサーがコンテナに転送する際のポート | `number` | n/a | yes |
| container\_protocol | ロードバランサーがコンテナに転送する際のプロトコル | `string` | n/a | yes |
| container\_service\_desired\_count | コンテナサービス内の、タスクのインスタンス数 | `number` | n/a | yes |
| container\_service\_platform\_version | コンテナサービスを実行するプラットフォームのバージョン | `string` | n/a | yes |
| container\_subnets | コンテナサービスを配置する、サブネットIDのリスト | `list(string)` | n/a | yes |
| container\_task\_cpu | コンテナサービス内で実行されるタスクに割り当てるCPU | `number` | n/a | yes |
| container\_task\_memory | コンテナサービス内で実行されるタスクに割り当てるメモリ | `string` | n/a | yes |
| container\_traffic\_inbound\_cidr\_blocks | コンテナサービスが受け付けるトラフィックの、CIDRブロックのリスト | `list(string)` | n/a | yes |
| dns | ロードバランサーのDNSに関する設定（zone\_name, record\_name）。<br>zone\_name = record\_nameを登録するホストゾーン名<br>record\_name = Amazon Route 53に登録するレコードセットの名前 | `map(any)` | n/a | yes |
| name | コンテナサービス、ロードバランサーに関する名前 | `string` | n/a | yes |
| public\_subnets | ロードバランサーに割り当てる、パブリックサブネットのIDのリスト | `list(string)` | n/a | yes |
| public\_traffic\_inbound\_cidr\_blocks | ロードバランサーが受け付けを許可するトラフィックのCIDRブロックのリスト | `list(string)` | n/a | yes |
| public\_traffic\_port | ロードバランサーが受け付けるトラフィックのポート | `number` | n/a | yes |
| public\_traffic\_protocol | ロードバランサーが受け付けるトラフィックのプロトコル | `string` | n/a | yes |
| vpc\_id | VPC ID | `string` | n/a | yes |
| container\_log\_group\_kms\_key\_id | CloudWatch Logsを暗号化するためのKMS CMKのARN | `string` | `null` | no |
| container\_log\_group\_names | コンテナのログ出力先と成るCloudWatch Logsのグループ名のリスト | `list(string)` | `[]` | no |
| container\_log\_group\_retention\_in\_days | CloudWatch Logsに保存した、コンテナログの保持日数 | `number` | `null` | no |
| container\_task\_execution\_role\_arn | コンテナサービスのタスクに割り当てるタスク実行IAMロールのARN。create\_default\_ecs\_task\_execution\_roleをfalseにする場合に指定すること | `string` | `null` | no |
| container\_task\_role\_arn | コンテナサービスのタスクが、他のAWSサービスを呼び出せるように割り当てるIAMロールのARN | `string` | `null` | no |
| container\_traffic\_inbound\_protocol | コンテナサービスが受け付けるトラフィックのプロトコル | `string` | `"tcp"` | no |
| create\_default\_ecs\_task\_execution\_role | デフォルトのタスク実行ロールを作成する場合、trueを指定する | `bool` | `true` | no |
| create\_default\_ecs\_task\_role | デフォルトのタスク実行ロールを作成する場合、trueを指定する | `bool` | `true` | no |
| create\_public\_traffic\_certificate | ロードバランサーで利用するSSL/TLS証明書を作成する場合、true | `bool` | `true` | no |
| default\_ecs\_task\_execution\_iam\_policy\_name | デフォルトのタスク実行ロールを作成する場合のIAMポリシー名 | `string` | `"DefaultContainerServiceTaskExecutionRolePolicy"` | no |
| default\_ecs\_task\_execution\_iam\_role\_name | デフォルトのタスク実行ロールを作成する場合のIAMロール名 | `string` | `"DefaultContainerServiceTaskExecutionRole"` | no |
| default\_ecs\_task\_iam\_policy\_name | デフォルトのタスクロールを作成する場合のIAMポリシー名 | `string` | `"DefaultContainerServiceTaskRolePolicy"` | no |
| default\_ecs\_task\_iam\_role\_name | デフォルトのタスクロールを作成する場合のIAMロール名 | `string` | `"DefaultContainerServiceTaskRole"` | no |
| public\_traffic\_access\_logs\_bucket | ロードバランサーのアクセスログを保存するS3バケット名。public\_traffic\_access\_logs\_enabledがtrueで、この変数を指定しない場合、nameから自動導出する | `string` | `null` | no |
| public\_traffic\_access\_logs\_create\_bucket | bucketに指定したS3バケットをこのモジュールで作成する場合、trueを指定する。デフォルトでtrueが指定されたものとして振る舞う | `bool` | `true` | no |
| public\_traffic\_access\_logs\_enabled | ロードバランサーのアクセスログを有効にする場合、trueを指定する。デフォルトでtrueが指定されたものとして振る舞う | `bool` | `true` | no |
| public\_traffic\_access\_logs\_force\_destroy | destroy時、データがあったとしても強制的にアクセスログ用S3バケットを削除する | `bool` | `false` | no |
| public\_traffic\_access\_logs\_prefix | アクセスログをS3バケットに保存する時のprefix。指定しない場合は、ログはS3のルートに保存される | `string` | `null` | no |
| public\_traffic\_certificate\_arn | ロードバランサーに与えるSSL/TLS証明書のARN。このpattern外部で証明書を作成する場合に使用する | `string` | `null` | no |
| public\_traffic\_inbound\_protocol | ロードバランサーが受け付けるトラフィックのプロトコル。セキュリティグループの指定に利用 | `string` | `"tcp"` | no |
| public\_traffic\_ssl\_policy | ロードバランサーのデフォルトのセキュリティポリシー | `string` | `"ELBSecurityPolicy-TLS-1-2-Ext-2018-06"` | no |
| tags | このモジュールで作成するリソースに、共通的に付与するタグ | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| cloudwatch\_container\_log\_names | コンテナのログ出力先となる、CloudWatch Logsのロググループ名のリスト |
| ecs\_cluster\_name | ECSクラスター名 |
| ecs\_service\_name | ECSサービス名 |
| load\_balancer\_access\_logs\_bucket | ロードバランサーのアクセスログ出力用のS3バケット名 |
| load\_balancer\_access\_logs\_bucket\_id | ロードバランサーのアクセスログ出力用のS3バケットのID |
| load\_balancer\_prod\_listener\_arn | 本番環境用トラフィックを受け持つ、ロードバランサのListenerのARN |
| load\_balancer\_target\_group\_names | LoadBalancerのターゲットグループ名のリスト |
