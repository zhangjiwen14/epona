# インフラセキュリティ

サービスを支えるインフラを安定的に稼働させるため、セキュリティ対策が重要なのは言うまでもありません。近年ではサイバー攻撃の多様化に伴い、単なるファイアウォールなどによる入口の対策のみでなく、内部リソースの状況を常に監視し不審なアクティビティを早期に検知するといった内部対策の重要性が増してきています。

[AWS Well-Architectedフレームワーク](https://wa.aws.amazon.com/wat.pillar.security.ja.html)
においても、単なる侵入防止だけでなく、権限管理やトレーサビリティなど内部ネットワークのリソースを不正に操作されないような対策が挙げられています。

Eponaでは、内部対策を含めたセキュリティを念頭に置きつつ、[Operation-Less](../../README.md#Operation-Less)の見地からAWSのマネージドサービスを活用したセキュアな環境の構築をサポートしています。

## 適用pattern・How To

本アクティビティを実現するためにEponaが用意しているpatternは以下の通りです。

- [`users pattern`](../patterns/aws/users.md)
- [`roles pattern`](../patterns/aws/roles.md)
- [`bind_role pattern`](../patterns/aws/bind_role.md)
- [`audit pattern`](../patterns/aws/audit.md)
- [`rule_violation pattern`](../patterns/aws/rule_violation.md)
- [`parameter_store pattern`](../patterns/aws/parameter_store.md)
- [`webacl pattern`](../patterns/aws/webacl.md)
- [`threat_detection pattern`](../patterns/aws/threat_detection.md)

## 権限を管理できる

外部からの侵入者による不正な操作のみならず、開発者の誤操作によるインフラ障害を避けるためにも、権限管理は重要です。
しかし、「誰に」「どんな」操作を許容するかをゼロから定義していく作業は複雑で手間がかかります。  
Eponaでは、事前に権限をまとめたロールを定義し、このロールをユーザーに付与する形で権限を管理します。なお、もしデフォルトで用意されたロールに権限の不足があれば、権限を個別に追加もできます。

また、Runtime環境（サービスが稼働する環境）には極力ユーザーを作成せず、ロールの付け替えのみで操作可能となるよう構成しています。
これにより、1つのID/PWで複数のマネジメントコンソールへログインでき、ID管理の手間が削減できます。

Eponaで作成するユーザーやロールの詳細については以下のガイドをあわせて参照してください。

- [users pattern](../patterns/aws/users.md)
- [roles pattern](../patterns/aws/roles.md)
- [bind_role pattern](../patterns/aws/bind_role.md)

## 誰が何を行ったのかを表す監査証跡を保存できる

攻撃の検知やその後の影響範囲の特定のためには、「いつ」「誰が」「何を」行ったのかを証跡として記録しておくことが重要です。
Eponaでは[AWS CloudTrail](https://aws.amazon.com/jp/cloudtrail/)を活用し、AWS上のリソースに対する操作を自動的に記録します。

詳細は[audit pattern](../patterns/aws/audit.md)のガイドを参照してください。

## 監査証跡の改ざんを防止できる

監査証跡を残していたとしても、侵入者により証跡自体が削除・改ざんされてしまっては攻撃の検知の遅れにつながり、証跡を残している効果が十分発揮できなくなります。  
Eponaでは監査証跡をS3に保存し、[バケットポリシー](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/using-iam-policies.html)により書き込み権限を制限するとともに、
[CloudTrail ログファイルの整合性の検証](https://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/cloudtrail-log-file-validation-intro.html)により改ざんの有無を検証可能となっています。

詳細は[audit pattern](../patterns/aws/audit.md)のガイドを参照してください。

## 構成変更のルール違反を検知できる

継続的にサービス開発を行いAWS環境の構成変更が続いていくと、誤設定によるセキュリティリスクや予期しない構成変更が紛れ込むこともあります。
しかし、多種多様なAWS環境のリソースの状態を人手で管理・監視し続けることは困難です。  
Eponaでは[AWS Config](https://aws.amazon.com/jp/config/)をサポートし、構成変更を自動的に監視するとともに、ルールに違反する変更があった場合は利用者に通知する仕組みを提供しています。

詳細は[rule_violation pattern](../patterns/aws/rule_violation.md)のガイドを参照してください。

## 秘匿すべき情報を開発者から隠すことができる

サービス開発においては、データベースへの接続情報や外部SaaSのアクセスキーなど、みだりに他人に知られてはならない設定情報を扱うことがしばしばあります。これらの情報をどこに保存しどのように共有するか、開発者の利便性とセキュリティをいかに両立するかは、開発環境の構成上懸念となるところです。

EponaではAWS Systems Managerの[パラメータストア](https://docs.aws.amazon.com/ja_jp/systems-manager/latest/userguide/systems-manager-parameter-store.html)の内容を暗号化し、限られたユーザーにしか復号する権限を与えないことで、設定情報を開発者から隠すことができます。

詳細は[parameter_store pattern](../patterns/aws/parameter_store.md)のガイドを参照してください。

## 第三者による不正アクセスを防止できる

一般に、ネットワークへのアクセス制御では[セキュリティグループ](https://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/ec2-security-groups.html)により、想定しないポートへの不正なアクセスをブロックします。
しかし、想定したポートへのアクセスであってもSQLインジェクションなど不正な内容のアクセスが混在することはありますが、これはセキュリティグループのみでは検知できません。

Eponaでは[AWS WAF](https://aws.amazon.com/jp/waf/)により、特別なアプライアンスやソフトウェアを用意することなく、外部からの一般的な攻撃パターンをブロックする仕組みを構築できます。

詳細は[webacl pattern](../patterns/aws/webacl.md)のガイドを参照してください。

また、[Amazon GuardDuty](https://aws.amazon.com/jp/guardduty/)により、内部ネットワークで発生しているマルウェアによる攻撃、不審なアクティビティなどを検知する仕組みを提供しています。検知した結果はチャットツールに通知可能です。

こちらについては[threat_detection pattern](../patterns/aws/threat_detection.md)のガイドを参照してください。

## コンテナに対してセキュリティスキャンをかけられる

Eponaでは可搬性やスケーリングの観点から[コンテナの利用](../README.md#コンテナの利用)を想定しています。コンテナの利用には多くの利点がありますが、一方でコンテナ内に脆弱性のあるバージョンのOSSやイメージを含めてしまったり、新しく脆弱性が見つかったりといった状況への対処は必要となります。

この点、AWSでは[Amazon ECR イメージスキャン](https://docs.aws.amazon.com/ja_jp/AmazonECR/latest/userguide/image-scanning.html)というマネージドサービスが提供されており、こちらを利用することでコンテナ内の脆弱性を検知できます。
