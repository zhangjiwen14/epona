# コミュニケーション

DevOpsにおいては、チーム全員が共通の目標に向かって高速に改善を繰り返していきます。
その中では、頻繁に変化する環境下でチーム全員が常に情報を把握し、同じ認識の元で議論を行えるようにしなければなりません。
コミュニケーションの重要性はDevOpsに限ったものではありませんが、DevOpsにおけるコミュニケーションの重要性はより高まっているといって良いでしょう。

そのようなコミュニケーションを成立させるためには「積極的に情報を発信・共有する文化」を育てるとともに、
ツールの支援が必要です。
Eponaではコミュニケーションハブとしてのチャットツールに情報を一元化することで、
そのようなコミュニケーションを支援します。

## 適用pattern・How To

本アクティビティを実現するためにEponaが用意しているpatternは以下の通りです。

- [`rule_violation`](../patterns/aws/rule_violation.md)

また、本アクティビティを支えるHow Toドキュメントは以下の通りです。

- [Datadogのアラート設定](../how_to/datadog_monitor.md)
- [PagerDutyでのインシデント管理](../how_to/pagerduty_guide.md)

## チームメンバーがチャットでコミュニケーションできる

チャットを使うと、情報をチームで素早く共有できます。また、オープンなコミュニケーションが促進されます。

もちろん、共有の用途でメールを用いていたチームもあるでしょう。
しかし電子メールは基本的に非公開なコミュニケーションであり、簡単には見つけられないとともに
組織全体にも行き渡りません。

チャットを使うと、チームメンバーがシステムやサービスについての理解を深め共有できます。
オープンなコミュニケーションの中で、問題が発生したときは互いに助け合えるようになります。
このようなメカニズムを作ると、組織的な学習が加速していき、
必要な情報を得る時間や作業を進めるまでにかかる時間が飛躍的に向上します。

Eponaでは必要な情報をこれらチャットツールに集約し、コミュニケーションハブとして活用することで、
円滑なコミュニケーションを強力に支援します。

Eponaでサポートするチャットツールは以下の2つです。

- [Microsoft Teams](https://www.microsoft.com/ja-jp/microsoft-365/microsoft-teams/group-chat-software)
- [Slack](https://slack.com/intl/ja-jp/)

## コミュニケーションの過程・結果を残せる

チャットルームを使えば、その議論や情報は全て記録されます。
作業中に気づいたこと、チーム内の議論、問題解決の一部始終がチャット上に記録され、共有されるようになります。これは業務のあらゆる面を透明化し、コラボレーション、ひいてはチーム力の強化に繋がります。
インシデント対応の過程を残せば[ポストモーテム](https://landing.google.com/sre/sre-book/chapters/postmortem-culture/)のための対応履歴にもなります。

このため、あらゆる情報をチャットツールに集約しましょう。

なお、[Slackで無料プランを利用する場合は参照・検索できるメッセージ数には制限がある](https://slack.com/intl/ja-jp/pricing)点にご注意ください。
Microsoft Teamsの場合は[アイテム保持ポリシーに依存](https://docs.microsoft.com/ja-jp/microsoftteams/retention-policies#:~:text=Data%20is%20retained%20for%20compliance,policy%20indicates%20whether%20to%20do)します。
どのような保持ポリシーになっているかはMicrosoft Teamsの管理者にお問い合わせください。

## 課題起票やMerge Requestの通知を受け取れる

サービスとしてどのような課題があり、それらに対してどのような改善が図られているのかをチーム内で共有することは極めて重要です。
それらの状況が変化するごとに、コミュニケーションハブであるチャットツールに通知があれば、
チーム内での共有は容易になります。

Eponaでは[ITS、BTS](./its_bts.md)として[GitLab](https://gitlab.com/)の利用を想定しています。
GitLabは[様々なツールやサービスとのインテグレーション](https://docs.gitlab.com/ee/user/project/integrations/)を用意しており、
その中にはSlackやMicrosoft Teamsも含まれます。
これらインテグレーションを利用することで、課題の起票やMerge Requestの作成やマージといったイベントを、
チャットツールで共有できるようになります。

- [Microsoft Teams service](https://docs.gitlab.com/ee/user/project/integrations/microsoft_teams.html)
- [GitLab Slack application](https://docs.gitlab.com/ee/user/project/integrations/gitlab_slack_application.html)
- [Webhooks](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)

## デプロイメントパイプラインの成否を確認できる

:construction: 今後提供予定です。

## 障害通知を含むアラートを受け取れる

サービスチームの仕事は開発してリリースするだけではありません。
インシデント発生時にその問題の原因を作ったエンジニアに問題が見えなくなっていると、
何度も同じ問題が繰り返され運用を苦しめることになります。
それはバリューストリーム全体の効率を下げていることに他なりません。
このようなことが起こらないよう、どのようなアラート、インシデントが発生しているのかをチーム内で共有しましょう。

そうすることで、上流のアーキテクチャやコードに対する下流からのフィードバックを全員で受けることができ、品質に関する責任を引き受けられるようになります。
新機能の開発(Dev)と本番環境の問題解決(Ops)との間の適切なバランスも見つけられるようになるでしょう。

EponaではアラートやインシデントをDatadogやPagerDutyにて管理しますが、
これらSaaSの機能を用いるとアラート・インシデント情報をチャットツールに送信できます。

- [Datadogのアラート設定](../how_to/datadog_monitor.md)
- [PagerDutyでのインシデント管理](../how_to/pagerduty_guide.md#インシデントの通知)

また、環境に対してルールに反する構成変更を検出した場合は、`rule_violation pattern`を使用してチャットツールへ通知できます。
詳しくは、[インフラセキュリティ](./infra_security.md)も参照してください。
