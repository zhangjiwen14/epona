# 仮想セキュアルーム

DevOpsにおいてもセキュリティの確保は重要です。
とくに個人情報を扱うサービスや顧客にサービス提供しているサービスは本番環境へのアクセスは限定すべきです。

この限定的なアクセスを実現するためには、物理的なセキュアルームを設け、そこにある端末からのみ本番環境へアクセスできるようにします。
さらに、セキュアルームへは許可された限られた人間のみが入退室をできるようにすることで、より本番環境へのアクセスを限定できます。

しなしながら、昨今の情勢や場所に縛られる物理的なセキュアルームではDevOpsのスピードが損なわせる恐れもあります。
そこでEponaでは、場所に縛られない安全な本番環境へのアクセス手段として、仮想セキュアルームを提供します。

## 適用pattern

本アクティビティを実現するためにEponaが用意しているpatternは以下の通りです。

- [`virtual_secure_room_directory_service`](../patterns/aws/virtual_secure_room_directory_service.md)
- [`virtual_secure_room_workspaces`](../patterns/aws/virtual_secure_room_workspaces.md)
- [`virtual_secure_room_proxy`](../patterns/aws/virtual_secure_room_proxy.md)

また、仮想セキュアルームと本番環境で動作する実サービスはネットワーク的に隔離することが望ましいです。
そのため、それぞれ異なるVPCを用意することをEponaでは推奨します。
異なるVPC間でAWS内に閉じた通信をするには以下patternを使用します。

- [`vpc_peering`](../patterns/aws/vpc_peering.md)

## 限られたユーザー・場所のみ仮想セキュアルームへアクセスできる

仮想セキュアルームのセキュリティを確保する上で、仮想セキュアルームへのアクセス制限の対策は必須です。
Eponaでは仮想セキュアルームへのアクセスを特定のユーザーおよび場所（ソースIPアドレス）に限定することでこの対策をします。

仮想セキュアルームの端末はAmazon WorkSpaces（以下WorkSpaces）で構成します。
WorkSpacesへログインするユーザーはAWS Directory Service（以下Directory Service）で管理します。
任意のユーザーをDirectory Serviceに登録します。
WorkSpacesへは登録したユーザーの認証情報（ユーザー名・パスワード）を使用してアクセスします。
また、WorkSpacesのIPアクセスコントロールグループを設定することでWorkSpacesへアクセスできるIPアドレスを制限できます。
Directory Serviceへのユーザー登録方法は
[こちら](../patterns/aws/virtual_secure_room_directory_service.md)のガイドを確認ください。
WorkSpacesのIPアクセスコントロールグループの設定は
[こちら](../patterns/aws/virtual_secure_room_workspaces.md)のガイドを確認ください。

Directory Serviceへのアクセスもユーザー認証およびIPアドレス制限で保護します。
管理者であるAdminのパスワードは必ず変更する様にしてください。
Directory ServiceへのアクセスはDirectory Service管理用のEC2インスタンスから行います。
このEC2インスタンスへのアクセス制限はセキュリティグループで行います。
Directory Serviceのユーザーパスワードの変更やセキュリティグループの設定は
[こちら](../patterns/aws/virtual_secure_room_directory_service.md)のガイドを確認ください。

## 仮想セキュアルームから本番環境にアクセスできる

仮想セキュアルームは本番環境へセキュアにアクセスすることを目的としています。
しかし、仮想セキュアルームと本番環境のネットワークは隔離すべきです。
これは仮想セキュアルームの変更などが本番環境に対して思わぬ影響を与えないようにするためです。
そのため、Eponaでは仮想セキュアルーム用と本番環境用でVPCを分けて構成することを推奨します。

異なるVPC間でAWSに閉じたセキュアなアクセスを行うにはVPCピアリングを設定します。
EponaではVPCピアリングを設定するために[network pattern](../patterns/aws/network.md)と
[vpc_peering pattern](../patterns/aws/vpc_peering.md)を提供します。

なお、VPCピアリングを設定した後に本番環境側で仮想セキュアルームからのアクセスを許可する必要があります。
具体的にはセキュリティグループのインバウンド設定です。
たとえば仮想セキュアルームから本番環境のRDSへ接続したい場合、RDSに付与しているセキュリティグループのインバウンドにWorkSpacesのセキュリティグループを設定します。

また、仮想セキュアルームからはAWSマネジメントコンソールへアクセスもできます。
これは後述する[virtual_secure_room_proxy pattern](../patterns/aws/virtual_secure_room_proxy.md)とも関連します。
WorkSpacesからAWSマネジメントコンソールへは[user pattern](../patterns/aws/users.md)で作成したIAMユーザーを使いログインします。

## 仮想セキュアルームからのアウトバウンドは制限できる

仮想セキュアルームは本番環境へ接続できるため、仮想セキュアルームから本番データを持ち出せないように制限する必要があります。
また、インターネットへ自由にアクセスできてしまうと仮想セキュアルーム内のWorkSpacesがマルウェアに感染する危険性もあります。
Eponaでは仮想セキュアルームからのアウトバウンドを制限するため仮想セキュアルーム用のプロキシとして
[virtual_secure_room_proxy pattern](../patterns/aws/virtual_secure_room_proxy.md)を提供します。

プロキシ設定で仮想セキュアルームから任意のURLへのアクセスを許可します。
Eponaでは参考としてマネジメントコンソールなどAWSへ接続するために最低限必要な設定を案内しています。
たとえばプロジェクトで使用しているGitリポジトリのURLなど、必要なアクセス先は利用者自身で追加する必要があります。
設定をすれば指定のアウトバウンドへの通信は可能となりますが、プロキシの本来の目的は必要なアクセス先を限定するためのものです。
仮想セキュアルームから接続可能なアクセス先は、必要なものだけに絞り、利用者自身の責任で管理してください。

プロキシは[Amazon ECS](https://aws.amazon.com/jp/ecs/)(以下、ECS)のサービスとして
[AWS Fargate](https://aws.amazon.com/jp/fargate/)で動かします。
仮想セキュアルームの端末には上記ECSサービスをプロキシに設定してください。
詳しいやり方は[こちら](../patterns/aws/virtual_secure_room_proxy.md)のガイドを参照ください。

## 本番環境の運用を実施するにあたって管理者の許可を必須にできる

仮想セキュアルームは本番環境へアクセスできてしまうため、誰がいつアクセスするか管理者が把握する必要もあるでしょう。
そのため、Eponaでは仮想セキュアルームのWorkSpacesを起動できるIAMユーザーを制限しています。
具体的には`AmazonWorkSpacesAdmin`のIAMポリシーを持つIAMユーザーがWorkSpacesを起動できます。
Eponaの[user pattern](../patterns/aws/users.md)で提供してるIAMユーザーのうちこのIAMポリシーを持つのは`AdminRole`IAMロールに属するIAMユーザーのみです。

一方で、仮想セキュアルームのWorkSpacesは常時起動している必要はありません。
また、利用料を抑えるためにも必要な時だけ起動する方が望ましいです。
そのため、Eponaで提供する仮想セキュアルームのWorkSpacesは時間経過で自動停止するように設定します。
デフォルトでは2時間としていますが停止までの時間は利用者で調整可能です。
なお、WorkSpacesを停止してもWorkSpacesに保存されたデータは保持されます。
自動停止の設定については[こちら](../patterns/aws/virtual_secure_room_workspaces.md)のガイドを確認ください。
