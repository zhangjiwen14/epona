# ECS Fargate上のコンテナのメトリクスをDatadogへ送信する

## 概要

Amazon Elastic Container Service（以下ECS）のFargate起動タイプで起動したコンテナは、ホストがマネージドになります。
このため、コンテナのホストにSSHで接続して直接メトリクスを収集するといったことができません。

このページでは、ECSのFargate起動タイプで起動しているコンテナのメトリクスをDatadogへ送信する方法について説明します。
なお、ここでのメトリクスは、CPU使用率やメモリ使用量などのシステムメトリクスを指しています。

## このページの内容と、patternとの関連

このページで説明する手順は、AWS CodePipeline (以下CodePipeline)が読み込む[タスク定義](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/task_definitions.html)を修正する内容となっています。
このCodePipelineは、[cd_pipeline_backend pattern](../../patterns/aws/cd_pipeline_backend.md)で構築されている前提となります。

また、本手順ではECS上で実行するコンテナを追加します。
このため、当該コンテナが利用するAmazon CloudWatch (以下CloudWatch)のロググループを追加する必要があります。
コンテナ用のロググループは、[public_traffic_container_service pattern](../../patterns/aws/public_traffic_container_service.md)で構築されている前提となります。

## 事前準備

以下の準備ができていることを前提とします。

- Datadogユーザー登録が済んでいること

## Fargate上のコンテナのメトリクスをDatadogに送信する方法について

Fargate起動タイプで動かしているコンテナのメトリクスを収集するため、DatadogはDatadog Agentのコンテナイメージを提供しています。
このコンテナイメージは、同じタスク内で動いている全てのコンテナのメトリクスを収集します。
そして、集めたメトリクスをDatadogに送信する機能を持っています。

![Datadog Agentの仕組み](../../resources/send_metrics_to_datadog.png)

このページでは、CodePipelineが読み取るタスク定義にDatadog Agentのコンテナを追加する手順について説明しています。

Datadog Agentの仕組みの詳細については、[Datadogのドキュメント](https://docs.datadoghq.com/ja/integrations/ecs_fargate/)を参照してください。
また、具体的にどのようなメトリクスが送信されるかについては、Datadogのドキュメントの[収集データ](https://docs.datadoghq.com/ja/integrations/ecs_fargate/#%E5%8F%8E%E9%9B%86%E3%83%87%E3%83%BC%E3%82%BF)を参照してください。

## Datadog API キーの設定

Datadog Agentを使ってメトリクスを送信するためには、DatadogでAPIキーを作成してDatadog Agentのコンテナに設定する必要があります。

Datadog AgentコンテナにDatadog APIキーを設定するには、環境変数を使用します。
ただし、Datadog APIキーは秘匿情報なので、環境変数としては設定しません。
代わりに、[parameter_store pattern](../../patterns/aws/parameter_store.md)を使い、
AWS Systems Managerのパラメータストアに保存して使用します。

ここでは、Datadog APIキーを作成する方法と、`parameter_store pattern`の設定にDatadog APIキーを追加する方法について説明します。

### Datadog API キーの作成

Datadog APIキーの作成は、Datadogの画面から行います。

詳しくは、Datadogのドキュメントの[API キーとアプリケーションキー](https://docs.datadoghq.com/ja/account_management/api-app-keys/)および、[キーの追加](https://docs.datadoghq.com/ja/account_management/api-app-keys/#%E3%82%AD%E3%83%BC%E3%81%AE%E8%BF%BD%E5%8A%A0)を参照してください。

### parameter_store pattern に Datadog API キーを追加する

まず、Datadog APIキーを[入力変数ファイル](https://www.terraform.io/docs/configuration/variables.html#variable-definitions-tfvars-files)で渡せるようにするため、下記のように変数の定義を追加します。

```terraform
variable "datadog_api_key" {
  type = string
}
```

次に、Datadog APIキーの設定を`parameters`に追加します。

```terraform
  parameters = [
    ...
    {
      name   = "/App/Config/datadog_api_key"
      value  = var.datadog_api_key
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/my-encryption-key"].key_alias_name
    }
  ]
```

そして、以下のようにDatadog APIキーを入力変数ファイルに追加します。

```terraform
...
datadog_api_key = "[作成したDatadog APIキー]"
```

最後に`parameter_store pattern`を適用すれば、パラメータストアにDatadog APIキーが追加されます。

## Datadog Agentコンテナ用のロググループを追加する

Datadog Agentコンテナが出力するログをCloudWatchに出力するため、ロググループを追加します。
コンテナ用のロググループの作成は`public_traffic_container_service pattern`で行うため、この設定を修正します。

詳しくは、[public_traffic_container_service pattern](../../patterns/aws/public_traffic_container_service.md#ログの保存について)を参照してください。

なお、CloudWatchへのログ出力は、AWS FireLensを使用することをおすすめします。
詳しくは、[Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する](../../how_to/aws/ecs_log_send_to_cloudwatch_logs_using_firelens.md)を参照してください。

## Datadog Agentのコンテナをタスクに追加する

ここでは、CodePipelineが読み込むタスク定義にDatadog Agentのコンテナを追加する方法について説明します。

Datadog Agentのコンテナは、下記のように`taskdef.json`の`containerDefinitions`に追加して設定します。

:information_source: `logConfiguration`は、AWS FireLensを使う場合の設定となっています。

```json
{
  "ipcMode": null,
  "taskRoleArn": "...",
  "executionRoleArn": "...",
  "containerDefinitions": [
    {
      # アプリケーションのコンテナ定義
    },
    {
      "name": "datadog-agent",
      "image": "datadog/agent:latest",
      "essential": true,
      "logConfiguration": {
        "logDriver": "awsfirelens",
        "options": {
          "Name": "cloudwatch",
          "region": "ap-northeast-1",
          "log_group_name": "...",
          "auto_create_group": "false",
          "log_stream_prefix": "..."
        }
      },
      "memoryReservation": 256,
      "cpu": 10,
      "environment": [
        {
          "name": "ECS_FARGATE",
          "value": "true"
        }
      ],
      "secrets": [
        {
          "name": "DD_API_KEY",
          "valueFrom": "/App/Config/datadog_api_key"
        }
      ]
    }
  ],
  ...
}
```

`image` には、[datadog/agent](https://hub.docker.com/r/datadog/agent)を使用します。

`memoryReservation` と `cpu` に設定している値は、[Datadogのドキュメント](https://docs.datadoghq.com/ja/integrations/ecs_fargate/#web-ui)で下記のように指定されている値を設定しています。

> 10.Memory Limits に、ソフト制限として **256** を入力します。  
> 11.Advanced container configuration セクションまでスクロールし、CPU units に **10** と入力します。

`environment`では、環境変数`ECS_FARGATE`を定義し値に`true`を設定しています。
この設定により、Datadog Agentのコンテナは自分が今ECSのFargate上で動いていることを認識し、メトリクスを収集するようになります。

`secrets`では、環境変数`DD_API_KEY`を定義し、パラメータストアに格納したDatadog APIキーを設定しています。

以上の設定を、CodePipelineが読み込むS3のバケットにアップロードして、デプロイを実行してください。
Datadog Agentのコンテナが、アプリケーションと同じタスク内で起動するようになります。

## Datadogに送信されたメトリクスを確認する

アプリケーションのコンテナと同じタスク内で起動したDatadog Agentのコンテナは、自動的にメトリクスの収集と送信を開始します。

送信されたメトリクスは、DatadogのMetrics Explorer（メニューのMetrics->Explorer）で次のように確認できます。

![Metrics Explorerで見たメトリクスの様子](../../resources/datadog_agent_metrics_explorer.jpg)

Datadog Agentが送信するメトリクスの名前は、全て`ecs.fargate`で始まります。
したがって、この値で絞り込むと受信したメトリクスを探しやすくなります。
