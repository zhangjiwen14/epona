# ビジネス指標可視化のためのAmazon QuickSightのセットアップと使い方ガイド

本ドキュメントでは、Eponaで構築したサービスのビジネス指標をAmazon QuickSight(以降、QuickSight)で可視化するためのガイドをします。

本ガイドでは以下の情報について解説します。

- [管理者向け] QuickSightの開始方法
- [管理者向け] QuickSightのユーザー管理方法
- [管理者、作成者向け] QuickSightで取得するデータの定義方法
- [管理者、作成者向け] QuickSightでのメトリクス作成、ダッシュボード共有方法
- [全ユーザー向け] ダッシュボードの閲覧方法

:warning:
本ガイドでは、ユースケースに基づくビジネス指標を可視化する方法や考え方については解説しません。
これは、利用者によってビジネス指標や可視化するデータ、分析が異なっており、Eponaとして画一的なガイディングを実施できないためです。
どのようなビジネス指標を可視化するかは本ガイドを参照した上で、各サービスチームで十分に検討してください。

## はじめに

チームや企業がサービスやビジネスを成長させるためには、自分たちのサービスやビジネスに対してさまざまな意思決定が必要となります。
このような意思決定は、勘や経験といった属人的な要素ではなく、蓄積されたデータに基づいて実施されることが重要です。

このようなデータに基づく意思決定を支援するのがBIツールです。
多くのBIツールでは、さまざまなリソースに分散したデータを収集し、収集されたデータを結合してデータを可視化する機能を有しています。
また、BIツールによっては収集されたデータから新たな知見を発見するためのInsightを提供する機能を有しているものもあります。

このようにBIツールを導入することで、収集されたデータから利用者が分析を行い、ビジネスに重要な意思決定の支援が期待できます。

## Amazon QuickSightについて

[Amazon QuickSight](https://aws.amazon.com/jp/quicksight/)は、クラウドスケールのBIを提供するマネージドサービスです。

QuickSightは、AWSサービスだけでなくサードパーティのサービス、オンプレミスのデータソースに接続してデータの収集ができます。
データを可視化するダッシュボードについては、ブラウザだけでなくモバイルデバイスからの参照も可能です。
また分析手法として、機械学習と自然言語を活用した`ML Insights`が用意されているため、これらの専門知識があまりないユーザーも分析や予測が実施できます。

QuickSightの詳細な特徴については、以下のドキュメントを参照してください。
[Amazon QuickSight](https://aws.amazon.com/jp/quicksight/)

### Editionによる違いと料金体型について

QuickSightには`Standard Edition`と`Enterprise Edition`のサブスクリプションがあり、利用できる機能が異なります。

本ガイドに関連する`Standard Edition`と`Enterprise Edition`の違いを以下にまとめます。

| 機能                                                                                                          | Standard Edition | Enterprise Edition |
| :------------------------------------------------------------------------------------------------------------ | :--------------: | :----------------: |
| [SPICE](https://aws.amazon.com/jp/quicksight/resources/faqs/)の利用                                           |       :o:        |        :o:         |
| ダッシュボードの利用                                                                                          |       :o:        |        :o:         |
| モバイル端末からのアクセス                                                                                    |       :o:        |        :o:         |
| 閲覧者権限                                                                                                    |       :x:        |        :o:         |
| <!-- textlint-disable spellcheck-tech-word --> ML Insightの利用 <!-- textlint-enable spellcheck-tech-word --> |       :x:        |        :o:         |
| Amazon VPCへの接続                                                                                            |       :x:        |        :o:         |

QuickSightのEditionによる詳細な違いは下記リンクの`比較`ページを参照してください。
[料金 Amazon QuickSight | AWS](https://aws.amazon.com/jp/quicksight/pricing/?nc=sn&loc=4)

:information_source:
本ドキュメントの画面イメージは、全て`Enterprise Edition`の利用に基づく画面です。
`Standard Edition`を利用している場合は、一部画面の表示に違いがあります。

また、QuickSightの料金体系はそれぞれのEditionによって異なります。
どちらのエディションも基本的には、QuickSightの利用ユーザー単位の課金になります。
`Enterprise Edition`で有効になる`閲覧者`については、ユーザーあたり最大5ドルを上限とした閲覧セッション数の従量課金となります。

QuickSightの利用料金の体系は下記リンクを参照してください。
[料金 Amazon QuickSight | AWS](https://aws.amazon.com/jp/quicksight/pricing/?nc=sn&loc=4)

## 想定する適用対象環境

QuickSightは、取得すべきメトリクスが保存されるRuntime環境へ導入することを想定しています。

:information_source:
管理者や作成者権限をもつユーザーがQuickSightで作業をする場合、本番環境を含むRuntime環境上へのクエリを定義することになります。  
そのため、管理者や作成者となるユーザーは仮想セキュアルームからQuickSightにアクセスして作業することを推奨します。  
仮想セキュアルームの適用と詳細については、以下のドキュメントを参照してください。

- [virtual_secure_room_directory_service pattern](../../patterns/aws/virtual_secure_room_directory_service.md)
- [virtual_secure_room_workspaces pattern](../../patterns/aws/virtual_secure_room_workspaces.md)

一方で、閲覧者となるユーザーは共有されたダッシュボードの閲覧のみ実施できます。  
ダッシュボードにコンプライアンス違反となるデータが表示されないようにしていれば、仮想セキュアルーム外からの閲覧も可能になるでしょう。

## 前提事項・事前準備

BIは今あるデータ可視化のために導入するものではなく、ビジネスの成長と長期的な運用のために、データに基づいた適切な意思決定を行うことが目的です。
適切な意思決定を行うためには、どのようなデータを可視化してどのように分析するかを明確にすることが重要です。

BIツールによって適切なビジネス分析をするためには、BIツールを導入する前に以下のようなポイントを検討する必要があります。

- サービスで達成すべきビジネス指標は何かを定義する
- サービスを提供するシステムがビジネス指標の分析を実施するために、どのようなデータが必要になるかを定義する
  - どのようなグラフや軸をもとに、ビジネス指標の可視化/分析をするか
  - ビジネス指標を可視化するためのデータが収集できるように、アプリケーションがどのようなデータ構造を取るべきか
  - 将来的な可視化のために、データが蓄積できるようになっているか
- ビジネスを成長させるために、どのような分析をする必要があるかを定義する

上記のポイントを定義した上で、サービスにBIツールを導入してください。

:information_source:
QuickSightで利用できるデータ型は、以下のドキュメントを参照してください。  
[サポートされているデータ型と値 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/supported-data-types-and-values.html)

## 作業の流れ

本ガイドではQuickSightの利用方法について、S3バケットからのデータ可視化とAmazon VPC内に構築されたRDSからのデータ可視化を例に説明します。

1. [QuickSightの有効化](#QuickSightの有効化)
1. [QuickSight用ユーザーの追加](#QuickSight用ユーザーの追加)
1. [データソースの作成](#データソースの作成)
1. [データソースからメトリクスを作成](#データソースからメトリクスを作成)
1. [ダッシュボードの共有](#ダッシュボードの共有)

### QuickSightの有効化

ここでは、QuickSightを開始する方法について説明します。
本手順は管理者ユーザーが実施してください。

AWSマネジメントコンソールから、QuickSightを選択するとQuickSightのランディングページが表示されます。
サインアップされていない場合は、以下の手順にしたがってサインアップをしてください。

1. AWSマネジメントページからAmazon QuickSightを選択する
1. Amazon QuickSightランディングページにて、`Sign up for QuickSight`をクリックする
1. エディションを比較、選択して`続行`をクリックする
1. リージョンとアカウント名、QuickSight管理者ユーザーとなるメールアドレスの入力、各種リソースへのアクセス権限を選択し`完了`をクリックする

- 各種リソースへのアクセス権限は管理画面でも設定可能です
- :warning: 利用リージョンは後で変更も可能ですが、SPICEで利用できる容量はここで選択されたリージョンに追加されます  
別リージョンでSPICEを利用する場合、別途容量の追加購入が必要になります

### QuickSight用ユーザーの追加

ここでは、QuickSightで利用するユーザーを追加する方法について説明します。
本手順は、管理者権限を持つユーザーで実施できます。

QuickSightで用意されている権限と実施可能な操作は以下のようになっています。

| 操作                                                      | 管理者 | 作成者 | 閲覧者(`Enterprise Edition`) |
| :-------------------------------------------------------- | :----: | :----: | :--------------------------: |
| QuickSightの管理(ユーザー管理、AWSサービスへの権限管理等) |  :o:   |  :x:   |             :x:              |
| データソースの管理(追加、削除)                            |  :o:   |  :o:   |             :x:              |
| 取り込んだデータセットの操作                              |  :o:   |  :o:   |             :x:              |
| メトリクスの管理(作成、編集)                              |  :o:   |  :o:   |             :x:              |
| ダッシュボードの共有                                      |  :o:   |  :o:   |             :x:              |
| ダッシュボードの閲覧                                      |  :o:   |  :o:   |             :o:              |

管理者権限をもつユーザーはQuickSightアカウントのリソースアクセスやユーザー管理権限を持っています。
そのため、AWSアカウントの管理者、もしくはそれに準じるユーザーへ権限を割り当てた方が良いでしょう。

一方で作成者権限では、QuickSightへのデータ取り込みやメトリクス作成といった、分析に必要なデータの可視化を実施できます。
以下のユーザーに割り当てることを推奨します。

- データソース作成のために、データがどのAWSリソースに存在するかを熟知したEponaで構築したインフラ環境に詳しいユーザー
- ダッシュボード作成と共有のために、ビジネス分析にどのようなメトリクスが必要になるかの検討を実施するユーザー
  - 開発を実施しないビジネスサイドのユーザーも対象となります

閲覧者権限は、ダッシュボードの閲覧のみ実施できるユーザーです。  
日常的にダッシュボードを確認したり、ダッシュボードに表示されたデータから新たな意思決定をするユーザーに閲覧者権限を割り当ててください。

QuickSightを利用するユーザーの追加は、以下の手順で実行できます。

:information_source:
本手順で追加されるユーザーは、QuickSightで管理されるユーザーです。

1. QuickSightの管理画面に移動する。  
  [管理者ユーザーのみ] トップページ右上のアイコンをクリックし、`QuickSightの管理`を選択することで管理画面に移動できます
1. メニューの`ユーザーを管理`を選択し、`ユーザーを招待`をクリックする
1. ダイアログ上部のテキストボックスに"追加したいユーザー名"もしくは"追加したいユーザーのメールアドレス"を入力し、`+`ボタンをクリックする
1. ユーザーのメールアドレスと権限を選択し、`招待`をクリックする

   - :warning: ここで選択した権限については、その後に上位権限から下位権限へ引き下げることができません。  
   このような操作をする場合、一度ユーザーを削除してから再度ユーザーを追加してください

上記手順を実施後、ユーザーのメールアドレスに招待メールが送信されます。
メールに記載されたリンクをクリックすることで、ユーザー登録は完了です。

### データソースの作成

ここでは、QuickSightでの可視化、分析を実施するためのデータ取得元となるデータソースへの接続方法を説明します。
本手順は、管理者、作成者権限を持つユーザーで実施できます。

本ガイドでは、例として以下の2つのデータソースとの接続方法について説明をします。

- [S3バケットとの接続](#S3バケットとの接続)
- [Private Subnetに配置されたRDSとの接続](#Private_Subnetに配置されたRDSとの接続)
  - :warning: Private Subnet内のデータソース接続は`Enterprise Edition`のみサポートされています。ご注意ください

上記以外のデータソースへの接続については、以下のドキュメントを参照ください。

[データソースの作成 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/create-a-data-source.html)

:warning:
QuickSightはデータソースに格納された情報をもとにビジネス指標の可視化を行います。
[前提事項・事前準備](#前提事項・事前準備)の項でも述べたように、ビジネス指標の可視化/分析にはどのようなデータが必要になるかは事前に検討する必要があります。

#### S3バケットとの接続

ここでは、S3バケットにアップロードされたcsvファイルをデータソースとして、QuickSightがS3バケットと接続する方法について説明します。

##### 1. インポートするデータファイルの準備

QuickSightでS3バケットをデータソースとして利用する場合、アップロードされたファイルをデータセットとして読み込むことになります。
QuickSightがS3にアップロードされたファイルをデータソースとして扱うためには、ファイルは以下の条件を満たしている必要があります。

- インポートするファイルは、全て同じファイル形式
  - 対応しているファイル形式は、`CSV` `TSV` `CLF` `ELF` `JSON`です
- 各列のデータタイプは、全ての行で一致している
  - 対応しているデータ型は[サポートされているデータ型と値 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/supported-data-types-and-values.html)を参照してください
- インポートするファイルのエンコードが`UTF-8(BOMなし)`

これらの条件を満たすファイルをQuickSightが接続するS3バケットにアップロードしてください。

##### 2. マニフェストファイルの用意

次に、QuickSightがどのS3バケットにアクセスし、どのファイルをどのように取り込むかを定義したマニフェストファイルを用意します。
マニフェストファイルは、JSON形式で記述されます。

マニフェストファイルの形式は`Amazon QuickSight形式`と`Amazon Redshift形式`が存在し、それぞれ以下の制約があります。

- Amazon QuickSight形式
  - JSON形式であり、拡張子は`.json`
  - 記述するJSONのフィールドは[Amazon QuickSight用のマニフェストファイル形式](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/supported-manifest-file-format.html#quicksight-manifest-file-format)を参照してください
- Amazon RedShift形式
  - JSON形式であり、拡張子は任意の拡張子
  - 記述するJSONのフィールドは[マニフェストを使用し、データファイルを指定する - Amazon Redshift](https://docs.aws.amazon.com/ja_jp/redshift/latest/dg/loading-data-files-using-manifest.html)を参照してください

以下に、S3バケットからデータを取得するAmazon QuickSight形式のmanifest.jsonのサンプルを示します。

```json
{
   "fileLocations": [
      {
        "URIPrefixes": [
           "s3://epona-quicksight-bucket/object-path-1/",
           "s3://epona-quicksight-bucket/object-path-2/"
        ]
      }
   ],
   "globalUploadSettings": {
      "format": "CSV",
      "delimiter": ",",
      "textqualifier": "\"",
      "containsHeader": "true"
   }
}
```

上記のサンプルファイルについて解説します。

`fileLocations`では、接続するS3バケットの情報を定義します。
この例では、`epona-quicksight-bucket`という名称のS3バケットの`object-path-1`, `object-path-2`パス以下の全てのファイルにアクセスします。

`globalUploadSettings`は、インポートするファイルの形式について定義します。
この例では区切り文字が`,`であり、テキストが`"`で括られており、ヘッダ行があるCSVファイルを読み込んでいます。

##### 3. S3データセットの作成

S3バケットへのファイルのアップロードと、マニフェストファイルの準備ができたら、QuickSightでデータセットとして取り込みを行います。

1. QuickSightのトップページ左の`データセット`を選択し、`新しいデータセット`をクリックします  

  ![quicksight_s3_dataset_1](../../resources/quicksight_monitor/quicksight_s3_dataset_1.png)

1. 表示されているデータソース一覧から`S3`を選択します
1. 表示されたダイアログの`データソース名`にデータソースの名称、`マニフェストファイルのアップロード`では作成したマニフェストファイルを入力し、`接続`をクリックします

   - インターネットにアップロードされている場合、`URL`を選択しURLを入力してください  
   （S3バケットにマニフェストファイルがアップロードされている場合は、S3オブジェクトのURLも指定可能です）
   - ローカルファイルからアップロードする場合、`アップロード`を選択しアイコンをクリックしたダイアログからマニフェストファイルを選択してください

   ![quicksight_s3_dataset_2](../../resources/quicksight_monitor/quicksight_s3_dataset_2.png)

1. 正しく接続できたら、確認ダイアログが表示されます

   - インポートしたデータで分析を開始する場合は、`視覚化する`をクリックし[データソースからメトリクスを作成](#データソースからメトリクスを作成)の手順に進みます
   - データのフィルタリング・フィールドの編集やプレビューを行う場合は`データの編集/プレビュー`を選択し[データセットの編集](#データセットの編集)の手順に進みます

これで、QuickSightがS3バケットをデータソースとして取り込むことができます。

#### Private_Subnetに配置されたRDSとの接続

ここでは、QuickSightを`database pattern`で構築されたPrivate Subnet上のRDSと接続する方法について説明します。

:warning: QuickSightがAmazon VPC(以降、VPC)と接続するためには、`Enterprise Edition`のサブスクリプションが必要です。
本手順は、`Enterprise Edition`のサブスクリプションを契約した上で実施してください。

##### 1. QuickSight用のセキュリティグループを作成する

QuickSightがVPC内に構築されたRDSと接続するためには、RDSとアクセスするためのセキュリティグループの設定が必要です。
これは、QuickSightで利用するElastic Network InterfaceにアタッチされたセキュリティグループはStatefulな通信がなされないためです。

:information_source:
[Amazon QuickSightのプライベートVPC内のデータアクセスの設定方法について | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-quicksight-private-vpc-access/)

Eponaでは、QuickSightがVPCと接続するためのセキュリティグループを作成する[`quicksight_vpc_inbound_source pattern`](../../patterns/aws/quicksight_vpc_inbound_source.md)を用意しています。

Eponaで構築したRDSをQuickSightのデータソースとする場合、[quicksight_vpc_inbound_source pattern](../../patterns/aws/quicksight_vpc_inbound_source.md#patternの適用順序)の`patternの適用順序`を参考にしてください。

[`patternの適用順序 | quicksight_vpc_inbound_source pattern document`](../../patterns/aws/quicksight_vpc_inbound_source.md#patternの適用順序)

##### 2. VPCの接続設定を行う

VPCのリソースと接続するためには、QuickSightの管理画面でVPCと接続設定を行う必要があります。

:warning: この操作は、管理者権限をもつユーザーで実施してください。

1. 管理者権限をもつアカウントで管理画面にアクセスする
1. 管理画面の`VPC接続の管理`から`VPC接続の追加`をクリックする

  ![quicksight_vpc_config_1](../../resources/quicksight_monitor/quicksight_vpc_config_1.png)

1. "VPC接続の追加"ページで以下のように値を設定し、`作成`をクリックする

   - `VPC接続名`は任意の接続の名称
   - `VPC ID`にRDSが構築されているVPC
   - `サブネットID`にPrivate SubnetのID
   - `セキュリティグループID`に`quicksight_vpc_inbound_source pattern`で出力されたセキュリティグループID
   - (DNSリゾルバーを利用している場合) DNSリゾルバーのエンドポイント

   ![quicksight_vpc_config_2](../../resources/quicksight_monitor/quicksight_vpc_config_2.png)

   - 入力に際しては、[『グループ管理ガイド』の「VPC 接続を構成する」 QuickSight コンソール - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/vpc-creating-a-connection-in-quicksight.html)も参照してください

##### 3. RDSデータセットの作成

QuickSight用セキュリティグループの作成と、VPCの接続設定ができたら、QuickSightでRDSをデータセットとして取り込みできます。

1. QuickSightのトップページ左の`データセット`を選択し、`新しいデータセット`をクリックします
1. 表示されているデータソース一覧から`RDS`を選択します
1. 表示されたダイアログに以下の情報を入力します

   - `データソース名`にデータソースの名称
   - `インスタンスID`で接続するRDSのインスタンスIDを選択
   - `接続タイプ`で[2. VPCの接続設定を行う](#2.-VPCの接続設定を行う)で作成したVPC接続名を選択
   - `データベース名`にRDSのデータベース名
   - `ユーザー名`にデータベースへアクセスするためのユーザー名(`database pattern`のusernameで設定した値)
   - `パスワード`にデータベースへアクセスするためのパスワード(`database pattern`適用後、再設定したパスワード)

   ![quicksight_rds_dataset_1](../../resources/quicksight_monitor/quicksight_rds_dataset_1.png)

1. `データソースを作成`をクリック

   - `接続を検証`をクリックすることでRDSへ接続できるかを確認できます
   - :warning: [2. VPCの接続設定を行う](#2.-VPCの接続設定を行う)を実施直後では、RDSと接続できない場合があります  
   RDSに接続できない場合、時間をおいてから再度RDSデータセットの作成を試してください

1. 正しく接続できたら以下のような画面が表示されるので、QuickSightで取り込む`スキーマ`と`テーブル`を選択し`選択`をクリックします

  ![quicksight_rds_dataset_2](../../resources/quicksight_monitor/quicksight_rds_dataset_2.png)

1. データの取り込み方法を選択し、次のアクションを決めます

   - データの取り込み方法は以下が選択できます
      - SPICEに取り込む場合、`迅速な分析のためにSPICEへインポート`を選択します
      - 直接クエリを発行してRDSからデータを取得する場合、`データクエリを直接実行`を選択します
         - :warning: この選択では、ダッシュボードでデータ参照をする度にクエリが発行されます。  
         本番環境で利用しているデータベースを参照する場合、パフォーマンスに影響を与える可能性がありますので注意してください
   - 次のアクションは以下があります
      - 選択したテーブルで分析を開始する場合は、`Visualize`をクリックし[データソースからメトリクスを作成](#データソースからメトリクスを作成)の手順に進みます
      - 複数のテーブルをQuickSightに取り込む場合、`データの編集/プレビュー`をクリックして、[データセットの編集](#データセットの編集)の手順に進みます

   ![quicksight_rds_dataset_3](../../resources/quicksight_monitor/quicksight_rds_dataset_3.png)

これでQuickSightがRDSをデータソースとして取り込むことができます。

:warning:
データセットとなるAWSサービスに接続できない場合、QuickSightの管理画面からAWSサービスへのアクセス権限を確認してください。
アクセス権限の確認は、以下の手順で実施できます。

1. 管理者権限をもつアカウントで管理画面にアクセスする
1. 管理画面の`セキュリティとアクセス権限`から`QuickSight の AWS のサービスへのアクセス`の`追加または削除する`をクリックする
1. 対象のAWSサービスの右のチェックボックスが有効になっていることを確認する
  ![quicksight_access_permission](../../resources/quicksight_monitor/quicksight_access_permission.png)
1. 各S3バケットへのアクセス権限については、"詳細"のリンクをクリックして表示された`S3バケットを選択する`をクリックする

   - 表示されたダイアログで、各S3バケットへのアクセス権限を確認できます

上記手順でも解決しない場合、下記ドキュメントも参照してください。  
[データソースに接続できない - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/troubleshoot-connect-to-datasources.html)

#### データセットの編集

ここでは、データセットを編集してメトリクスを作成するためのデータを編集する方法について説明します。

例として、RDSの異なるテーブルとS3データソースとの結合方法を説明します。
本手順は管理者、作成者権限をもつユーザーが実施できます。

"データの編集/プレビュー"では、複数のデータセットから条件に基づいて結合できます。

![quicksight_data_edit_1](../../resources/quicksight_monitor/quicksight_data_edit_1.png)

この機能を利用することで、RDSのテーブル間の結合や異なるデータソースのデータセットを結合し、QuickSightのデータセットとして扱えます。

##### 異なるデータの追加

データセットにデータを追加する場合、画面上部の`データを追加`をクリックすることでデータを追加できます。
`データを追加`をクリックすることで、以下のようなダイアログが表示されます。

![quicksight_data_edit_2](../../resources/quicksight_monitor/quicksight_data_edit_2.png)

- テーブル間の結合

  1. "データを追加"ダイアログ内で新たに追加したい`スキーマ`の`テーブル`を選択し、`選択`をクリックする

     - 選択した`スキーマ`と`テーブル`から独自のSQLを発行に基づいてテーブルのデータを取得する場合、`カスタムSQLを使用`をクリックします
        - :information_source: カスタムSQLを使用する場合は、[クエリエディタの使用 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/adding-a-SQL-query.html)を参照してください

  1. グリッドが表示されている領域(以降、ワークスペース)に新たに追加したデータが追加されます  

      - 異なるデータを繋ぐ線上のアイコンをクリックすると、画面下部に"結合設定"が表示されます
  ![quicksight_data_edit_3](../../resources/quicksight_monitor/quicksight_data_edit_3.png)

  1. 結合条件を指定して`適用`をクリックすることで、異なるテーブルのデータを結合できます

詳細な結合方法については、[データの結合 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/joining-data.html)を参照してください。

- 異なるデータソースとの結合

  1. "データを追加"ダイアログ内の`データソースの切り替え`をクリックする
  1. すでにQuickSightに追加されているデータソース一覧が表示されるので、追加したいデータソースを選択し`選択`をクリックします
    ![quicksight_data_edit_4](../../resources/quicksight_monitor/quicksight_data_edit_4.png)
  1. グリッドが表示されている領域(以降、ワークスペース)に新たに追加したデータが追加されます

      - 異なるデータを繋ぐ線上のアイコンをクリックすると、画面下部に"結合設定"が表示されます

  1. 結合条件を指定して`適用`をクリックすることで、異なるデータセットを結合できます

##### データフィールドの編集

"データの編集/プレビュー"ではメトリクスに用いるデータソースのフィールド名の編集/フィルタリングもできます。

![quicksight_data_edit_5](../../resources/quicksight_monitor/quicksight_data_edit_5.png)

画面左のメニューの`選択`内にあるフィールドのチェックボックスから、メトリクスに利用するフィールドをチェックしてください。
ここで選択されたフィールドをメトリクスの作成に利用できます。

またフィールド名を編集する場合は、画面下部のフィールド名の横にあるアイコンをクリックしてください。
"フィールドを編集"ダイアログ内の`名前`を変更し`適用`することで、メトリクスに利用するフィールド名を任意の名称に変更できます。

![quicksight_data_edit_6](../../resources/quicksight_monitor/quicksight_data_edit_6.png)

また、QuickSightによって自動認識されたデータ型が意図したものでない場合、画面下部のフィールド名下にあるデータ型をクリックすることで、任意の型に変更できます。

:information_source:
ここまでに解説したQuickSightでのデータソースの作成やデータの編集方法について、詳細は以下のドキュメントを参照してください。

[データ準備 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/preparing-data.html)

### データソースからメトリクスを作成

ここでは、作成されたデータソースからビジネス指標をメトリクスとして可視化する方法について解説します。  
本手順は、管理者、作成者権限を持つユーザーで実施できます。

メトリクスの作成をするためには、以下のどちらかを実施してください。

- "データの編集/プレビュー"画面上部にある`保存して可視化`をクリックします
- QuickSightトップ画面左の"データセット"から可視化に利用したいデータを選択し、表示されたダイアログ内の`分析の作成`をクリックします

メトリクスの作成は、以下の画面で実施できます。

![quicksight_metrics_1](../../resources/quicksight_monitor/quicksight_metrics_1.png)

#### 新規のメトリクスを追加する

本手順では、共有するダッシュボードにどのようなメトリクスを追加するかを説明します。

新規にメトリクスを追加する場合、画面上部にある`追加`のアイコンをクリックしてください。

- メトリクスグラフを追加する場合、`ビジュアルを追加`を選択します
- [Enterprise Editionのみ]データセットから新たな知見を得るためのインサイトとなるメトリクスを追加する場合、`インサイトを追加`を選択します
  - 新たなダイアログが表示され、どのようなインサイト情報を表示させるかを選択できます
  - 選択できるインサイト情報と、どのようなインサイトを表示されるかは[インサイトの追加 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/computational-insights.html)をご覧ください

#### 既存のメトリクスを編集する

本手順では、どのようなデータを元にメトリクスグラフを表示するかの方法について説明します。

追加されたメトリクスは、そのままでは何も表示されていません。  
画面右のメトリクスが表示されているスペース（以降、ワークスペースと呼びます）にあるデータを表示させたいメトリクスを選択してください。

1. 画面左のメニューから`視覚化`を選択し、左下にある"ビジュアルタイプ"からどのようなグラフを表示させるかを選択します
1. それぞれのグラフには軸として必要になるデータフィールドが定義されています。
  グラフとして表示させたいフィールドを画面左のフィールドリストから選択することで、どのようなデータをグラフとして表示させるかを設定できます
1. 表示されたグラフのタイトルは自動で設定されますが、タイトル部をクリックすることで任意の名称を設定できます

:information_source:
ビジュアルタイプの"AutoGraph"は選択されたフィールドから、QuickSightが最適なグラフ表示をしてくれるものになります。
それぞれの"ビジュアルタイプ"でできることの詳細は、以下のドキュメントを参照してください。
[Amazon QuickSightビジュアルの使用 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/working-with-visuals.html)

また、メトリクスのグラフに表示するデータに対してフィルタリングも可能です。

1. ワークスペースからフィルタリングをかけたいグラフを選択して、画面左のメニューから`フィルタ`をクリックします
1. 表示されたフィルタメニュー上部の`+`アイコンをクリックすることで、グラフに利用するデータのフィルタ条件を設定できます。
  フィルタリングについては[データのフィルタリング - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/filtering-visual-data.html)を参照してください

上記の手順を繰り返すことでワークスペースにメトリクスが作成されます。  
メトリクスを共有できる状態になったら[メトリクスを共有](#メトリクスの共有)に進み、ユーザーにメトリクスを共有してください。

#### 機械学習を使った分析を実施する

本手順では、既存のメトリクスから機械学習を使った分析を実施するための説明をします。
本機能は、`Enterprise Edition`でのみ実施可能です。

これまでの手順では、ビジネス指標を評価/分析をするために、過去と現在のデータを可視化して現状把握する方法について説明しました。  
ビジネスの分析としてデータに基づいた現状把握をすることは非常に重要ですが、ビジネスを発展させるための意思決定をするために予測があると便利です。  
QuickSightでは、蓄積されたデータから機械学習を利用して将来の値を予測できます。

:warning:
QuickSightでメトリクスデータの機械学習を用いた予測を利用する場合、データの量や蓄積したデータの期間などに制約があります。  
QuickSightで機械学習を利用した分析を実施する場合の制約や、詳細については以下のドキュメントを参照してください。

[ML インサイトの使用 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/making-data-driven-decisions-with-ml-in-quicksight.html)

[ML Insights を Amazon QuickSight で使用するためのデータセット要件 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/ml-data-set-requirements.html)

ここではQuickSightの機械学習を利用した、ビジネス指標予測の可視化方法を解説します。

1. 予測を可視化するメトリクスを選択し、右上に表示されるメニューから`予測を追加`を選択します  

  ![quicksight_ml_insight_1](../../resources/quicksight_monitor/quicksight_ml_insight_1.png)

1. 左に"予測プロパティ"が表示されるので、予測に関する設定を変更します  

  ![quicksight_ml_insight_2](../../resources/quicksight_monitor/quicksight_ml_insight_2.png)

1. "予測プロパティ"下部の`適用`をクリックすることで、メトリクスグラフに将来の値の予測が追加されます  

  ![quicksight_ml_insight_3](../../resources/quicksight_monitor/quicksight_ml_insight_3.png)

### ダッシュボードの共有

サービスにおいて新たな意思決定をするためには、作成されたメトリクスをビジネスサイドのユーザーと共有し、データを元に分析/意思決定を行う必要があります。

本手順では、これまでに作成したメトリクスをダッシュボードとして他のユーザーに共有する方法について説明します。

:information_source:
QuickSightにおけるダッシュボード操作の詳細は[ダッシュボードでの作業 - Amazon QuickSight](https://docs.aws.amazon.com/ja_jp/quicksight/latest/user/working-with-dashboards.html)を参照してください。

#### ダッシュボードを共有する

ここでは、作成したメトリクスをダッシュボードとして共有する方法を説明します。  
本手順は管理者、作成者の権限をもつユーザーが実施できます。

1. メトリクス作成画面の右上の`共有`アイコンを選択し、`ダッシュボードの公開`をクリックします  

  ![quicksight_dashboard_share_1](../../resources/quicksight_monitor/quicksight_dashboard_share_1.png)

1. 表示された"ダッシュボードの公開"ダイアログにダッシュボードの名称を入力して`ダッシュボードの公開`をクリックします  

  ![quicksight_dashboard_share_2](../../resources/quicksight_monitor/quicksight_dashboard_share_2.png)

1. "ダッシュボードをユーザーと共有する"ダイアログが表示されるため、共有したいユーザーを選択して`共有`をクリックします  

  ![quicksight_dashboard_share_3](../../resources/quicksight_monitor/quicksight_dashboard_share_3.png)

これで、共有対象のユーザーにダッシュボードの共有ができました。

#### ダッシュボードを閲覧する

本手順では、共有したダッシュボードの閲覧方法を説明します。
本手順は、全ての権限をもつユーザーが実施できます。

1. [ダッシュボードを共有する](#ダッシュボードを共有する)の手順で、ダッシュボードを共有したユーザーでQuickSightにログインします
1. トップページのダッシュボードを選択すると共有されたダッシュボードが表示されます  
  以下は、閲覧権限をもつユーザーでログインした場合の画面例です  

  ![quicksight_dashboard_share_4](../../resources/quicksight_monitor/quicksight_dashboard_share_4.png)

1. 閲覧したいダッシュボードを選択することで、ダッシュボードの閲覧ができます  

  ![quicksight_dashboard_share_5](../../resources/quicksight_monitor/quicksight_dashboard_share_5.png)
