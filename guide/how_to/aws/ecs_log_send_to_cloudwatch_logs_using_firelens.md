# Amazon ECS上のコンテナログを、AWS FireLensを使ってAmazon CloudWatch Logsに送信する

## 概要

このページでは、Amazon ECS上で動作するコンテナのログをAWS FireLensを介してAmazon CloudWatch Logsに送信する方法について説明します。

:information_source: EponaではAmazon ECSのうち、AWS Fargateを対象にしています。

また、合わせてログドライバーとAWS FireLensに関しても、簡単に記載します。

## このページの内容と、patternとの関連

このページの記載内容は、[cd_pipeline_backend pattern](../../patterns/aws/cd_pipeline_backend.md)が構築する
AWS CodeDeployで使用するタスク定義内(`taskdef.json`)に組み込むことを想定しています。

また、AWS CodeDeployのデプロイ先となるAWS Fargateは[public_traffic_container_service pattern](../../patterns/aws/public_traffic_container_service.md)で構築されます。

## このページの内容に関連するhow_to

このページの手順で、Javaのスタックトレースのような改行を含むログをAmazon CloudWatch Logsに転送すると、
改行ごとに別のログエントリとして取り扱われます。  
このページでは主にFluent Bitを使用した手順を紹介していますが、複数行のログを1つのログエントリに集約する場合は、Fluentdを使用する必要があります。

複数行のログを扱う必要がある場合は、このページの内容に加えて
[複数行で出力されるログをひとつのログに集約する](handling_multiline_log.md)
を参照してください。

## コンテナのログ設定

タスク内で動作するコンテナのログ出力先は、`logConfiguration`で設定します。

[タスク定義の作成](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/userguide/create-task-definition.html)

コンテナの出力するログを取得する仕組みはログドライバーと呼ばれ、`logConfiguration`内の`logDriver`でどのようなログドライバーを使うか指定できます。

[ストレージとログ記録](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/userguide/task_definition_parameters.html#container_definition_storage)

:warning: AWS Fargateを使用し、かつ`logConfiguration`を設定しない場合はコンテナのログを確認できなくなります。

ログドライバーを適切に設定することで、コンテナのログ出力先を制御できます。

## AWS FireLensとは

AWS FireLensは、Fluent BitまたはFluentdと合わせて動作するログドライバーです。

[Firelens の発表 – コンテナログの新たな管理方法](https://aws.amazon.com/jp/blogs/news/announcing-firelens-a-new-way-to-manage-container-logs/)

[カスタムログルーティング](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/userguide/using_firelens.html)

具体的には、以下の手順で導入します。

- タスク定義内に、Fluent BitまたはFluentdのコンテナを追加
- 各コンテナのログドライバーとして`awsfirelens`を指定し、ログ送信先を設定

:information_source: 特に理由がなければ、AWSが提供している[AWS for Fluent Bitイメージ](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/userguide/using_firelens.html#firelens-using-fluentbit)を利用しましょう。

つまり、`awsfirelens`ログドライバーを使用するために、Fluent BitまたはFluentdのコンテナをタスク定義に追加する必要があります。

コンテナのログをAmazon CloudWatch Logsに送信するだけであれば、`awslogs`ログドライバーで実現可能です。

[awslogs ログドライバーを使用する](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/using_awslogs.html)

`awsfirelens`ログドライバーを使用することで、Amazon ECSメタデータの付与、Fluent BitまたはFluentdを使ったログの制御を行えるメリットがあります。

[Amazon ECS メタデータの使用](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/userguide/using_firelens.html#firelens-taskdef-metadata)

特にDatadogが利用できる場合、AWS FireLensおよびAmazon CloudWatch Logsを経由してのログ集約をおすすめします。  
この時、AWS FireLensにより付与されたメタデータも有効に利用できます。

詳細は、次の2つのpatternのドキュメントを参照してください。

- [datadog_forwarder pattern](../../patterns/aws/datadog_forwarder.md)
- [datadog_log_trigger pattern](../../patterns/aws/datadog_log_trigger.md)

## タスク定義でAWS FireLensを使い、Amazon CloudWatch Logsにログを送信する

AWS FireLensおよびAWS for Fluent Bitイメージを使い、Amazon CloudWatch Logsにログをルーティングを行う方法を記載します。

基本的な考え方、設定内容は、以下も参考にしてください。

[FireLens 設定を使用するタスク定義の作成](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/userguide/using_firelens.html#firelens-taskdef)

必要なAmazon CloudWatch Logsのロググループは、以下の和になります。

- Fluent Bitコンテナ用のロググループ
- 他のコンテナ用のロググループ

これらのロググループは[public_traffic_container_service pattern](../../patterns/aws/public_traffic_container_service.md)を使い、明示的に作成するよう構成してください。

タスク内に組み込むFluent Bitのコンテナ定義の例は、以下のようになります。

:information_source: ここでは、利用するリージョンを東京と想定して記載しています。  
:information_source: `[...]`の部分は、サービスなどに応じてわかりやすい名前を指定してください。  
:information_source: Fluet Bit自身のログは、`awslogs`ログドライバーでAmazon CloudWatch Logsに直接送信してください。

```json
   {
      "essential": true,
      "image": "906394416424.dkr.ecr.ap-northeast-1.amazonaws.com/aws-for-fluent-bit:latest",
      "name": "log_router",
      "firelensConfiguration": {
        "type": "fluentbit"
      },
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "[ロググループ名]",
          "awslogs-region": "ap-northeast-1",
          "awslogs-stream-prefix": "[ログストリームの接頭辞]"
        }
      },
      "memoryReservation": 50
    },
```

AWS for Fluent Bitイメージは、使用するリージョンによって選択すべきAmazon ECRも異なります。  
東京リージョン以外で使用する場合は、以下を参照して`image`を指定してください。

[AWS for Fluent Bit イメージの使用](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/userguide/using_firelens.html#firelens-using-fluentbit)

タスク内で動作する他のコンテナの`logConfiguration`は、以下のように設定します。

```json
      "logConfiguration": {
        "logDriver":"awsfirelens",
        "options": {
          "Name": "cloudwatch",
          "region": "ap-northeast-1",
          "log_group_name": "[ロググループ名]",
          "auto_create_group": "false",
          "log_stream_prefix": "[ログストリームの接頭辞]"
        }
      },
```

`logDriver`として`awsfirelens`を指定し、`options`の`Name`では`cloudwatch`を指定します。  
これでAWS FireLensと前述のFluent Bitのコンテナを介して、Amazon CloudWatch Logsにログを送信できます。

この定義で指定した`options`は、以下のドキュメントにも記載がある通り、Fluent Bitのコンテナに連携され設定ファイルの内容に組み込まれます。

[FireLens 設定を使用するタスク定義の作成](https://docs.aws.amazon.com/ja_jp/AmazonECS/latest/userguide/using_firelens.html#firelens-taskdef)

これで、コンテナのログをAWS FireLensを介してAmazon CloudWatch Logsへ送信できるようになりました。
