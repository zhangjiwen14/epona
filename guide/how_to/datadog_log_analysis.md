# Datadogを用いたログからのメトリクス抽出、可視化ガイド

## はじめに

Eponaではモニタリング用のSaaSとして[Datadog](https://www.datadoghq.com/ja/)をサポートしています。
Datadogには各種のメトリクスを集約の上で抽出や集計、可視化する機能があり、サービスの状態を把握する上で大きな役割を果たします。  
一部のメトリクスについては、Datadogのエージェントやクローラーで自動的に収集されます。  
しかし、場合によっては直接のメトリクスではなく、アプリケーションログ等のテキストデータからメトリクスデータを取得しなければならないケースもあります。  
本ガイドでは、Datadogを使ってログからメトリクスを抽出し、可視化する方法について記載します。

## 前提事項・事前準備

以下の準備ができていることを前提とします。

- Datadogユーザー登録が済んでいること
- 必要となるログについて、AWS各サービスからDatadogへのログ収集が実現できていること

Datadogへのログ収集方法に関しては、以下のリンクを参照してください。

- [datadog_forwarder](../patterns/aws/datadog_forwarder.md)
- [datadog_log_trigger](../patterns/aws/datadog_log_trigger.md)

## 作業の流れ

ログからメトリクスを抽出し、可視化するための大まかな流れは以下のようになります。

1. [パイプラインの構築](#パイプラインの構築)
1. [プロセッサーによるログのパース(Parse)](#プロセッサーによるログのパースparse)
1. [ファセットの登録](#ファセットの登録)
1. [可視化用ウィジェットの作成](#可視化用ウィジェットの作成)

## パイプラインの構築

Datadogにログを送信した際、ログが一番最初に通るのが
[パイプライン](https://docs.datadoghq.com/ja/logs/processing/pipelines/)
です。パイプライン上で、受信したログをフィルタで絞り込み、
[プロセッサー](https://docs.datadoghq.com/ja/logs/processing/processors)
を順次適用して加工します。  
CloudTrailやCloudFront等、ログフォーマットが定まっているログについては、Datadogにて
[インテグレーションパイプライン](https://docs.datadoghq.com/ja/logs/processing/pipelines/#%E3%82%A4%E3%83%B3%E3%83%86%E3%82%B0%E3%83%AC%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3%E3%83%91%E3%82%A4%E3%83%97%E3%83%A9%E3%82%A4%E3%83%B3)
が用意されています。  
これを適用することで、ログを自動的に分解して取り込むことができます。

一方、アプリケーションログの場合は、ログフォーマットはアプリケーション依存となるため、フォーマットに合わせてパイプラインを個別に構築する必要があります。  
パイプラインを構築するには、パイプラインの名前（任意）と、「どのログにパイプラインを適用するか」というフィルター条件
([パイプラインフィルター](https://docs.datadoghq.com/ja/logs/processing/pipelines/#%E3%83%91%E3%82%A4%E3%83%97%E3%83%A9%E3%82%A4%E3%83%B3%E3%83%95%E3%82%A3%E3%83%AB%E3%82%BF%E3%83%BC))
を設定する必要があります。  
フィルタの構文はログの[検索構文](https://docs.datadoghq.com/ja/logs/search_syntax/)と同じです。

## プロセッサーによるログのパース(Parse)

[プロセッサー](https://docs.datadoghq.com/ja/logs/processing/processors/)
は、ログに対して構造化のための処理を実行するものです。たとえば以下のようなプロセッサが用意されています。

- Grok構文により、非構造化ログから属性を抽出する[Grokパーサー](https://docs.datadoghq.com/ja/logs/processing/processors/?tab=ui#grok-%E3%83%91%E3%83%BC%E3%82%B5%E3%83%BC)
- URLを要素ごとに分解して属性を抽出する
[URLパーサー](https://docs.datadoghq.com/ja/logs/processing/processors/?tab=ui#url-%E3%83%91%E3%83%BC%E3%82%B5%E3%83%BC)
- 単位を変換したり、簡単な計算をする場合に使用する[算術演算プロセッサー](https://docs.datadoghq.com/ja/logs/processing/processors/?tab=ui#%E7%AE%97%E8%A1%93%E6%BC%94%E7%AE%97%E3%83%97%E3%83%AD%E3%82%BB%E3%83%83%E3%82%B5%E3%83%BC)

その他、プロセッサーの内容については[公式ドキュメント](https://docs.datadoghq.com/ja/logs/processing/processors/?tab=ui)を参照してください。これらを組み合わせることで、ログを解析して必要なメトリクスを抽出することになります。

## ファセットの登録

構造化して取り込んだ属性は、そのままでは検索条件やメトリクスとして使うことはできません。メトリクスの可視化に使用するためには該当の属性を
[ファセット](https://docs.datadoghq.com/ja/logs/explorer/facets/)として登録する必要があります。  
（ファセットとは「切り口」「要素」のことですが、Datadogにおいては属性をインデックス化したものを指します）  
必ずしも全属性をファセットにする必要はなく、検索条件等で使用する属性のみで構いません。

## 可視化用ウィジェットの作成

Datadogにおいてメトリクスを可視化する場所は、主として
[ダッシュボード](https://docs.datadoghq.com/ja/dashboards/)
になります。  
ダッシュボードにはスクリーンボードとタイムボードの2種類があります。両者の違いについては
[スクリーンボードとタイムボード](https://docs.datadoghq.com/ja/dashboards/#%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%83%9C%E3%83%BC%E3%83%89%E3%81%A8%E3%82%BF%E3%82%A4%E3%83%A0%E3%83%9C%E3%83%BC%E3%83%89)
をご参照ください。  
ダッシュボード上でメトリクスの値を可視化するためには、次の手順が必要になります。

1. 可視化に使うウィジェット（表やグラフの種類）を選択する
1. 可視化するメトリクスを選択する
1. フィルターする
1. 集計、ロールアップする

2〜4については、ウィジェットの設定画面内の
[クエリ](https://docs.datadoghq.com/ja/dashboards/querying/)
にて設定します。

なお、ここでの「フィルター」とは、可視化するメトリクスに対する抽出条件を設定するものです。
どのホストから送信されてきたものか、どのタグが設定されているメトリクスか、などの条件を指定します。  
また、「集計」はメトリクスに適用する演算のことです。
平均(avg by)や最大・最小(それぞれmax by、min by)、総和(sum by)等を指定できます。  
「ロールアップ」は時系列でグラフ化する際に設定するもので、データポイント（グラフ上の点）をどれくらいの間隔（1秒、1分、10分など）で丸めるのかを指定するものです。データポイントを細かく取る（狭い間隔）ほどグラフは精密になりますが、上下の激しいデータの場合は線が重なり傾向を見にくくなる場合もあるでしょう。参照したい期間の幅に合わせて、ロールアップにより適切に間隔を調整してください。

## パイプラインサンプル

ここでは、[SPA + REST API構成のサービス開発リファレンスのコード](https://github.com/Fintan-contents/example-chat)
(以降、example-chat)のログを参考に、簡単なダッシュボードを作成してみます。

### パイプラインの構築

まず最初に、example-chatアプリケーションログ用の新しいパイプラインを作成します。  
メニューの[`Logs`]-[`Configuration`]を選択して表示される画面にて[`New Pipeline`]ボタンを押下してください。

パイプライン作成ダイアログが開きますので、フィルター条件(`Filter:`)に`Source:cloudwatch`と入力してください。
これは、example-chatのアプリケーションログがAmazon CloudWatch Logsから連携されるためです。

フィルター条件の入力が終わったら、パイプライン名を`Name:`に入力します。
任意の名称で良いですが、ここでは`example-chat backend app log`とします。

最後に[`Save`]ボタンを押下すると、作成したパイプラインが画面に反映されます。

### プロセッサーによるログのパース(Parse)

作成したパイプライン名をクリックすると、[`Add Processor`] or [`Add Nested Pipeline`]メニューが表示されます。

ここでは、example-chatのログを構造化するため、プロセッサーとしてGrokパーサーを使用します。[`Add Processor`]をクリックしましょう。

そうすると、`New Processor`というプロセッサーの構成画面が開きます。`Select the processor type`で`Grok Parser`を選択しましょう。

`Log Samples`には、実際にパースしたいログのサンプルを入力します。ここでは以下のように入力してみてください。

```text
2020-10-19 09:45:12.430 WARN DEV GET /api/accounts/me - 8f738764-db38-49fe-a28f-412edc0ee8a6 qtp1886478937-17 Unauthorized access to path. path=[/api/accounts/me] method=[GET]
```

次に`Define parsing rules`欄にパース規則を入力します。
わかりやすいように複数行に分けていますが、1行で入力してください。

```grok
example_chat_dev %{date("yyyy-MM-dd hh:mm:ss.SSS"):date} %{word:logLevel}
 %{word:runtimeLoggerName} %{word:http.method} %{notSpace:http.url_details.path}
 %{notSpace:userId} %{notSpace:sessionId} %{notSpace:threadName} %{data:message}
```

正しく入力すると、その下にパースされ構造化されたログ情報が出力されます。

![image](../resources/datadog_log_analysis/grok_parser_sample.png)

パース規則は`ルール名 %{MATCHER:EXTRACT:FILTER} ...`という形の書式になります。  
属性名には任意の名称を設定できますが、Datadogで用意されている
[標準の属性名](https://docs.datadoghq.com/ja/logs/processing/attributes_naming_convention/#%E3%83%87%E3%83%95%E3%82%A9%E3%83%AB%E3%83%88%E3%81%AE%E6%A8%99%E6%BA%96%E5%B1%9E%E6%80%A7%E3%83%AA%E3%82%B9%E3%83%88)
に該当するものがあれば、そちらに揃えたほうが便利です（このあとのファセットの登録を省略できます）。  
上記の例だと、`http.method`が標準の属性名を利用した例です。

最後に、プロセッサー名を`Name the processor`に入力してください。名前は任意で良いですが、ここでは`example-chat access log`としました。

なお、Grokパーサーの使い方については
[公式ドキュメント](https://docs.datadoghq.com/ja/logs/guide/log-parsing-best-practice/)
にも記載がありますので、あわせて参照してください。

### ファセットの登録

メニューの[`Logs`]-[`Search`]で`Log Explorer`を表示します。サイドパネルにある「`+Add`」をクリックするとファセットの登録画面が表示されます（丸印で囲ってある箇所です）。

![image](../resources/datadog_log_analysis/add_facets.png)

ファセットの登録画面の `Path`に、登録したい属性名を`@`付きで設定します。

- `%{word:logLevel}`であれば`@logLevel`

登録が成功すると、`Log Explorer`のサイドパネルの下部に追加されます。

![image](../resources/datadog_log_analysis/added_facets.png)

### 可視化用ウィジェットの作成

メニューの[`Dashboards`]-[`New Dashboard`]を選択すると、ダッシュボードの選択画面が表示されます。  
`Dashboard name`にはデフォルトでユーザー名と作成日時の名称が付与されますが、今回は`example-chat-dashboard`とします。
ダッシュボードの種類は`New Screenboard`を使用します。

空白のダッシュボードが作成されるので、画面上部の`Edit Widgets`をクリックします。すると、ウィジェットの選択メニューが表示されます。  
今回は`Top List`を使って、パスごとのアクセス件数を表示してみます。`Top List`は、複数の値のリストを表示するときに利用するウィジェットです。ディスクごとの空き容量の可視化などにも利用できます。

ウィジェットのアイコンを下の画面にドラッグすると、ウィジェットの編集画面が表示されます。`Graph your data`欄で、可視化するデータの抽出・集計条件を入力します。ここでは`http.url_details.path`でグループ化した件数を取得したいので、以下の画面のように入力します。

![image](../resources/datadog_log_analysis/edit_widget.png)

[`Save`]をクリックして保存すると、ダッシュボード上に以下のような表が表示されます。

![image](../resources/datadog_log_analysis/view_dashboard.png)

以上が、Datadogを用いたログからのメトリクス抽出、可視化の流れとなります。

### ログフィルターが設定されている場合の注意点

[ログフィルター](../patterns/aws/datadog_log_trigger.md#ログフィルターについて)が設定されているログについても、本ガイドに記載の手順で構造化・可視化できます。以下のように読み替えてください。

- [ファセットの登録](#ファセットの登録)は不要です
- [可視化用ウィジェットの作成](#可視化用ウィジェットの作成)について、フィルターにより除外したログでは`Log Event`での検索はできません。事前に必要な情報を[メトリクス](https://docs.datadoghq.com/ja/logs/logs_to_metrics/)に登録し、`Metrics`を指定してウィジェットを作成してください
