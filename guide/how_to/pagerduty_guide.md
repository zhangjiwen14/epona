# PagerDutyでのインシデント管理

## はじめに

Eponaでは[Operation-Less](../../README.md#concept)の観点から、SaaSの利用を推奨しています。  
本ガイドでは、[PagerDuty](https://ja.pagerduty.com/)というSaaSによるインシデント管理について記載します。

## PagerDutyについて

システムが成長していくに従って、複数のアプリケーションやSaaSが組み合わさるようになると、運用面で以下のような課題が起きがちです。

- 複数の監視ツールからバラバラにアラートが届き、それぞれの監視ツールの画面を見てメッセージを確認しなければならない
- 発生内容を目で見て、誰に連絡を取るかをオペレーターが逐一判断しなければならない
- 夜間休日など連絡がつきにくい場合に、オペレーターが手動で何度も電話をかけなければならない
- 連絡が取れたのか、現状誰が対応しているのかが、連絡をした当人達にしかわからない

PagerDutyを導入することで、複数のツールからのアラートを集約し、適切な担当者へ自動的に通知（電話、SMS、メール、モバイルアプリ）することが可能になります。これにより、以下のようなメリットがあります。

- アラートをPagerDutyに集約できる。これにより「まずはPagerDutyを見ればよい」と最初のアクションを統一できる
- 発生内容、発生箇所に応じた担当者へ自動的に連絡できる
- 連絡が取れない場合、他の連絡方法（電話に出なければSMSなど）に切り替えたり、他の担当者に掛け直すなどの対応が自動的に行われる
- PagerDutyのWeb UIを見ることで、誰が応答・担当しているのか状況を把握できる

機能の詳細については
[製品のサポートページ](https://support.pagerduty.com/)
をご参照ください。

## 前提事項・事前準備

本ガイドにおいては以下の準備ができていることを前提とします。

- PagerDutyユーザー登録が済んでいること

なお、[設定例](#設定例)ではDatadogからのアラートを受け取る例を紹介しています。こちらを実施する場合には以下の準備が必要となります。

- Datadogユーザー登録が済んでいること

## 設定後のイメージ

Pageduty設定後のインシデント管理の流れは以下のとおりです。

1. Datadogからメトリクスのしきい値超えやエラーログなどのアラートがPagerDutyに送られます
1. PagerDutyは受信したアラートをもとに連絡先を判断し、順番に通知します。このとき、インシデント情報も作成されます
1. 通知を受け取ったユーザーは、PagerDuty上のインシデント情報を参照し、ステータスを更新します
    - インシデントのステータスについては[こちら](https://support.pagerduty.com/docs/incidents#incident-states)を参照してください
1. 問題が解消したら、インシデントのステータスを更新して、インシデントをクローズします

[クイックスタートガイド](https://support.pagerduty.com/docs/quick-start-guide)
では、ユーザー -> オンコールスケジュール -> エスカレーションポリシー -> サービス
の順に設定するよう解説されていますので、ここでも同様の順に記載します。

## ユーザー

PagerDutyにユーザーを追加するには、権限のあるユーザーでログインし、招待メールを送付する必要があります。
ユーザーを追加する手順については
[こちら](https://support.pagerduty.com/docs/users)
を参照してください。  
招待メールを受け取ったユーザーは、パスワード及びユーザープロファイルを設定します。
[ユーザープロファイル](https://support.pagerduty.com/docs/configuring-a-user-profile)
の情報としては、主に連絡先（Contact Information）と通知ルール（Notification Rules）を設定します。

### 役割

各ユーザーに設定できる役割（権限）の内容については[こちら](https://support.pagerduty.com/docs/user-roles)を参照してください。

- 利害関係者（サービス利用者やお客様）用にインシデント情報の参照のみのユーザーを作成する機能もあります。  
この機能により、状況を個別に連絡しなくとも、PagerDutyを参照していただくだけで情報共有が可能となります。  
なお、この機能を利用する場合はビジネスプランもしくはデジタルオペレーションプランでの契約が必要となります。詳細は[こちら](https://www.pagerduty.com/pricing/)をご確認ください

### 連絡先（Contact Information）

電話番号、SMS、メール、モバイルアプリを設定できます。  
なお、着信は海外からとなるため、電話番号、SMSについては国コード「+81」付きで設定してください。

### 通知ルール（Notificaton Rules）

複数の連絡方法がある場合に、どの連絡方法を使うかを設定できます（まずメールで通知して、10分間応答がなければ電話をかけるなど）。  
また、インシデントの緊急度によって連絡方法を変えることもできます（緊急度が低いものはメール、高いものは電話など）。  
他には、自分がオンコール（インシデント連絡を受ける）状態になることを忘れないよう、事前にリマインドするような設定（`オンコールバックオフ`）も可能です。

## オンコールスケジュール

[オンコールスケジュール](https://support.pagerduty.com/docs/schedules)では[ユーザー](#ユーザー)の担当日・担当時間を設定します。

- 基本的には、ローテーションタイプ（週次／日次など）とハンドオフ時間（交代時間）の設定で作成できます
- ユーザー・ローテーションタイプ・ハンドオフ時間を設定すると、PagerDuty側でオンコールシフトを自動的に割り当てます
- ただし、自動割当では24時間年中無休の想定で割り当てられるため、平日／休日や日中／夜間など細かく制御する場合は[スケジュールの制限](https://support.pagerduty.com/docs/schedules#schedule-restrictions)にて設定してください
- 平日と休日でローテーションを分けるような場合は[スケジュールレイヤー](https://support.pagerduty.com/docs/schedules#schedule-layers)を分けることで設定できます

## エスカレーションポリシー

[エスカレーションポリシー](https://support.pagerduty.com/docs/escalation-policies)では、
[サービス](#サービス)で発生したインシデントを、誰にどの順番で連絡するかを設定します。

- 連絡先としては、オンコールスケジュールまたはユーザーを指定可能です
- 連絡順のことを[レベル](https://support.pagerduty.com/docs/escalation-policies#escalation-levels)といいますが、同一レベルには5人（または5スケジュール）を設定可能です
- インシデントがトリガーされると、PagerDutyはエスカレーションポリシーの最初のレベルに通知しようとします
  - オンコールスケジュールが指定されていた場合は、その時間帯にオンコールとなるユーザーをPagerDutyが自動的に判断し、そのユーザーに通知します
- 通知後、一定期間内にインシデントが確認されない場合、インシデントは次のレベルにエスカレーションされ、以下同様に続きます
- もし最後までエスカレーションしてもインシデントが確認されない場合、再度最初のレベルから連絡し直すよう設定できます
- 次のレベルにエスカレーションするまでの時間（タイムアウト時間）や、最初のレベルから連絡し直す回数は、エスカレーションポリシーの画面で設定可能です

### 注意事項

オンコールスケジュールおよびエスカレーションポリシーの設定に関して、いくつか注意点があります。

#### オンコールスケジュール内でオンコールとなるのは1ユーザーのみ

たとえば、シフトを組んでインシデントの実対応をする担当者とは別に、発生したことを常に知っておきたい人がいたとします。  
PagerDutyでは
[スケジュールレイヤー](https://support.pagerduty.com/docs/schedules#schedule-layers)
という、サイクルが違うスケジュールを層（レイヤー）を分けて登録する設定がありますので、それを使って以下のようにスケジュールを設定したとしましょう。

- レイヤー１：担当者A…インシデント連絡を常に受け取りたい担当者（常にオンコール）
- レイヤー２：担当者B/C/D…交代でインシデント実対応をする担当者（1日交代でオンコール）

しかし、このように設定したとしても、`担当者A`に通知されることはありません。

PagerDutyの仕様上、**ある時間帯にオンコールとなるのは1ユーザーのみ**であり、
同一時間帯に複数のユーザーが重なっていた場合は、**画面上最も下のレイヤー**のユーザーが使用されるからです。

- 上の例で上下を逆にした場合、`担当者A`には通知されますが、`担当者B/C/D`には通知されなくなります

最終的に誰に連絡されるかは画面上の`Final Schedule`に表示されていますので、そちらを確認してください。

同一時間帯に複数のユーザーに通知したい場合は、
[ドキュメント](https://support.pagerduty.com/docs/escalation-policies-and-schedules#schedules-and-escalation-levels)
で紹介されているように、個人別のオンコールスケジュールを作成してエスカレーションポリシーに設定する必要があります。

#### オンコールとなっているユーザーが1人もいない場合、インシデントは作成されない

[スケジュールの制限](#オンコールスケジュール)などで個別にスケジュールを設定すると、スケジュール間に隙間のできる場合があります。  
この隙間の時間帯にインシデントが発生した場合、該当レベルはスキップされて次の連絡先にエスカレーションされます。
ただし、**最後まで行ってもオンコールとなっているユーザーが見つからない場合は、インシデントは作成されません**
（[参考](https://support.pagerduty.com/docs/schedules#creating-schedule-gaps)）。  
最終的に、いつ誰がオンコールとなるのかはエスカレーションポリシーの設定画面の`Schedule`タブなどで確認できますので、忘れずに確認してください。

## サービス

[サービス](https://support.pagerduty.com/docs/glossary#service)
は、インシデントの発生元となるアプリケーションやコンポーネント等を指す概念です。  
[サービスの設定](https://support.pagerduty.com/docs/services-and-integrations)では、システムがどのような`サービス`によって構成されるのか、各`サービス`がどんなイベントに対して誰（エスカレーションポリシー）に連絡するかを設定します。

### インテグレーション

PagerDutyでは様々な外部サービスとのインテグレーションを行うことができ、
別サービスからのインシデント作成、別サービスへのインシデント通知といったことが可能になります。

#### インシデントの作成

PagerDutyでは、以下のようなツールと統合して、インシデントを作成可能です。

- 外部サービス（インテグレーション）  
AWS CloudWatchやDatadogなどからのイベントを検知します。統合可能なサービスについては
[インテグレーションライブラリ](https://www.pagerduty.com/integrations/)
を参照してください
- 電子メール  
電子メールをトリガーとしてインシデントを発生させることが可能です。詳細は
[メール統合ガイド](https://www.pagerduty.com/docs/guides/email-integration-guide/)
を参照してください
- API  
PagerDutyのAPIを直接コールしてインシデントを発生させることが可能です。詳細は
[開発者用ドキュメント](https://developer.pagerduty.com/docs/get-started/getting-started/)
を参照してください

ガイドの後半に[Datadogと統合する場合の設定例](#設定例)を記載していますので、あわせてご確認ください。

#### インシデントの通知

インシデント連絡は[ユーザー](#ユーザー)の連絡先宛に行われますが、拡張機能を使うことでチャットツールなどの外部サービスへの通知ができます。
EponaでサポートしているSlackやMicrosoft Teamsに対する通知ももちろん可能です。

- [Slack Integration Guide](https://support.pagerduty.com/docs/slack-integration-guide)
- [Microsoft Teams Integration Guide](https://support.pagerduty.com/docs/microsoft-teams)

### イベントルール

インテグレーションから受け取ったイベントの内容に基づいて、インシデントの発生を抑止したり、重大度を割り当てるなどのルールを設定できます。  
詳細は
[サービスイベントルール](https://support.pagerduty.com/docs/rulesets#section-service-event-rules)
を参照してください。

- なお、ここで設定できる「重大度」と、インシデントの「緊急度」は別の概念です。詳細は
[こちら](https://support.pagerduty.com/docs/dynamic-notifications)
を参照してください

### アサインと通知

誰に通知を送るかを設定します。以下のような項目を設定可能です。

#### 通知先となるエスカレーションポリシー

どの[エスカレーションポリシー](#エスカレーションポリシー)でインシデント連絡するかを指定します。

#### 緊急度

インシデントの緊急度を設定できます。なお、デフォルトではすべて緊急度「高」となります。以下のような設定が可能です。

- 緊急度を一律に設定（高または低）
- 発生時間帯に応じて高／低を切り替え
- イベントの[重大度](#イベントルール)に応じて高／低を切り替え

[ユーザー](#ユーザー)の通知ルールで緊急度別の連絡方法を設定していた場合には、ここで判定した緊急度により連絡先が決定されます。

#### 未解決インシデントに対するアクション

インシデントが放置されないよう、確認応答のタイムアウトや自動解決の設定が可能です。なお、デフォルトではいずれもオフとなっています。

- 確認応答のタイムアウト  
インシデントが放置されないように、確認応答後、一定時間経過しても解決しない場合はリマインドするよう設定できます  
  - [エスカレーションポリシー](#エスカレーションポリシー)
  にもタイムアウトの設定がありますが、これは確認応答が「ない」場合にいつまで待つかを設定するものです。  
  これに対して確認応答のタイムアウトとは、確認応答が「あった」場合に、解決までどれくらい待つかを設定するものです

- 自動解決  
インシデントが一定時間更新されない場合に、自動的にステータスを更新するよう設定できます

## 設定例

ここでは、Datadogのアラートを受け取ってインシデントを発生させる例を紹介します。詳細な設定手順については
[こちら](https://www.pagerduty.com/docs/guides/datadog-integration-guide/)
を参照してください。

### PagerDuty側でデフォルトのエスカレーションポリシーを設定しておく

事前準備として、PagerDuty側のエスカレーションポリシーを設定しておきます。これがないとインシデント通知を受け取れません。  
[`People`]-[`Escalation Policies`]でエスカレーションポリシーの設定画面が表示されますので、
`Notify`に通知を受け取れるユーザーを設定してください。  
デフォルトではメールアドレスが有効になっていると思われますので、問題なければそのままユーザーを指定してください。

### PagerDuty側でDatadogと接続するサービスを作成する

まずPagerDuty側で、Datadogと統合するサービスを作成します。  
メニュー[`Services`]-[`Service Directory`]をクリックして`Service Directory`画面を表示します。
`+New Service`でサービスの登録画面を表示します。  
`Name`に任意の名称（ここでは`Datadog test`としておきます。この名称は後で使いますので控えておいてください）を入力します。  
`Integration type`のプルダウンで`Datadog`を指定します。
その他の項目は初期設定で構いませんので、[`Add Service`]でサービスを作成します。

登録後、[`Integrations`]タブの`Integrations`でDatadogの`Intagration Key`が発行されていることを確認してください。

### Datadog側でPagerDutyとのインテグレーションを設定する

続いてDatadog側で、PagerDutyとの接続設定を行います。  
メニューの[`Integrations`]-[`Integrations`]をクリックして`Integrations`画面を表示します。検索バーで`PagerDuty`と入力し、表示されるアイコンの`+Install`（2回目以降は`+Configure`）を選択します。

`Configuration`タブをクリックし、`Alert With PagerDuty`をクリックします。
PagerDutyの認証画面が表示されるので、PagerDutyのログインID/PWDでログインします。

`Configure the Datadog integration.`という画面が表示されます。  
今回は先にPagerDuty側のサービスを作成しておいたので、`Use an integration on an existing service`で、
先程作成したサービス名を選択します。  
選択したら`Finish Integration`で設定を終了します。

Datadogの`Integrations`画面が表示されます。
すでにサービス名や`Integration Key`はセットされているので、そのまま`Update Configuration`で設定を終了します。

### DatadogのアラートでPagerDutyのインシデントを発生させる

DatadogのアラートでPagerDutyのインシデントを発生させてみます。
なお、アラートの詳細に関しては[こちら](datadog_monitor.md)もあわせて参照してください。

Datadogのメニューで[Monitors]-[New Monitor]をクリックします。Monitorの種類は任意ですがここでは`Logs`を使用します。

このMonitorはしきい値超えによりアラートを発生させるため、`Set alert conditions`で任意のしきい値を設定します。
ここでは`Alert threshold`を10、`Warning threshold`を5にします。

`Say what's happening`の`Edit`タブでアラートのタイトルを設定しておきます。名称は任意ですが`PagerDuty test`としておきます。

`Notify your team`にPagerDutyの宛先を指定します。先程作成したサービス名で`@pagerduty-SERVICE_NAME`という宛先が選択できるようになっていますので、それを選択します。

`Test Notification`をクリックすると発生させるアラートの種類を選択できます。任意ですが`Alert`を選択して`Run Test`をクリックします。

PagerDutyにログインし、インシデントが作成されることを確認してください。また、事前に設定したエスカレーションポリシーのユーザーに通知が送られていることも確認してください。

## 通知を受けてからの流れ

PagerDutyから実際にインシデント連絡を受け取った後の流れは、以下のようになります。

1. インシデントの[ステータス](#インシデントのステータスについて)を`Triggered`->`Acknowledged`に更新する
1. インシデント情報を確認する
    - 必要に応じてインシデント元（Datadogなど）の情報を参照したり、別の担当者にアサインし直す
1. 問題が解消したらインシデントのステータスを`Resoloved`に変更する

公式ドキュメントの[インシデントのライフサイクル](https://support.pagerduty.com/docs/incidents#incident-lifecycle)もあわせてご参照ください。

### インシデントのステータスについて

インシデントにはステータスがあり、PagerDutyはステータスによって通知を続けるかどうかを判断しています。

発生したインシデントは`Triggered`というステータスになっており、このステータスである限り、インシデントはエスカレーションポリシーに従って通知され続けます。

通知を受領したユーザーがステータスを`Acknowledge`にすることで、インシデントのエスカレーションは止まります。ただし、Web UIからステータスを`Triggered`に戻すことができ、その場合は再度最初から通知をやり直します。

ステータスが`Resolved`になったインシデントに関しては、ステータスを変更したり、再度通知を送ることはできません。

[公式ドキュメント](https://support.pagerduty.com/docs/incidents#incident-states)の説明もあわせて参照してください。

- 公式ドキュメント上明記されていませんが、同一タイトルのインシデントは、最初のインシデントが`Resolved`されるまでは再作成されません

### インシデント連絡の受領〜ステータスの更新

通知を受領したユーザーは、インデントのステータスを更新します。

- SMSが届いたり電話に出ただけでは、ステータスは`Acknowledged`にはならず、一定時間経過するとインシデントは次のレベルへエスカレーションされます
- 設定によっては、同時に複数の経路（電話とSMSなど）で通知されることもありますが、どれか１つで対応すればOKです
- ステータスは`Resolved`が優先されます。電話で`Resolved`したあと、SMSで`Acknoledged`したとしても、ステータスは`Resolved`のままです

ステータスはWeb UIで更新できますが、通知を受けた際の操作でも更新可能です。詳細は公式ドキュメントの[通知の内容と動作](https://support.pagerduty.com/docs/notifications)を参照してください。

### インシデント情報の確認

Web UIでインシデントの詳細情報を確認できます（モバイルアプリでも一部の情報が参照できます）。

#### インシデント一覧

Web UIの[`Incidents`]メニューを選択すると、現在発生しているインシデントの一覧が表示されます。

- Open: ステータスが`Triggered`, `Acknowledged`のインシデントが表示されます
- Triggered: ステータスが`Triggered`のインシデントが表示されます
- Acknowledged: ステータスが`Acknowledged`のインシデントが表示されます
- Resolved: ステータスが`Resolved`のインシデントが表示されます
- Any Status: 全インシデントが表示されます

インシデント一覧の`Title`欄の右下の`#`にインシデント番号が表示されていますので、その番号でインシデントを特定してください。
インシデント一覧の右上にある`Go to Incident #`で直接表示もできます。  
また、自分がアサインされたインシデントのみ表示したいときは`Assigned to me`を選択してください。

#### インシデント情報（単票）

`Alerts`に、PagerDutyがインシデントを発生させる原因となった情報が表示されています。
通知元がDatadogの場合、画面下部の`LINKS`や`CLIENT`のリンクによりDatadogの画面に遷移し、メトリクスやログの詳細を確認できます。

`Timeline`にインシデントに関する操作履歴が表示されています。対応経緯など確認したいときに参照してください。

画面右の`Notes`に、調査した内容などのメモを残すことができます。ここに記入した内容は`Timeline`にも表示されるため、`Timeline`に情報を集約できます。

インシデント対応が完了したら、ステータスを`Resolved`に変更します。画面右上の`Resolve`をクリックしてください。

### インシデントの委任

他のユーザーに対応を委任する場合は`Reassign`をクリックしてください。`Search for an Escalation Policy, User, Level`と表示されているテキストボックスを選択すると、委任先を指定できます。

- Escalation Policy  
  指定したエスカレーションポリシーの先頭からインシデントを通知します
- User  
  指定したユーザーにインシデントを通知します
- Level  
  現在のエスカレーションポリシー内の指定したレベルにインシデントを通知します

詳細は[公式ドキュメント](https://support.pagerduty.com/docs/reassigning-and-delegating-incidents#reassign-an-incident-from-pagerduty)を参照してください。

その他の操作については公式ドキュメントの[インシデントの編集](https://support.pagerduty.com/docs/editing-incidents)を参照してください。

### インシデントのレポート

Web UIの[`Analytics`]メニューにより、インシデントの発生件数や対応時間に関するレポートを表示できます。  
詳細は公式ドキュメントの[基本的なレポート](https://support.pagerduty.com/docs/reporting#basic-reporting)を参照してください。
