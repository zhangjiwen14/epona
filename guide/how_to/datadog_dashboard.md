# Datadogのダッシュボード設定

## はじめに

Eponaでは[Operation-Less](../../README.md#operation-less)の観点から、マネージドサービスやSaaSの利用を推奨しており、
モニタリングについては[Datadog](https://www.datadoghq.com/ja/)をサポートしています。  
[Datadogを用いたログからのメトリクス抽出、可視化ガイド](datadog_log_analysis.md)では非構造化ログを構造化し
可視化するまでの手順を示しましたが、本ガイドでは特に[ダッシュボード](https://docs.datadoghq.com/ja/dashboards/)
について記述します。

## ダッシュボードとは

ダッシュボードとは、もともとは自動車の計器盤のことを指します。走行速度や燃料の残量など、運転に必要な様々な情報を一箇所に集約して運転者を支援するものです。  
ここから転じて、複数の情報源から必要な情報を抽出し、グラフ・表などで可視化して1箇所に集約したものも「ダッシュボード」と呼ばれるようになりました。  
ダッシュボードを作る際は、ダッシュボードを作成する目的、つまり「いつ」「誰が」「何のために」ダッシュボードを見るかを考えることが重要です。
用途や目的のないダッシュボードは使われることがなく、いずれ陳腐化していくからです。  
目的を明確にしたうえでダッシュボードを作成し、足りない情報を補ったり不要な情報を落としたりしながら、
サービスの成長に合わせてメンテナンスしていくことが望ましいでしょう。

EponaではシステムメトリクスについてはDatadog、ビジネスメトリクスについては[Amazon QuickSight](https://aws.amazon.com/jp/quicksight/)を使ったダッシュボードの作成方法をガイディングします。
Amazon QuickSightのガイドに関しては[こちら](aws/quicksight_monitor.md)を参照してください。

本ガイドでは、Datadogでのダッシュボードについて記述します。

## 前提事項・事前準備

本ガイドにおいては以下の準備ができていることを前提とします。

- Datadogユーザー登録が済んでいること
- [Datadogとのインテグレーション設定](../patterns/aws/datadog_integration.md)や、[ログからのメトリクス抽出](datadog_log_analysis.md)などの手順により、必要なメトリクスをDatadogで取得できる状態になっていること

## Datadogでプリセットされているダッシュボードについて

Datadogでは、すぐに使えるダッシュボードが事前に用意されています。
Datadogと外部サービスのインテグレーションを設定することで、そのサービスからメトリクスが取得され、以下のようなダッシュボードが自動的に作成されます。

![image](../resources/datadog_dashboard/preset_rds.png)

![image](../resources/datadog_dashboard/preset_ecs.png)

インテグレーションについては、公式ドキュメントの[インテグレーション入門](https://docs.datadoghq.com/ja/getting_started/integrations/)やEponaドキュメントの[Datadogとのインテグレーション設定](../patterns/aws/datadog_integration.md)を参照してください。

## Datadogで作成可能なダッシュボードの種類

Datadogでは、プリセット以外にも利用者独自でカスタムしたダッシュボードを作成できます。  
Datadogのダッシュボードには[スクリーンボード](https://docs.datadoghq.com/ja/dashboards/screenboards/)と[タイムボード](https://docs.datadoghq.com/ja/dashboards/timeboards/)という2つの種類があります。両者の違いについては[スクリーンボードとタイムボード](https://docs.datadoghq.com/ja/dashboards/)にも記載がありますが、主な違いは以下の2点です。

- レイアウト  
  スクリーンボードでは配置するグラフの大きさや位置を自由に変えられますが、タイムボードは大きさや位置は固定です
- 時間軸  
  スクリーンボードは個々のグラフ内で別々に期間を指定できますが、タイムボードは全グラフで同じ期間を共有します

2点目について実際の画面で補足します。

- スクリーンボード  
  ![screenboard](../resources/datadog_dashboard/screenboard_sample.png)

- タイムボード  
  ![screenboard](../resources/datadog_dashboard/timeboard_sample.png)

タイムボードの右上に、期間を指定するバーがあるのを確認してください。
このバーで期間を指定すると、全グラフがその期間に応じて動的に再集計・再描画されます。
期間はユーザーが自由に設定できます（2020/10/10 09:00〜2020/10/11 10:39のような、端数のある期間も指定できます）。

一方、スクリーンボードでは動的に期間を指定できず、グラフの作成時に指定した期間で表示されます。期間は直近X時間/日/月など、Datadogが用意している選択肢の中で選択します。現在どの期間のグラフが表示されているかは、各グラフの右上に表示されています。

このような違いから、スクリーンボードは通常時の定点観測として使用し、タイムボードは問題発生時のトラブルシューティングに使用することが想定されます。

以上の説明をもとに公式ドキュメントの
[スクリーンボード](https://docs.datadoghq.com/ja/dashboards/screenboards/)と
[タイムボード](https://docs.datadoghq.com/ja/dashboards/timeboards/)
の記述を参照すると、より理解が深まると思われます。

## Datadogで使用できるウィジェット

Datadogではグラフや表などを`ウィジェット`と読んでいます。ダッシュボードは、このウィジェットを組み合わせることで作成します。  
主な`ウィジェット`の意味を以下に示します。その他の`ウィジェット`については[こちら](https://docs.datadoghq.com/ja/dashboards/widgets/)を参照してください。

| ウィジェット名           | 説明                                                                                                                                                                                                                              |
| ------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Timeseries: 時系列       | 指定したメトリクスの経時変化を、折れ線グラフや棒グラフで表示します。<BR>![image](../resources/datadog_dashboard/widget_timeseries.png)                                                                                            |
| Query Value: クエリ値    | 指定したメトリクスやクエリ結果をテキストで表示します。<BR>![image](../resources/datadog_dashboard/widget_queryvalue.png)                                                                                                          |
| Table: 表                | 指定したメトリクスの値を表形式で表示します。<BR>![image](../resources/datadog_dashboard/widget_table.png)                                                                                                                         |
| Top List: 上位リスト     | 指定したメトリクスの上位または下位を表形式で表示します。<BR>![image](../resources/datadog_dashboard/widget_toplist.png)                                                                                                           |
| SLO Summery: SLOサマリ   | Datadogの[SLO（サービスレベル目標）機能](https://docs.datadoghq.com/ja/monitors/service_level_objectives/)で設定したSLOの達成状況を表示します。<BR>![image](../resources/datadog_dashboard/widget_slo.png)                        |
| Heat Map: ヒートマップ   | 指定したメトリクスをホストやタグなどで横断的に集計し、濃淡で表すことで傾向を可視化します。<BR>![image](../resources/datadog_dashboard/widget_heatmap.png)                                                                         |
| Check Status: ステータス | DatadogエージェントやAWSサービスの状態などを表示します。[サービスチェック](https://docs.datadoghq.com/ja/developers/service_checks/)の内容も参照してください。<BR>![image](../resources/datadog_dashboard/widget_checkstatus.png) |
| Change: 変化量           | 指定したメトリクスの一定期間内の変化量を表示します。<BR>![image](../resources/datadog_dashboard/widget_change.png)                                                                                                                |
| Host Map: ホストマップ   | ホストやコンテナ全体の状態をマップで表示します。[ホストマップ](https://docs.datadoghq.com/ja/infrastructure/hostmap/)の内容も参照してください。<BR>![image](../resources/datadog_dashboard/widget_hostmap.png)                    |
| Scatter Plot: 散布図     | 2つのメトリクスの相関関係を散布図で表示します。<BR>![image](../resources/datadog_dashboard/widget_scatterplot.png)                                                                                                                |

## 設定例

サンプルとして、以下の情報を含むダッシュボードを作成します。対象となるアプリケーションとモニタリングしたい項目を、以下のように整理したとしましょう。

- 対象となるアプリケーション
  - [SPA + REST API構成のサービス開発リファレンスのコード](https://github.com/Fintan-contents/example-chat)
(以降、example-chat)
- 目的
  - コンテナに関するモニタリング
    - サービスが稼働しているか知りたい
    - リソースの使用状況を知りたい
  - データベースに関するモニタリング
    - 処理時間を知りたい（性能劣化していないか）
  - アプリケーションに関するモニタリング
    - アプリケーションの稼働状況を知りたい（エラー発生状況など）
- 取得したい情報
  - Amazon ECSに関する情報
    - タスクステータス
    - コンテナステータス
    - メモリ
    - CPU
  - Amazon RDSに関する情報
    - データベース遅延状況
    - コネクション数
  - アプリケーションに関する情報
    - エラーログ数
    - エラー発生箇所（パス）

ここではAmazon ECSやAmazon RDSに関する情報をタイムボード、アプリケーションに関する情報をスクリーンボードで作成してみます。

:information_source: Amazon ECSやAmazon RDSには[プリセットのダッシュボード](#datadogでプリセットされているダッシュボードについて)もあります。必要に応じてそちらもご参照ください。

### タイムボードの設定例

以下、タイムボードの作成例を記載します。

- [タイムボードの作成](#タイムボードの作成)
- [ウィジェットの設定項目](#ウィジェットの設定項目)
- [ウィジェット：Amazon ECS コンテナステータス](#ウィジェットamazon-ecs-コンテナステータス)
- [ウィジェット：Amazon ECS メモリ使用率](#ウィジェットamazon-ecs-メモリ使用率)
- [ウィジェット：Amazon ECS CPU使用率](#ウィジェットamazon-ecs-cpu使用率)
- [ウィジェット：Amazon RDS コネクション数](#ウィジェットamazon-rds-コネクション数)
- [ウィジェット：Amazon RDS 読み取り/書き込み遅延](#ウィジェットamazon-rds-読み取り書き込み遅延)
- [ウィジェット：Amazon RDS CPU利用率](#ウィジェットamazon-rds-cpu利用率)
- [ウィジェット：Amazon RDS 利用可能なメモリ](#ウィジェットamazon-rds-利用可能なメモリ)
- [タイムボード設定後のイメージ](#タイムボード設定後のイメージ)

#### タイムボードの作成

メニューの[`Dashboards`]-[`New Dashboard`]を選択すると、ダッシュボードの選択画面が表示されます。  
`Dashboard name`にはデフォルトでユーザー名と作成日時の名称が付与されますが、今回は`example-chat-timeboard`とします。
ダッシュボードの種類は`New Timeboard`を使用します。

空白のダッシュボードが作成されるので、画面上部の`Edit Widgets`をクリックします。すると、ウィジェットの選択メニューが表示されます。

#### ウィジェットの設定項目

使用したいウィジェットのアイコンをダッシュボード上の空欄にドラッグすると、編集画面が開きます。ここでは、以下のような情報を設定します。

`Select your visualization`は、現在選択しているウィジェットの種類です。

`Graph your data`では、グラフ化する項目の検索条件等を設定します。

- `Metric ▼`では、グラフ化する項目の分類を選択します。  
`Metric`の他に`Log Events`や`Network Traffic`なども選択できます
- `from ▼`では、グラフ化するデータの絞り込み条件（フィルター）を設定します。ホストやタグによる絞り込みが可能です
- `avg by ▼`では、集計方法を選択します。`avg by`の他に、`max by`、`min by`、`sum by`が選択可能です
- `Display`、`Color`では、グラフの見た目を設定します。`Line`(折れ線グラフ)、`Area`（面グラフ）、`Bar`（棒グラフ）を選択できます
- 1つの平面に複数のグラフを表示する場合は`Graph additional`で、追加したいグラフの内容を選択します
- `Give graph a name`でグラフの名称を設定します

設定内容の詳細については公式ドキュメントの[クエリ](https://docs.datadoghq.com/ja/dashboards/querying/)を参照してください。

#### ウィジェット：Amazon ECS コンテナステータス

Amazon ECSのメトリクスとしては、例えば`Running`、`Pending`、`Desired`といったステータスごとの数が取得できます。
これらのデータが可視化できれば、希望しているタスク数を充足しているか、何らかの問題でタスクが待たされている場合はどのくらい継続しているのかが
一目でわかるようになります。  
ここでは3つのメトリクスを1つのウィジェットに重ねて表示してみます。

`Timeseries`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

![image](../resources/datadog_dashboard/timeboard_container_setting.png)

1行目の設定は以下のとおりです。

- Metric: `aws.ecs.service.running`
- from: `cluster:epona-chat-example-backend`
- Display: `Area`、Color: `Classic`

`Graph additional`の`Metric`をクリックすると入力欄が増えます。2行目の設定は以下のとおりです。

- Metric: `aws.ecs.service.pending`
- from: `cluster:epona-chat-example-backend`
- Display: `Area`、Color: `Classic`

同様に入力欄を追加し、3行目に以下の設定を入力します。

- Metric: `aws.ecs.service.desired`
- from: `cluster:epona-chat-example-backend`
- Display: `Lines`、Color: `Warm`、Style: `Dashed`、Stroke: `Thick`

[`Save`]をクリックして保存します。これにより、タイムボードに`epona-chat-example-backend`サービスのステータス状況が可視化できます。

今回対象としたexample-chatには`epona-chat-example-notifier`というサービスもあります。
こちらも同様にタスクステータスの数を可視化してみましょう。  
同種のメトリクスを可視化する場合、Datadog上で既存のウィジェットをコピーすることで作成を効率化できます。

先程作成したウィジェットの右上に表示される上矢印マークをクリックしてください。メニュー内の`Copy`を選択します。

ダッシュボード画面で`Ctrl+v`を押すと、ウィジェットがコピーされます。コピーされたウィジェットの鉛筆マークをクリックし、
編集画面を表示してください。

`Graph your data`の`from`の内容を３行とも`cluster:epona-chat-example-notifier`に変更し、[`Save`]で
保存します。  
これにより、`epona-chat-example-notifier`のステータスごとのタスク数の推移がタイムボード上で可視化されます。

#### ウィジェット：Amazon ECS メモリ使用率

`Timeseries`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

- Metric: `aws.ecs.service.memory_utilization`
- Display: `Lines`、Color: `Classic`、Style: `Solid`、Stroke: `Normal`

#### ウィジェット：Amazon ECS CPU使用率

`Timeseries`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

- Metric: `aws.ecs.service.cpuutilization`
- Display: `Lines`、Color: `Classic`、Style: `Solid`、Stroke: `Normal`

#### ウィジェット：Amazon RDS コネクション数

`Timeseries`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

- Metric: `aws.rds.database_connections`
- from: `dbinstanceidentifier:epona-rds-instance`
- Display: `Lines`、Color: `Classic`、Style: `Solid`、Stroke: `Normal`

#### ウィジェット：Amazon RDS 読み取り/書き込み遅延

`Timeseries`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

1行目の設定は以下のとおりです。

- Metric: `aws.rds.read_latency`
- from: `dbinstanceidentifier:epona-rds-instance`
- Display: `Lines`、Color: `Classic`、Style: `Solid`、Stroke: `Normal`

`Graph additional`の`Metric`をクリックすると入力欄が増えます。2行目の設定は以下のとおりです。

- Metric: `aws.rds.write_latency`
- from: `dbinstanceidentifier:epona-rds-instance`
- Display: `Lines`、Color: `Classic`、Style: `Solid`、Stroke: `Normal`

#### ウィジェット：Amazon RDS CPU利用率

`Timeseries`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

- Metric: `aws.rds.cpuutilization`
- from: `dbinstanceidentifier:epona-rds-instance`
- Display: `Lines`、Color: `Classic`、Style: `Solid`、Stroke: `Normal`

#### ウィジェット：Amazon RDS 利用可能なメモリ

`Timeseries`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

- Metric: `aws.rds.freeable_memory`
- from: `dbinstanceidentifier:epona-rds-instance`
- Display: `Lines`、Color: `Classic`、Style: `Solid`、Stroke: `Normal`

#### タイムボード設定後のイメージ

![image](../resources/datadog_dashboard/timeboard_container_sample.png)

### スクリーンボードの設定例

スクリーンボードの作成手順も、概ね上記のタイムボードの手順と同様です。
ここでは、アプリケーションログから取得したメトリクスのスクリーンボード上での可視化に取り組んでみましょう。

題材として、アプリケーションで発生するエラー数の推移、エラーが発生するURIの可視化等を取り上げます。
これにより、視覚的にアプリケーションの健全性を確認できるようになります。

![image](../resources/datadog_dashboard/screenboard_app_sample.png)

もちろんこのためには、アプリケーションログをパースしておく必要があります。詳細については、[こちら](datadog_log_analysis.md)のガイドをご参照ください。

以下の手順で作成していきます。

- [事前準備（ログのパース）](#事前準備ログのパース)
- [スクリーンボードの作成](#スクリーンボードの作成)
- [ウィジェット：WARNレベル以上のアプリケーションログ数](#ウィジェットwarnレベル以上のアプリケーションログ数)
- [ウィジェット：アプリケーションエラーログ発生箇所（パス）](#ウィジェットアプリケーションエラーログ発生箇所パス)
- [ウィジェット：パスごとのアクセス数](#ウィジェットパスごとのアクセス数)
- [ウィジェット：httpエラーが発生しているパス](#ウィジェットhttpエラーが発生しているパス)

#### 事前準備（ログのパース）

アプリケーションログから抽出したメトリクスを可視化するにあたって、ログ用のパイプラインに以下のような設定を入れています。  
なお、これらの設定の詳細は[Datadogを用いたログからのメトリクス抽出、可視化ガイド](datadog_log_analysis.md)を
参照してください。  
以下に述べる設定は、メニューの[Logs]->[Configuration]からPipelineを選択して実施します。

- Pre processing for JSON logs  
  EponaでデプロイしたアプリケーションのログをDatadogに取り込むと自動的にパースされますが、`log`という項目が構造化されません。  
  [Datadogドキュメント](https://docs.datadoghq.com/ja/logs/processing/#json-%E3%83%AD%E3%82%B0%E3%81%AE%E5%89%8D%E5%87%A6%E7%90%86)
  を参考に、`Message attributes`に`, log`を追記して、構造化箇所を指定してください

- Pipeline  
  まずはアプリケーションからのログを処理するパイプラインを構成します。  
  アプリケーションはAmazon Fargate上で動作しているため、Fargateから送られてくるログを対象とします。  
  `Add a pipeline`のリンクをクリックして、以下のパイプラインを追加してください

  - Filter: `Source:fargate`
  - Name: `fargate_sample`

- Processor  
  対象のアプリケーションは構造化ログではなくテキスト形式の非構造化ログを出力しています。
  そのため、これをGrok Parserを用いて構造化ログへと変換する必要があります。  
  上記で作成したパイプライン内の`Add Processor`をクリックして、以下のGrok Parserを追加してください。  
  読みやすさのため改行を入れていますが、実際の画面では1行で入力します

  - 1個目
    - Define parsing rules

          ```grok
          example_chat %{date("yyyy-MM-dd hh:mm:ss.SSS"):date} %{word:logLevel} %{word:runtimeLoggerName}
           %{word:http.method} %{notSpace:http.url_details.path} %{notSpace:userId} %{notSpace:sessionId}
           %{notSpace:threadName} %{data:message}
          ```

    - Name: `example_chat`

  - 2個目
    - Define parsing rules

          ```grok
          http_access %{ip:clientip} %{notSpace:ident} %{notSpace:auth} \[%{date("dd/MMM/yyyy:HH:mm:ss Z"):date}\]
           "%{word:http.method} %{notSpace:http.url_details.path} %{notSpace:http.version}"
           %{notSpace:http.status_code} %{notSpace:bytes}
          ```

    - Name: `http_access`

  ![image](../resources/datadog_dashboard/screenboard_app_ready.png)

  - Facet  
    [Datadogを用いたログからのメトリクス抽出、可視化ガイド](datadog_log_analysis.md#ファセットの登録)にあるとおり、
    構造化した項目はそのままでは検索条件として使用できません。この後、エラーログの検索のために`@logLevel`を使用するので、
    この項目をファセットへ登録する必要があります。
    メニューの[`Logs`]->[`Search`]を選択し、サイドバーの「+ Add」を選択してください。`Add facet`画面で以下の情報を
    入力してください

    - Path
      - `@logLevel`

#### スクリーンボードの作成

事前準備の設定後、メニューの[`Dashboards`]-[`New Dashboard`]を選択し、ダッシュボードの選択画面を表示してください。  
`Dashboard name`にはデフォルトでユーザー名と作成日時の名称が付与されますが、今回は`example-chat-screenboard`とします。
ダッシュボードの種類は`New Screenboard`を使用します。

#### ウィジェット：WARNレベル以上のアプリケーションログ数

`Timeseries`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

- Log Events
- クエリ条件: `@logLevel:(FATAL OR ERROR OR WARN)`
- Group by: `logLevel(@logLevel)`
- Display: `Lines`、Color: `Warm`、Style: `Solid`、Stroke: `Normal`

`Set display preference`には表示期間を設定します。ここでは`Past 4 Hours`とします。
なお、ここでは説明を簡潔にするため`WARN`レベル以上と定義していますが、実運用時はログレベルごとに可視化する方が望ましいでしょう。

#### ウィジェット：アプリケーションエラーログ発生箇所（パス）

`Table`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

<!-- textlint-disable -->
- Log Events
- Group by: `logLevel(@logLevel) URL Path(@http.url_details.path)`
  - 上記の条件をコピー・ペーストでうまく設定できない場合は、`@logLevel`を入力・確定してから次の`@http.url_details.path`を入力してください
<!-- textlint-enable -->

`Set display preference`は`Past 4 Hours`とします。

#### ウィジェット：パスごとのアクセス数

`Timeseries`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

<!-- textlint-disable -->
- Log Events
- クエリ条件: `-@http.url_details.path:/api/health`
  - 無関係なログが混在している場合は条件に`Source:fargate`などを追加してください
  - 上記の条件をコピー・ペーストでうまく設定できない場合は、手打ちで`@http.url_details.path`と入力して条件確定後、灰色背景の条件文をクリックして`-`を追記してください
- Group by: `URL Path(@http.url_details.path)`
- roll up every: `5m`
- Display: `Lines`、Color: `Classic`、Style: `Solid`、Stroke: `Normal`
<!-- textlint-enable -->

`Set display preference`は`Past 4 Hours`とします。

#### ウィジェット：httpエラーが発生しているパス

`Table`をダッシュボード上の空欄にドラッグし、`Graph your data`に以下の情報を入力します。

<!-- textlint-disable -->
- Log Events
- クエリ条件: `@http.status_code:>=400`
  - 無関係なログが混在している場合は条件に`Source:fargate`などを追加してください
- Group by: `URL Path(@http.url_details.path) Status Code(@http.status_code)`
  - 上記の条件をコピー・ペーストでうまく設定できない場合は、`@http.url_details.path`を入力・確定してから次の`@http.status_code`を入力してください
  <!-- textlint-enable -->

`Set display preference`は`Past 4 Hours`とします。

## ダッシュボードの共有、エクスポート/インポート

例えば、チーム外のステークホルダと情報を共有する時などには、メトリクスを可視化したダッシュボードを共有できると便利です。  
また、エクスポートとインポートを組み合わせれば、ダッシュボードの構築内容をリポジトリで管理できます。  
ダッシュボード画面右上の歯車アイコンをクリックすることで、ダッシュボードの共有やコピー、エクスポートなどが可能です。

### Generate public URL

スクリーンボードのみ利用できる機能です。  
ダッシュボードの
[読み取り専用のURL](https://docs.datadoghq.com/ja/dashboards/sharing/#%E3%83%80%E3%83%83%E3%82%B7%E3%83%A5%E3%83%9C%E3%83%BC%E3%83%89)
を発行します。これにより、外部ユーザーとダッシュボードを共有できます。共有をやめるときは同じメニューから`Revoke Public URL`を選択してください。
なお、ダッシュボード全体でなく、個々の
[グラフの埋め込みコード](https://docs.datadoghq.com/ja/dashboards/sharing/#%E3%82%B0%E3%83%A9%E3%83%95)
を生成することでも外部と共有可能です。  
グラフの共有に関しては、スクリーンボード・タイムボードともに利用可能です。

### Clone dashboard

ダッシュボード全体をコピーします。メニュー選択後、コピー先の名称を指定することで、その名前でダッシュボードがコピーされます。

### Copy dashboard JSON

ダッシュボードのJSONをクリップボードにコピーします。そのままテキストエディタなどに貼り付けることも可能ですが、[Import dashboard JSON](#Import-dashboard-JSON)で直接インポートに使うことも可能です。

### Import dashboard JSON

指定されたJSONをダッシュボードとしてインポートします。
[Export dashboard JSON](#Export-dashboard-JSON)で出力したJSONファイル、
または[Copy dashboard JSON](#Copy-dashboard-JSON)で事前にコピーしておいた内容からダッシュボードを作成します。  
このオプションを使うと、現在のダッシュボードの内容はすべて上書きされますので注意してください。

### Export dashboard JSON

ダッシュボードのJSONファイルをダウンロードします。

### ログフィルターが設定されている場合の注意点

[ログフィルター](../patterns/aws/datadog_log_trigger.md#ログフィルターについて)が設定されているログについても、本ガイドに記載の手順でダッシュボードに可視化できます。

ただし除外したログについては`Log Event`での検索ができないため、事前に必要な情報を[メトリクス](https://docs.datadoghq.com/ja/logs/logs_to_metrics/)に登録し、`Metrics`を指定してウィジェットを作成してください。
